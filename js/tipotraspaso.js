//Estaticos
var LIGA_PAGINA = 100;
var LECTURA_TARGETA = 101;
var ligaCase = "php/casetipotraspaso.php";
var cTituloModal = "Traspaso";

//Tipo Traspaso
var iEmpleado = 0;
var iFolio = 0;
var sCURP = "";
var arrSolicitud = new Array();

//Mostrar PDF
var aConsultaHtml = new Array();
var aConsultaLigaPagina = new Array();
var iOpcion = 1;
var iFlagBoton = 0;

var sNumTarj = 0;
var sNumTarjC = 0;
var sNumCvc = 0;

// MOVIL
// Instruccion que permite conocer el sistema operativo de donde ingresa el usuario
var OSName = "Desconocido";
if (navigator.appVersion.indexOf("Win") != -1) OSName = "Windows";
if (navigator.appVersion.indexOf("Mac") != -1) OSName = "MacOS";
if (navigator.appVersion.indexOf("X11") != -1) OSName = "UNIX";
if (navigator.appVersion.indexOf("Linux") != -1) OSName = "Linux";
if (navigator.appVersion.indexOf("Android") != -1) OSName = "Android";

$(document).ready(function () {
	$(document).keydown(function(ev)
    {			
		if((ev.keyCode >= 112) && (ev.keyCode <= 123))
		{			
			return false;
		}

		return true;
	});

	bloquearModal();

	/*Obtener Variables pasadas por URL*/
	if (OSName == "Android") {
		iEmpleado = getParameterByName("empleado");
		iFolio = getParameterByName("folio");
		$("#btnDelizarTarjeta").val('Capturar Tarjeta');

		var scbxTipoTraspaso = document.getElementById("cbxTipoTraspaso");
		scbxTipoTraspaso.style.backgroundColor = "white";

		var scbxAforeCedente = document.getElementById("cbxAforeCedente");
		scbxAforeCedente.style.backgroundColor = "white";

		var scbxPeriodoEstadoCuenta = document.getElementById("cbxPeriodoEstadoCuenta");
		scbxPeriodoEstadoCuenta.style.backgroundColor = "white";

	} else {
		iEmpleado = getQueryVariable("empleado");
		iFolio = getQueryVariable("folio");
	}

	if (OSName == "Android") {
		$("body").on("keydown", "#numTarjet", function (e) {

			var texto = document.getElementById("numTarjet").value;
			texto = texto.toString();

			if (e.key == 'Unidentified') {
				document.getElementById("numTarjet").value = "";
				setTimeout(function () {
					document.getElementById("numTarjet").value = texto;
				}, 10);
			}

			if ($(this).val().length == 16) {
				if (e.which != 8 && e.which != 0) {
					e.preventDefault();
				}
			} else {
				if ((e.which < 48 && e.which > 58) && e.which != 0) {
					e.preventDefault();
				}

			}
		});
		$("body").on("keydown", "#numTarjetC", function (e) {

			var texto = document.getElementById("numTarjetC").value;
			texto = texto.toString();

			if (e.key == 'Unidentified') {
				document.getElementById("numTarjetC").value = "";
				setTimeout(function () {
					document.getElementById("numTarjetC").value = texto;
				}, 10);
			}

			if ($(this).val().length == 16) {
				if (e.which != 8 && e.which != 0) {
					e.preventDefault();
				}
			} else {
				if ((e.which < 48 && e.which > 58) && e.which != 0) {
					e.preventDefault();
				}

			}
		});
		$("body").on("keydown", "#txtFolioEdocta", function (e) {

			var texto = document.getElementById("txtFolioEdocta").value;
			texto = texto.toString();

			if (e.key == 'Unidentified') {
				document.getElementById("txtFolioEdocta").value = "";
				setTimeout(function () {
					document.getElementById("txtFolioEdocta").value = texto;
				}, 10);
			}

			if ($(this).val().length == 2) {
				if (e.which != 8 && e.which != 0) {
					e.preventDefault();
				}
			} else {
				if ((e.which < 48 && e.which > 58) && e.which != 0) {
					e.preventDefault();
				}

			}
		});
	} else {
		$("body").on("keydown", "#numTarjet", function (event) {
			if ($(this).val().length == 16) {
				if (event.which != 8 && event.which != 0) {
					event.preventDefault();
				}
			} else {
				if (event.which < 48 || event.which > 58) {
					if (event.which != 8 && event.which != 0 && (event.which < 96 || event.which > 105)) {
						event.preventDefault();
					}

				}

			}

		});
		$("body").on("keydown", "#numTarjetC", function (event) {
			if ($(this).val().length == 16) {
				if (event.which != 8 && event.which != 0) {
					event.preventDefault();
				}
			} else {
				if (event.which < 48 || event.which > 58) {
					if (event.which != 8 && event.which != 0 && (event.which < 96 || event.which > 105)) {
						event.preventDefault();
					}

				}

			}

		});
		$("#txtFolioEdocta").keypress(function (e) {
			if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57))
				return false;
		});
	}
	//variables para validar marca renapo.
	var divRespuestaRenapo 	= document.getElementById('divRespuestaRenapo');
	var addRespuesta = '';
	//Falta numero de Empleado o no hay datos de este
	if ((iEmpleado != "") && (obtnerinformacionpromotor(iEmpleado))) {
		comboTipoTraspaso();
		comboAforeCedente();
		comboPeriodoEstado();
		desbloquearModal();
		if (obtenerCURP(iFolio)) {
			$("#txtCurp").val(sCURP);
			if(ValidaConsultaRenapoPrevia(sCURP,1))
			{
				if(divRespuestaRenapo != null)
				{
					divRespuestaRenapo.innerHTML = '';
					addRespuesta += '<input style="cursor:default" autocomplete="off" type = "image" src="imagenes/RenapoExitoso.png" width = "132" height = "28"></input>';
					divRespuestaRenapo.innerHTML = addRespuesta;
				}
			}
			desbloquearModal();
		}
		else {
			MensajeCerrar("El folio: " + iFolio + " no esta ligado a una CURP, Favor de comunicarce con Mesa de Ayuda!.");
		}

	}

	$("#cbxTipoTraspaso").change(function () {
		var iTipoTra = $("#cbxTipoTraspaso option:selected").val();

		if (iTipoTra == 1) {
			$(".divEstado").hide();
			$(".divBanco").show();
			$("#txtFolioEdocta").val("00");
			$("#txtFolioEdocta").prop('disabled', true);
			$('#cbxAforeCedente').prop('selectedIndex', 0);
		}
		else {
			$(".divBanco").hide();
			$(".divEstado").show();
			$("#txtFolioEdocta").val("");
			$("#txtFolioEdocta").prop('disabled', false);
			$('#cbxAforeCedente').prop('selectedIndex', 0);
			$('#cbxPeriodoEstadoCuenta').prop('selectedIndex', 0);
		}
	});

	$("#btnDelizarTarjeta").click(function () {
		arrSolicitud = [];
		arrSolicitud = CamposASolicitud(iFolio);
		if(arrSolicitud.length > 0)
		{
			if(OSName == "Android"){

				var sHtml='<div class=\"form-group\">'+
				'<table>'+
					'<tr>'+
						'<td colspan=\"2\" align="center">'+
							'<img src="imagenes/tarjeta.png" width="200"><br><br>'+
						'</td>'+
					'</tr>'+
						'<tr id="divTarjeta">'+
							'<td>'+
								'<label for="numTarjet">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;N&uacute;mero Tarjeta:</label>'+
							'</td>'+
							'<td>'+
								'<input type="tel" class="form-control" id="numTarjet">'+
							'</td>'+
						'</tr>'+
						'<tr id="divTarjetaC">'+
							'<td>'+
								'<label for="numTarjetC">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Confirmar N&uacute;m.:</label>'+
							'</td>'+
							'<td>'+
								'<input type="tel" class="form-control" id="numTarjetC">'+
							'</td>'+
						'</tr>'+
						'<tr id="divCvc" style="display:none">'+
							'<td>'+
								'<label for="CVC">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Código de verificación:</label>'+
							'</td>'+
							'<td>'+
								'<input type="tel" class="form-control" id="numCvc" size=3 maxlength=3>'+
							'</td>'+
						'</tr>'+
				'</table>'+
				'<center><br><label style="font-size:9px; color:#FF0000;" id="mensajeError"></label></center>'+
				'</div>';

				var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
				myDlg.capturartarjeta(450, 350, 'Capturar Tarjeta');
				myDlg.mostrar(sHtml);

			}else{
				iOpcion = LECTURA_TARGETA;
				opcionEjecuta("");
			}
		}
	});

	$("#btnContinuar").click(function () {
		if (validarCampos()) {
			arrSolicitud = [];
			arrSolicitud = CamposASolicitud(iFolio);
			if (arrSolicitud.length > 0)
				llamarMateriaFinanaciera();//levantar materiaFinanciera
		}
	});

	$("#btnVolver").click(function () {
		MensajeCerrarPreg("Seguro que desea Cerrar la pagina.");
	});
	//Activar el select para el tipo traspaso
	$("#cbxTipoTraspaso").trigger("change");
});

/*--------------------------------------------GENERAL---------------------------------------------*/
function getQueryVariable(varGet)
{

	var sUrl = window.location.search.substring(1);
	var vars = sUrl.split("&");
	for (var i=0;i<vars.length;i++)
	{
	   var par = vars[i].split("=");
	   if(par[0] == varGet)
	   {
			return par[1];
		}
	}
	return (false);
}

function validarTarjeta() {
	sNumTarj = $("#numTarjet").val();
	sNumTarjC = $("#numTarjetC").val();

	if (sNumTarj.length < 16 || sNumTarj == "4169123456789010") {
		$('#mensajeError').html("N&uacute;mero de tarjeta incorrecto.");
	} else {
		$('#mensajeError').html("");
		if (sNumTarj == sNumTarjC) {
			document.getElementById("divTarjeta").style.display = "none";
			document.getElementById("divTarjetaC").style.display = "none";
			document.getElementById("divCvc").style.display = "block";
			document.getElementById("botones1").style.display = "none";
			document.getElementById("botones2").style.display = "block";
			$('#mensajeError').html("");
		} else {
			$('#mensajeError').html("Los N&uacute;meros no coinciden.");
		}
	}
}

function validarCVC(){

	sNumCvc = $("#numCvc").val();

	if (sNumCvc.length < 3) {
		$('#mensajeError').html("Ingrese los tres dígitos del CVC.");
	} else {
		$('#mensajeError').html("");
		guardarTarjeta(sNumTarj);
	}
}


function guardarTarjeta(sNumTarj) {
	var bRetorna = 0;
	$.ajax(
		{
			async: false,
			cache: false,
			data: { opcion: 8, cNumeroTarjeta: sNumTarj, ipCliente: iFolio },
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			success: function (data) {
				bRetorna = data.respuesta;
				cerrar();
				iOpcion = LECTURA_TARGETA;
				respuestaWebService(sNumTarj);
			},
			error: function (xhr, status, error) { },
		});

	return bRetorna;
}

/**************************************<FIN APPLET>**************************************/
/**************************************</INICIO EXE>**************************************/
function opcionEjecuta(sParametros) {
	var Ruta = '';
	if (OSName != "Android") {

		switch (iOpcion) {
			case LIGA_PAGINA:
				Ruta = 'C://sys//firefoxportable//navegadorAfore.exe';
				break;
			case LECTURA_TARGETA:
				Ruta = 'C:\\sys\\modulo\\LEERTARJETAAFORE.EXE';
				break;
		}

		ejecutaWebService(Ruta, sParametros);

	} else {

		switch (iOpcion) {
			case LIGA_PAGINA:

				sParametros = sParametros.replace('http://', '');
				sParametros = sParametros.replace('https://', '');
				var indice = sParametros.indexOf("/");
				sParametros = sParametros.substring(indice);
				sParametros = 'http://' + location.host + sParametros;

				Android.llamaComponenteEnlace(sParametros);
				setTimeout(function ()
				{
					closeNavegador();
				}, 1000);
				break;
		}
	}
}
/**
 * RESPUESTA WEBSERVICE
 */
function ejecutaWebService(sRuta, sParametros)
{
	if(OSName != "Android"){
		soapData 	= "",
		httpObject 	= null,
		docXml 		= null,
		iEstado 	= 0,
		sMensaje 	= "";
		sUrlSoap 	= "http://127.0.0.1:20044/";
		var bResp	= false;

		soapData =
			'<?xml version=\"1.0\" encoding=\"UTF-8\"?>' +
			'<SOAP-ENV:Envelope' +
			' xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"' +
			' xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\"' +
			' xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"' +
			' xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"' +
			' xmlns:ns2=\"urn:ServiciosWebx\">' +
			'<SOAP-ENV:Body  SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">' +
			'<ns2:ejecutarAplicacion>' +
			'<inParam>' +
			'<Esperar>1</Esperar>' +
			'<RutaAplicacion>' + sRuta + '</RutaAplicacion>' +
			//'<parametros>' + sParametros + '</parametros>'+
			'<parametros><![CDATA[' + sParametros + ']]></parametros>' +
			'</inParam>' +
			'</ns2:ejecutarAplicacion>' +
			'</SOAP-ENV:Body>' +
			'</SOAP-ENV:Envelope>';

		httpObject = getHTTPObject();

		if(httpObject)
		{
			if(httpObject.overrideMimeType)
			{
				httpObject.overrideMimeType("false");
			}

			httpObject.open('POST', sUrlSoap, false); //-- no asincrono
			httpObject.setRequestHeader("Accept-Language", null);
			httpObject.onreadystatechange = function () {
				if (httpObject.readyState == 4 && httpObject.status == 200) {
					parser = new DOMParser();
					docXml = parser.parseFromString(httpObject.responseText, "text/xml");
					iEstado = docXml.getElementsByTagName('Estado')[0].childNodes[0].nodeValue;
					//Llamada a la función que recibe la respuesta del WebService
					setTimeout(function () {
						respuestaWebService(iEstado);
					}, 1000);
					bResp = true;
				}
				else
				{

				}
				return bResp;
			};
			httpObject.send(soapData);
		}
	}
}

function getHTTPObject()
{
    var xhr = false;
    if (window.ActiveXObject) {
        try {
            xhr = new ActiveXObject("Msxml2.XMLHTTP");
        } catch(e) {
            try {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            } catch(e) {
                xhr = false;
            }
        }
    } else if (window.XMLHttpRequest) {
        try {
            xhr = new XMLHttpRequest();
        } catch(e) {
            xhr = false;
        }
    }
    return xhr;
}

/**
 * RESPUESTA WEBSERVICE
 */
function respuestaWebService(iRespuesta) {
	switch (iOpcion) {

		case LIGA_PAGINA:
			if (iRespuesta == 0) {
				setTimeout(function () {
					$('#HtmlMateriaFinanciera').empty();
					$('#HtmlMateriaFinanciera').dialog('close');
					closeNavegador();
				}, 1000);
			}
			else {
				setTimeout(function () {
					closeNavegador();
				}, 2000);
			}
			break;

		case LECTURA_TARGETA:
			{
				switch (iRespuesta) {
					case "0":
						ModalMensaje("Se cancelo la lectura de la tarjeta.");
						break;
					case "-1":
						ModalMensaje("No hay pinpad conectada.");
						break;
					case "-2":
						ModalMensaje("Error al inicializar la pinpad.");
						break;
					case "-3":
						ModalMensaje("Error al leer la tarjeta.");
						break;
					default:
						var sNumTarjeta = "";
						var NumeroTarjeta = "";


						if (OSName == "Android") {
							sNumTarjeta = iRespuesta;
							llamarMateriaFinanaciera();
						} else {
							sNumTarjeta = ObtenerNumeroTarjeta();
						}

						if (sNumTarjeta.toString() != "" && sNumTarjeta.split(" ").join("") != "-1")//Si tenemos un numero de tarjeta
						{
							NumeroTarjeta = sNumTarjeta.substring(12, 16);

							arrSolicitud[0].snumtarjeta = sNumTarjeta;
							arrSolicitud[0].numtarjeta = NumeroTarjeta;

							if (guardarNumeroTarjeta(arrSolicitud[0].folio, arrSolicitud[0].numtarjeta))
								llamarMateriaFinanaciera();//levantar materiaFinanciera
						}
						else {
							var retrievedObject = localStorage.getItem('DeslizarTarjeta');
							/*var expire = function ('DeslizarTarjeta', 100000){
							     		 setTimeout(function ()
										 {localStorage.removeItem(key);
										 }, delay); };*/

							if (!retrievedObject) {

								localStorage.setItem('DeslizarTarjeta', 1);
								alert("Promotor: Favor de deslizar la tarjeta nuevamente ya que no pudo ser leída");

								location.reload(true)
							} else {
								localStorage.removeItem('DeslizarTarjeta');
								alert("Promotor: La tarjeta no puede ser leída, favor de reportarlo a Mesa de Ayuda");
							}

						}
				}
				break;
			}
	}
}
/**************************************<FIN WEBSERVICE EXE>**************************************/
function validarCampos() {
	var bResp = true;
	var iFolEdocta = $("#txtFolioEdocta").val();
	var iAfoCede = $("#cbxAforeCedente option:selected").val();
	var iPeriEsta = $("#cbxPeriodoEstadoCuenta option:selected").val();
	var iTipoTraspaso = $("#cbxTipoTraspaso option:selected").val();

	if(iTipoTraspaso == -1){
		bResp = false;
		ModalMensaje("Indique el tipo de traspaso.");
	}

	if(iFolEdocta.length < 2)//Validar Folio EDOCTA
	{
		bResp = false;
		ModalMensaje("El folio de estado de cuenta no puede estar vacio y debe ser de 20 caracteres.");
	}

	if (iAfoCede == -1)//
	{
		bResp = false;
		ModalMensaje("Debe de seleccionar la afore actual del trabajador.");
	}

	if(iPeriEsta == -1)//
	{
		bResp = false;
		ModalMensaje("Indique el periodo del estado de cuenta.");
	}

	return bResp;
}

function llamarMateriaFinanaciera() {
	var stateObject = {};
	var title = "Captura Afiliacion";
	var newUrl = "tipotraspaso.html";
	history.replaceState(stateObject, title, newUrl);
	materiafinanciera();
}

/*--------------------------------------------Mensajes---------------------------------------------*/
//Cerrar Modal Mensaje
function cerrar() {
	divDlgMain.innerHTML = "";
	divDlgMain.style.display = 'none';
};

//Metodo para Modal
function bloquearModal() {
	sHtml = "</br><img width=\"100%\" class=\"logoafore\" src=\"../imagenes/loadingBar1.gif\">";
	var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
	myDlg.espera(400, 150, 'Espere Un Momento Por Favor...');
	myDlg.mostrar(sHtml);
}

//Metodo para Modal
function desbloquearModal() {
	$("#divDlgMain").hide();//escondemos el div que bloquea pantalla
}

//Mensaje Generales
function ModalMensaje(cMensaje) {
	var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
	myDlg.crear(500, 200, cTituloModal);
	myDlg.mostrar(cMensaje);
}

//Mensaje y Cerrar en Navegador
function MensajeCerrar(sMensaje) {
	var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
	myDlg.modalcierre(500, 250, cTituloModal);
	myDlg.mostrar(sMensaje);
}

//Mensaje y Cerrar en Navegador PREGUNTA
function MensajeCerrarPreg(sMensaje) {
	var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
	myDlg.modalcierremensaje(500, 250, cTituloModal);
	myDlg.mostrar(sMensaje);
}

function closeNavegador() {
	if (OSName == "Android") {
		Android.volverMenu('1');
	} else {
		//firefox
		if (navigator.appName.indexOf('Netscape') >= 0) {
			//NAVEGADOR FIREFOX
			javascript: window.close();
		}
		else {
			if (navigator.appName.indexOf('Microsoft') >= 0) {//internet explorer
				var ventana = window.self;
				ventana.opener = window.self;
				ventana.close();
			}
		}
	}
}

/*--------------------------------------------Información---------------------------------------------*/
//Metodo que obtiene los datos de promotor para ponerlos en la parte superior del html que se obtiene de la una funcion
function obtnerinformacionpromotor(empleado) {
	var bResp = false;
	var arrInformacionProm = new Array();

	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 1, empleado: empleado },
			success: function (data) {
				arrInformacionProm = eval(data);
				//Validacion para saber si esta registrado el promotor o no
				if (arrInformacionProm.iCodigo == 0) {
					$("#txtFechaDia").val(arrInformacionProm.dFechaActual);
					$("#txtUsuario").val(arrInformacionProm.cNombre);
					$("#txtNomTienda").val(arrInformacionProm.cTienda);
					bResp = true;
				}
				else {
					MensajeCerrar(arrInformacionProm.cMensaje);
				}
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});

	return bResp;
}

function obtenerCURP(foliosol) {
	var bResp = false;
	var arrInfoTra = new Array();

	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 2, foliosol: foliosol },
			success: function (data) {
				arrInfoTra = eval(data);
				//Validacion para saber si esta registrado el promotor o no
				if (arrInfoTra.OK == 0) {
					sCURP = arrInfoTra.curp;
					bResp = true;
				}
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});

	return bResp;
}

function comboTipoTraspaso() {
	var arrTipTras = new Array();
	var addOption = '';
	var cbxTipTras = document.getElementById('cbxTipoTraspaso');

	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 3 },
			success: function (data) {
				arrTipTras = eval(data);
				var addOption = "";
				if (arrTipTras.length != 0) {
					for (var i = 0; i < arrTipTras.length; i++) {
						addOption += "<option value='" + arrTipTras[i].clave + "'>" + arrTipTras[i].descripcion + "</option>";
					}
				}
				cbxTipTras.innerHTML = addOption;
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});
}

function comboAforeCedente() {
	var arrAfoCed = new Array();
	var addOption = '';
	var cbxAfoCed = document.getElementById('cbxAforeCedente');

	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 4 },
			success: function (data) {
				arrAfoCed = eval(data);
				var addOption = "<option value='-1'>SELECCIONE ...</option>";
				if (arrAfoCed.length != 0) {
					for (var i = 0; i < arrAfoCed.length; i++) {
						addOption += "<option value='" + arrAfoCed[i].clave + "'>" + arrAfoCed[i].descripcion + "</option>";
					}
				}
				cbxAfoCed.innerHTML = addOption;
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});
}

function comboPeriodoEstado() {
	var arrPerEs = new Array();
	var addOption = '';
	var cbxPerEs = document.getElementById('cbxPeriodoEstadoCuenta');
	var sPeriodo = '';

	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 5 },
			success: function (data) {
				arrPerEs = eval(data);
				var addOption = "<option value='-1'>SELECCIONE ...</option>";
				if (arrPerEs.length != 0) {
					for (var i = 0; i < arrPerEs.length; i++) {
						sPeriodo = '';
						sPeriodo = obtenerPeriodo(arrPerEs[i].cuatrimestre);
						addOption += "<option value='" + arrPerEs[i].cuatrimestre + "'>" + sPeriodo + "</option>";
					}
				}
				cbxPerEs.innerHTML = addOption;
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});
}
function obtenerPeriodo(cuatrimestre) {
	var sPeriodo = '';
	var iYear = 0;
	var iNumero = 0;

	if (cuatrimestre.length > 0)// Si tenemos un codigo de cuatrimestre pero no año ni numero de cuatrimestre
	{
		iYear = cuatrimestre.substring(0, 4);
		iNumero = cuatrimestre.substring(4);
	}

	// Si ya estan asignados el año y el cuatrimestre
	if (iYear > 0 && iNumero > 0) {
		switch (iNumero) // Distinguir el cuatrimetre indicado
		{
			case "1":
				sPeriodo = "Enero a Abril " + iYear;
				break;
			case "2":
				sPeriodo = "Mayo a Agosto " + iYear;
				break;
			case "3":
				sPeriodo = "Septiembre a Diciembre " + iYear;
				break;
			default:
				sPeriodo = '';
				break;
		}
	}
	else // Aun no se asignan el año o el cuatrimestre
	{
		sPeriodo = '';
	}

	return sPeriodo;
}

function CamposASolicitud(foliosol) {
	var arrSol = new Array();
	var EstadoCuentaAforeFolio = "";
	var AforeCedente = 0;
	var CodigoCuatrimestre = 0;

	var iTipoTra = $("#cbxTipoTraspaso option:selected").val();
	var curp = $("#txtCurp").val().toUpperCase();
	var foledocta = $("#txtFolioEdocta").val();

	EstadoCuentaAforeFolio = curp + foledocta;

	if (iTipoTra == 3)//Copia de Estado de Cuenta
	{
		AforeCedente = $("#cbxAforeCedente option:selected").val();
		CodigoCuatrimestre = $("#cbxPeriodoEstadoCuenta option:selected").val();
	}

	arrSol.push
		({
			"tipotras": iTipoTra,
			"folio": foliosol,
			"estadocuenta": EstadoCuentaAforeFolio,
			"afocede": AforeCedente,
			"codicuatr": CodigoCuatrimestre,
			"snumtarjeta": "",
			"numtarjeta": 0
		});

	guardarInfoTraspasoAfiliacion(foliosol, iTipoTra, AforeCedente, EstadoCuentaAforeFolio, CodigoCuatrimestre, AforeCedente);

	return arrSol;
}

function ObtenerNumeroTarjeta() {
	var bResp = "";
	var arrNumTar = new Array();

	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 6 },
			success: function (data) {
				arrNumTar = eval(data);
				if (arrNumTar.respuesta != "-1")
					bResp = arrNumTar.respuesta;
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});

	return bResp;
}

function guardarNumeroTarjeta(foliosol, numtarjeta) {
	//SELECT fnGuardarNumTarjetaTraspaso AS respuesta FROM fnGuardarNumTarjetaTraspaso(FolioSolicitud,'NumeroTarjeta')
	var bResp = false;
	var arrGuarTar = new Array();

	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 7, foliosol: foliosol, numtarjeta: numtarjeta },
			success: function (data) {
				arrGuarTar = eval(data);
				if (arrGuarTar.respuesta == 1)
					bResp = true;
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});

	return bResp;
}

/********************************************************************************************************
 * MATERIA FINANCIERA
 ********************************************************************************************************/
function obtenercodigoHTML() {

	$.ajax(
		{
			async: true,
			cache: true,
			data: { opcion: 1 },
			url: 'php/materiafinanciera.php',
			type: 'POST',
			dataType: 'json',
			success: function (data) {
				aConsultaHtml = eval(data);
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); }
		});
}

function obtenerligapagina(iOpcionPagina, iFolio, iEmpleado) {
	$.ajax(
		{
			async: false,
			cache: true,
			data: { opcion: 2, opcionpag: iOpcionPagina, foliomatfinanciera: iFolio, empleado: iEmpleado },
			url: 'php/materiafinanciera.php',
			type: 'POST',
			dataType: 'json',
			success: function (data) {
				aConsultaLigaPagina = eval(data);
			},

			error: function (a, b, c) {
				alert("error ajax " + a + " " + b + " " + c);
			}
		});
}

function mostrarleyendafinanciera(sCodigo) {
	$("#HtmlMateriaFinanciera").empty();
	$("#HtmlMateriaFinanciera").html(sCodigo);
	$('#HtmlMateriaFinanciera').dialog('open');

	if (aConsultaHtml.iTiempo > 0) {
		setTimeout(function () {
			iFlagBoton = 1;
		}, aConsultaHtml.iTiempo);
	}
	else {
		setTimeout(function () {
			iFlagBoton = 1;
		}, 60000);
	}

}

function materiafinanciera() {
	obtenercodigoHTML();//Obtenemos el codigo HTML y a la vez el tiempo que esperaran los botones para que aparescan y se habiliten
	setTimeout(function () {

		if (aConsultaHtml.sCodigoHtml != ' ') {

			$("#HtmlMateriaFinanciera").dialog
				({
					title: 'Materia Financiera',
					autoOpen: false,
					resizable: false,
					width: '820',
					height: '900',
					modal: true,
					closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
					position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
					open: function (event, ui) {
						$(".ui-dialog-titlebar-close", ui.dialog).hide();
					},
					buttons:
					{
						"Continuar": function () {
							if (iFlagBoton > 0) {
								obtenerligapagina(8, iFolio, iEmpleado);
								if (aConsultaLigaPagina.sDestino.length > 0) {
									iOpcion = LIGA_PAGINA;
									opcionEjecuta(aConsultaLigaPagina.sDestino);
								}
							}
						},
						"Terminar": function () {
							if (iFlagBoton > 0) {
								obtenerligapagina(7, iFolio, iEmpleado);
								if (aConsultaLigaPagina.sDestino.length > 0) {
									iOpcion = LIGA_PAGINA;
									opcionEjecuta(aConsultaLigaPagina.sDestino);
								}
							}
						}
					},
				});

			mostrarleyendafinanciera(aConsultaHtml.sCodigoHtml);
		}
		else {
			alert("Ocurrio un problema al cargar el HTML");
		}
	}, 1000);
}

function guardarInfoTraspasoAfiliacion(foliosol, tipotrasp, folioedo, aforeedo, cuatriedo, aforecedente) {
	var bResp = 0;
	var arrGuardar = new Array();

	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 12, foliosol: foliosol, tipotrasp: tipotrasp, folioedo: folioedo, aforeedo: aforeedo, cuatriedo: cuatriedo, aforeced: aforecedente },
			success: function (data) {
				arrGuardar = eval(data);
				bResp = arrGuardar.respuesta;
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});

	return bResp;
}

// MOVIL
/* Obtener parametros get desde el webview android */
function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search);
	return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
function modalToken() {
	sTitle = "";
	sMensaje = "PROMOTOR: La sesión expiró. Favor de iniciar sesión nuevamente";
	mdlMsjFunc(sTitle, sMensaje);
}

//funcion para desplegar un mensaje
function mdlMsjFunc(sTitle, mensaje) {
	$("#divMensaje").dialog
		({
			title: sTitle,
			autoOpen: true,
			resizable: false,
			width: '350',
			height: 'auto',
			modal: true,
			closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();
				$(this).html("<p>" + mensaje + "</p>");
			},
			buttons:
			{
				"Aceptar": function () {
					cerrarSesion();
				}
			}
		});
}

function cerrarSesion() {
	var ligaMenu = "";

	if (OSName == "Android") {
		Android.cerrarSesion();
	} else {
		ligaMenu = ligaMenuAfore();
		$.ajax({
			async: false,
			cache: false,
			data: { opcion: 20, empleado: iEmpleado },
			url: 'php/casecapturaafiliacion.php',
			type: "POST",
			dataType: "json",
			success: function (data) {
				localStorage.removeItem("tokenAfore");
				location.href = ligaMenu;
			},
			error: function (xhr, status, error) { }
		});
	}
}

function ligaMenuAfore() {
	$.ajax({
		async: false,
		cache: false,
		data: {
			opcion: 21
		},
		url: "php/casecapturaafiliacion.php",
		type: "POST",
		dataType: "json",
		success: function (data) {
			bRetorna = data.respuesta;
		},
		error: function (xhr, status, error) { }
	});

	return bRetorna;
}
function ValidaConsultaRenapoPrevia(cCurp,iEsTitular) 
{
	var bResp = false;
	var arrRenapo = new Array();

	$.ajax
    ({
		async: false,
		cache: false,
		url: 'php/casecapturaafiliacion.php',
		type: 'POST',
		dataType: 'json',
		data:{opcion:24,curp:cCurp,iEsTitular:iEsTitular},
		success: function(data)
		{
			arrRenapo = eval(data);

			//Validacion para saber si fue consultado ante renapo previamente.
			if(arrRenapo.iRenapo == 1)
			{
				bResp = true;
			}
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});

	return bResp;
}