//Variables Estaticas
var cTituloModal		= 'Consulta Afiliación';
var ligaCase			= 'php/caseconsultasolicitudesafiliacion.php';

//Declaracion de Variables
var iEmpleado			= 0;
var iFolioSol			= 0;
var iFolio				= 0;
var sCurp               = "";
var iNss                = 0;
var iNoCliente          = "";
var arrFolios			= new Array();

var OSName="Desconocido";
if (navigator.appVersion.indexOf("Win")!=-1) OSName="Windows";
if (navigator.appVersion.indexOf("Mac")!=-1) OSName="MacOS";
if (navigator.appVersion.indexOf("X11")!=-1) OSName="UNIX";
if (navigator.appVersion.indexOf("Linux")!=-1) OSName="Linux";
if (navigator.appVersion.indexOf("Android")!=-1) OSName="Android";

$(document).ready(function()
{
	$(document).keydown(function(ev)
    {			
		if((ev.keyCode >= 112) && (ev.keyCode <= 123))
		{			
			return false;
		}

		return true;
	});
	bloquearModal();
	//Parametros recibidos por la URL
	iEmpleado = getQueryVariable("empleado");

	//Falta numero de Empleado o no hay datos de este
	if((iEmpleado != "") && (obtnerinformacionpromotor(iEmpleado)))
	{
		desbloquearModal();
	}

	$("#txtFolioSol").keypress(function (e) {
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
			return false;
	});

    $("#txtNSS").keypress(function (e) {
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
			return false;
	});

    $("#txtNoCliente").keypress(function (e) {
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
			return false;
	});

	$("#btnVolver").click(function()
	{
        MensajeCerrarPreg("Seguro que desea Cerrar la Consulta de Solicitud de la Afiliación");
	});

	//Boton para validar la CURP y continuar con el procesos de CAPTURA DE AFILIACION
	$("#btnBuscar").click(function()
    {
        var bResp   = true;

		iFolioSol   = $("#txtFolioSol").val();
        sCurp       = $("#txtCURP").val().toUpperCase();
        iNss        = $("#txtNSS").val();
        iNoCliente  = $("#txtNoCliente").val();

        if(iFolioSol != 0 || sCurp != "" || iNss != 0 || iNoCliente != "")
        {
            if(sCurp.length > 0)
                bResp = validacionesCURP();

            if(iNss.length > 0)
                bResp = validacionesNSS();

            if(bResp)
            {
                if(!obtenerSolicitudAfiliacion(iFolioSol, sCurp, iNss, iNoCliente))
                {
                    ModalMensaje("Promotor: No se encontro ningun folio correspondiente a los parametros indicados.");
                }
            }
        }
        else
        {
            ModalMensaje("Promotor: Debe indicar al menos un parametro de busqueda.");
        }
	});

    $("#btnObtenerFolio").click(function()
	{
        var iFolioSolObt    = 0;
        iFolioSolObt        = $('input[type=radio][name=rdoFolSolAfi]:checked').val();

        if(iFolioSolObt != undefined)
        {
			if(obtenerInfoFolio(iFolioSolObt))
			{
				$("#tableConsultaAfiliacion").hide();
				$("#tableInfoFolio").show();
				document.getElementById("btnImprimir").disabled = false;//Habilitar Boton
			}
			else
			{
				ModalMensaje("PROMOTOR: No se encontro Información Relacionada a este Folio: " + iFolioSolObt);
			}
        }
        else
        {
            ModalMensaje("Promotor: Favor de seleccionar un folio.");
        }
	});

	/*Funciones para la Informacion del Solicitud*/
	$("#btnVolverConsula").click(function()
	{
        $("#tableInfoFolio").hide();
		$("#tableConsultaAfiliacion").show();
	});

	$("#btnImprimir").click(function()
	{
        document.getElementById("btnImprimir").disabled = true;//Desabilitar Boton
		iFolio    		= $('input[type=radio][name=rdoFolSolAfi]:checked').val();
        var iOpcionPag 	= 11;//Levantar Pagina de Impresion
        var iReImpre   	= 0
        var sUrl 		= obtenerRutaImprimir(iOpcionPag,iFolio,iEmpleado,iReImpre);
        if(sUrl != '')
		{
            $.ajax({					   
                url		: sUrl,
                async	: false,
                cache	: false,
                success	: function(data)
                {
					$("#divImpresionAfiliacion").addClass("divImpresionCont").removeClass("none");
                    $("#divImpresionAfiliacion").html(data);
                    //impresiondocimentosafiliacionprincipal(sUrl);
					impresiondocimentosafiliacionconsulta(iEmpleado,iFolio)
                },
                error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
                beforeSend: function(){}
            });

        /*$('#divImpresion').empty();
        $('#divImpresion').dialog('close');*/
		}
		else
		{
			ModalMensaje("Promotor: Intente Imprimir de nuevo, si el problema persiste favor de comunicarce con mesa de ayuda!.");
			document.getElementById("btnImprimir").disabled = false;//Habilitar Boton
		}
	});

});

/*--------------------------------------------General---------------------------------------------*/
//Metodo para extraer los parameteros  de la URL
function getQueryVariable(varGet)
{	
	//Obtener la url compleata del navegador
	var sUrl = window.location.search.substring(1);
	//Separar los parametros junto con su valor
	var vars = sUrl.split("&");
	for (var i=0;i<vars.length;i++) 
	{
		//Obtener el parametro y su valor en arreglo de 2
		var par = vars[i].split("=");
		if(par[0] == varGet) // [0] nombre de variable, [1] valor
		{
			return par[1];
		}
	}
	return(false);
}

//Valida la estructura de la curp
function validarEstructuraCurp(curp)
{
	var esValida = 0;
	if(curp.match(/^([A-Z])([A,E,I,O,U,X])([A-Z]{2})([0-9]{2})([0-1])([0-9])([0-3])([0-9])([M,H])([A-Z]{2})([B,C,D,F,G,H,J,K,L,M,N,Ñ,P,Q,R,S,T,V,W,X,Y,Z]{3})([0-9,A-Z])([0-9])$/i))
	{
		esValida = 1;
	}  
	return esValida;
}

function validacionesCURP()
{
	bResp = false;
	var sCurp = $('#txtCURP').val().toUpperCase();

    if(sCurp.length > 0)
    {
        if (sCurp.length == 18)
        {

            if(validarEstructuraCurp(sCurp))
            {
                bResp = true;
            }
            else
            {
                ModalMensaje("CURP no Valida.");
            }
        }
        else
        {
            ModalMensaje("Favor de Agregar la CURP con 18 caracteres.");
        }
    }

	return bResp;
}

function validacionesNSS()
{
	bResp = false;
	var iNssVal = $('#txtNSS').val();

	if (iNssVal.length > 0)
	{
		if (iNssVal.length == 11)
        {
            bResp = true;
        }
        else
        {
            ModalMensaje("Favor de Agregar el NSS con 11 caracteres.");
        }
	}
    
	return bResp;
}

/*--------------------------------------------Mensajes---------------------------------------------*/
//Cerrar Modal Mensaje
function cerrar()
{
	divDlgMain.innerHTML="";
	divDlgMain.style.display = 'none';
};

//Metodo para Modal
function bloquearModal()
{
	sHtml= "</br><img width=\"100%\" class=\"logoafore\" src=\"../imagenes/loadingBar1.gif\">";	
    var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
	myDlg.espera(400,150,'Espere Un Momento Por Favor...');
	myDlg.mostrar(sHtml);
}

//Metodo para Modal
function desbloquearModal() 
{
   //escondemos el div que bloquea pantalla
   $("#divDlgMain").hide();
}

//Mensaje Generales
function ModalMensaje(cMensaje) 
{
	var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
	myDlg.crear(500,200, cTituloModal);
	myDlg.mostrar(cMensaje);
}

//Mensaje y Cerrar en Navegador
function MensajeCerrar(sMensaje) 
{
	var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
	myDlg.modalcierre(500,250,cTituloModal);
	myDlg.mostrar(sMensaje);
}

//Mensaje y Cerrar en Navegador PREGUNTA
function MensajeCerrarPreg(sMensaje) 
{
	var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
	myDlg.modalcierremensaje(500,250,cTituloModal);
	myDlg.mostrar(sMensaje);
}

/*--------------------------------------------Información---------------------------------------------*/
//Metodo que obtiene los datos de promotor para ponerlos en la parte superior del html que se obtiene de la una funcion
function obtnerinformacionpromotor(empleado)
{
	var bResp = false;
	var arrInformacionProm = new Array();
	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:1,empleado:empleado},
		success: function(data)
		{
			arrInformacionProm = eval(data);
			//Validacion para saber si esta registrado el promotor o no
			if(arrInformacionProm.iCodigo == 0)
			{
				$("#txtFechaDia").val(arrInformacionProm.dFechaActual);
				$("#txtUsuario").val(arrInformacionProm.cNombre);
				$("#txtNomTienda").val(arrInformacionProm.cTienda);
				bResp = true;
			}
			else
			{
				MensajeCerrar(arrInformacionProm.cMensaje);
			}
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});

	return bResp;
}

function obtenerSolicitudAfiliacion(foliosol, curp, nss, nocliente)
{
    var bResp       = false;
	var arrSolAfi   = new Array();
	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:2, foliosol:foliosol, curp:curp, nss:nss, nocliente:nocliente},
		success: function(data)
		{
			arrSolAfi = eval(data);
			if(arrSolAfi.length > 0)
			{
				bResp = true;
                mostrarSolicitudes(arrSolAfi);
			}
            else
            {
                $(".tbMostrarFolios").hide();
            }
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});

	return bResp;
}

function mostrarSolicitudes(arrsolafi)
{
    var tbADD = document.getElementById('tbSolEnc');
    var tbBODY = "";
    $(".tbMostrarFolios").show();
    for(var i = 0;  i <= arrsolafi.length-1; i++)
    {
        if(arrsolafi[i].folio.length != 0)
		{
			tbBODY +=
			"<tr>"+
				"<td>" + arrsolafi[i].folio + "<input name='rdoFolSolAfi' type='radio' value='" + arrsolafi[i].folio + "'/></td>" +
				"<td>" + arrsolafi[i].fechacaptura + "</td>" +
				"<td>" + arrsolafi[i].tipo + "</td>" +
				"<td>" + arrsolafi[i].digitalizada + "</td>" +
				"<td>" + arrsolafi[i].estado + "</td>" +
				"<td>" + arrsolafi[i].nombrecliente + "</td>" +
				"<td>" + arrsolafi[i].curp + "</td>" +
				"<td>" + arrsolafi[i].nss + "</td>" +
				"<td>" + arrsolafi[i].nocliente + "</td>" +
				"<td>" + arrsolafi[i].promotor + "</td>" +
				"<td>" + arrsolafi[i].tienda + "</td>" +
			"</tr>";
			
			arrFolios.push
			({
				"foliosol":		arrsolafi[i].folio,
				"fechacaptura":	arrsolafi[i].fechacaptura,
				"digitalizada":	arrsolafi[i].digitalizada,
			});
		}
		else//YPPM
		{
			ModalMensaje("Promotor: No se encontro ningun folio correspondiente a los parametros indicados.");
			$(".tbMostrarFolios").hide();
		}
    }

    tbBODY += "</tbody>";
    tbADD.innerHTML = tbBODY;
}

function obtenerInfoFolio(foliosoli)
{
	var bResp = false;
	//Obtener Información General
	if(obtenerDatosSolicitudAfiliacionGeneral(foliosoli))
	{
		obtenerDatosSolicitudAfiliacionTelefonos(foliosoli);
		obtenerDatosSolicitudAfiliacionBeneficiarios(foliosoli);
		// obtenerFechaRendimiento(foliosoli);
		bResp = true;
	}

	return bResp;
}

function obtenerDatosSolicitudAfiliacionGeneral(foliosoli)
{
	var bResp       = false;
	var arrDaSolGen = new Array();
	var sDiagnostico = '';
	
	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:3, foliosol:foliosoli},
		success: function(data)
		{
			arrDaSolGen = eval(data);
			if(arrDaSolGen.length > 0 && arrDaSolGen[0].error == 1)
			{
				bResp = true;

				if(arrDaSolGen[0].finado == 1)
					$('#cbxFinado').prop('checked', true);

				$("#txtTipoSolicitud").val(arrDaSolGen[0].tiposolicituddesc);
				$("#txtFolioSolicitud").val(arrDaSolGen[0].folio);
				$("#txtFolioTraspaso").val(arrDaSolGen[0].foliodetraspaso);
				$("#txtEstatus").val(arrDaSolGen[0].estatus);
				$("#txtApellidoPaterno").val(arrDaSolGen[0].apaterno);
				$("#txtApellidoMaterno").val(arrDaSolGen[0].amaterno);
				$("#txtNombre").val(arrDaSolGen[0].nombres);
				$("#txtFechaNacimiento").val(arrDaSolGen[0].fechanac);
				$("#txtSexo").val(arrDaSolGen[0].sexo);
				$("#txtCurp").val(arrDaSolGen[0].curp);
				$("#txtNSSIn").val(arrDaSolGen[0].nss);
				$("#txtRFC").val(arrDaSolGen[0].rfc);
				$("#txtNacionalidad").val(arrDaSolGen[0].nacionalidad);
				$("#txtEntidadNacimiento").val(arrDaSolGen[0].entidadnac);
				$("#txtEstadoCivil").val(arrDaSolGen[0].estadocivil);
				$("#txtPrioridadParticular").val(arrDaSolGen[0].prioridad);
				$("#txtCorreoElectronico").val(arrDaSolGen[0].email);
				$("#txtNvlEstudio").val(arrDaSolGen[0].nivelestudio);
				$("#txtComentario").val(arrDaSolGen[0].comentarios);

				var iTipoDomPart 	= arrDaSolGen[0].tipodomicilio;
				var iCompDom		= arrDaSolGen[0].compdomicilio;
				var iOcupacionProf 	= arrDaSolGen[0].ocupacionprof;
				var iActividadNeg 	= arrDaSolGen[0].activinegocio;

				if(arrDaSolGen[0].aportaciones == 1)
					$('#cbxConstanciaAportaciones').prop('checked', true);
				
				if(arrDaSolGen[0].huelladigital == 1)
					$('#cbxHuellaDigi').prop('checked', true);

				if(iTipoDomPart != 0)
				{
					obtenerDomicilio(foliosoli,iTipoDomPart);//Obtener Domicilio Puesto en la COLSOLICITUDES
					obtenerDomicilio(foliosoli,1);//Obtener Domicilio Laboral si lo tiene
				}

				obtenerComprobanteDomicilio(iCompDom);
				obtenerOcupacionProf(iOcupacionProf);
				obtenerActividadGiro(iActividadNeg);
				
				$("#txtDiagnostico").val(obtenerDiagnosticoSolicitud(foliosoli));
				sDiagnostico = $('#txtDiagnostico').val();
				
				if (sDiagnostico.trim().length <= 0)
				{
					$("#txtDiagnostico").hide();
					$("#lblDiagnostico").hide();
				}
			}
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});

	return bResp;
}

function obtenerDomicilio(foliosoli,tipodom)
{
	var bResp 	= false;
	var arrDom 	= new Array();
	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:4, foliosol:foliosoli,tipodom:tipodom},
		success: function(data)
		{
			arrDom = eval(data);
			if(arrDom.length > 0)
			{
				bResp = true;
				if(tipodom == 1)
					datosDomicilioLaboral(arrDom);
				else
					datosDomicilioParticular(arrDom);
			}
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});

	return bResp;
}

function datosDomicilioParticular(arrdompart)
{
	$("#txtTipoDomicilioParticular").val(arrdompart[0].tipodom);
	$("#txtCalleParticular").val(arrdompart[0].calle);
	$("#txtNumExteriorParticular").val(arrdompart[0].numext);
	$("#txtNumInteriorParticular").val(arrdompart[0].numint);
	$("#txtCodigoPostalParticular").val(arrdompart[0].codpostal);
	$("#txtPaisParticular").val(arrdompart[0].pais);
	$("#txtEstadoParticular").val(arrdompart[0].estado);
	$("#txtMunicipioParticular").val(arrdompart[0].delegnmun);
	$("#txtCiudadParticular").val(arrdompart[0].ciudad);
	$("#txtColoniaParticular").val(arrdompart[0].colonia);
}

function datosDomicilioLaboral(arrdomlab)
{
	$("#txtTipoDomicilioLaboral").val(arrdomlab[0].tipodom);
	$("#txtCalleLaboral").val(arrdomlab[0].calle);
	$("#txtNumExteriorLaboral").val(arrdomlab[0].numext);
	$("#txtNumInteriorLaboral").val(arrdomlab[0].numint);
	$("#txtCodigoPostalLaboral").val(arrdomlab[0].codpostal);
	$("#txtPaisLaboral").val(arrdomlab[0].pais);
	$("#txtEstadoLaboral").val(arrdomlab[0].estado);
	$("#txtMunicipioLaboral").val(arrdomlab[0].delegnmun);
	$("#txtCiudadLaboral").val(arrdomlab[0].ciudad);
	$("#txtColoniaLaboral").val(arrdomlab[0].colonia);
}

function obtenerComprobanteDomicilio(compdom)
{
	var arrComDom 	= new Array();
	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:5, compdom:compdom},
		success: function(data)
		{
			arrComDom = eval(data);
			arrComDom.comprobante = arrComDom.comprobante.replace('&Aacute;','Á');
			arrComDom.comprobante = arrComDom.comprobante.replace('&Eacute;','É');
			arrComDom.comprobante = arrComDom.comprobante.replace('&Iacute;','Í');
			arrComDom.comprobante = arrComDom.comprobante.replace('&Oacute;','Ó');
			arrComDom.comprobante = arrComDom.comprobante.replace('&Uacute;','Ú');

			$("#txtComprobanteParticular").val(arrComDom.comprobante);
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});
}

function obtenerDatosSolicitudAfiliacionTelefonos(foliosoli)
{
	var arrTelef 	= new Array();
	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:6, foliosol:foliosoli},
		success: function(data)
		{
			arrTelef = eval(data);
			if(arrTelef.length > 0)
			{
				for (var i = 0; i < arrTelef.length; i++) {
					$("#txtTipoTel"+(i+1)).val(arrTelef[i].tipocontacto);
					$("#txtLadaTel"+(i+1)).val(arrTelef[i].lada);
					$("#txtTelTel"+(i+1)).val(arrTelef[i].telefono);
					$("#txtExtTel"+(i+1)).val(arrTelef[i].extensiont);					
				}
			}
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});
}

function obtenerOcupacionProf(ocupacionprof)
{
	var arrOcuPro 	= new Array();
	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:7, ocupacionprof:ocupacionprof},
		success: function(data)
		{
			arrOcuPro = eval(data);
			$("#txtOcupacion").val(arrOcuPro.ocupacion);
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});
}

function obtenerActividadGiro(activinegocio)
{
	var arrActGiro 	= new Array();
	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:8, activinegocio:activinegocio},
		success: function(data)
		{
			arrActGiro = eval(data);
			$("#txtActivavGiro").val(arrActGiro.actividad);
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});
}

function obtenerDatosSolicitudAfiliacionBeneficiarios(foliosoli)
{
	var arrBene = new Array();
	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:9, foliosol:foliosoli},
		success: function(data)
		{
			arrBene = eval(data);
			if(arrBene.length > 0)
			{
				for (var i = 0; i < arrBene.length; i++) {
					$("#txtApPaternoBene"+(i+1)).val(arrBene[i].apaterno);
					$("#txtApMaternoBene"+(i+1)).val(arrBene[i].amaterno);
					$("#txtNombBene"+(i+1)).val(arrBene[i].nombres);
					$("#txtProceBene"+(i+1)).val(arrBene[i].porcentaje);
					$("#txtParentescoBene"+(i+1)).val(arrBene[i].parentesco);
				}
			}
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});
}

function obtenerFechaRendimiento(foliosol)
{
	var dFechaRen 		= '';
	var dFechaActual 	= '';
	var fechaCaptura	= '1900-01-01';
	var digitalizada 	= '';
	var arrDate 		= new Array();

	$.ajax
	({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data: { opcion: 16},
		success: function (data) {
			arrDate 		= eval(data);
			dFechaActual 	= arrDate.fechadia;
			dFechaRen 		= arrDate.fecharen;
		},
		error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
		beforeSend: function () { }
	});

	if(arrFolios.length > 0)
	{
		for (var i = 0; i < arrFolios.length; i++) 
		{
			if(arrFolios[i].foliosol == foliosol)
			{
				fechaCaptura = arrFolios[i].fechacaptura;
				digitalizada = arrFolios[i].digitalizada;
			}
		}
	}

	$("#txtFechaRendimiento").val(dFechaRen);

	if ((fechaCaptura == dFechaActual) && (digitalizada != 'SI'))
		$("#btnImprimir").show();
	else
		$("#btnImprimir").hide();
}

function obtenerRutaImprimir(opcionpag,foliosol,empleado,reimpre)
{
	var bResp 		= '';
	var arrObtLiga 	= new Array();
	
	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:13,opcionpag:opcionpag,foliosol:foliosol,empleado:empleado,reimpre:reimpre},
		success: function(data)
		{
			arrObtLiga = eval(data);
			bResp = arrObtLiga.destino;
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});

	return bResp;
}

function obtenerDiagnosticoSolicitud(foliosoli)
{
	var arrResp 	= new Array();
	var cDiagSol = '';
	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:10, foliosol:foliosoli},
		success: function(data)
		{
			arrResp = eval(data);
			if(arrResp.diagnosticosolicitud.length > 0)
			{
				cDiagSol = arrResp.diagnosticosolicitud;
			}
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});

	return cDiagSol;
}