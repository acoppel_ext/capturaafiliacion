//Variables Estaticas
var ligaCase 				= 'php/casemotivoafiliacion.php';
//Declaracion de Variables
var iEmpleado 				= 0;
var iFolio 					= 0;
var cTituloModal 			= 'Captura Afiliación';
var iTipoTraspaso 			= 0;
var sTipoSol 				= '';
var tipoAfi 				= 0;//Tipo Solicitud
var iRenapo 				= 0;
//Obtener el día Actual
var dFechaActualHora 		= '';

////Parte de la INE
var validaINE 				= 0;
var statusEnvio 			= 1;
var intentos 				= 0;
var opcioncombo 			= 1;
var cCurp 					= '';
var statusRealizado 		= 2;
var statusEnvio 			= 2;
var statusSmsRealizado 		= 0;
//INE 2
var codigoautenticacionIvr 	= '';
var telefonoINE 			= '';
var cLigaLamadaIVR 			= '';
var generaCodigo 			= '';
var banderabotenerparaIVR 	= 0;
var contadorDetonacionCodigoAu = 0;
//MEDIO DE ENVIO
var iEnvioAutenticacion 	= 0; //1-INE / 2-BCPL

//WS
var intentosWS 				= 0;
var tiempoWS 				= 0;
var keyxWs 					= 0;
var nIntentos 				= 0;
var mensajeWs 				= '';
var folioAnterior 			= '';
var sCurpTitular = '';
var ligaFrame = '';

var tiposolicitante = 0;
var codigoautenticacion = '';
var validacodautentica = 0;
var isTercerasFiguras = false;
//Instruccion que permite conocer el sistema operativo de donde ingresa el usuario
var OSName = "Desconocido";
if (navigator.appVersion.indexOf("Win") 	!= -1) OSName = "Windows";
if (navigator.appVersion.indexOf("Mac")		!= -1) OSName = "MacOS";
if (navigator.appVersion.indexOf("X11") 	!= -1) OSName = "UNIX";
if (navigator.appVersion.indexOf("Linux") 	!= -1) OSName = "Linux";
if (navigator.appVersion.indexOf("Android") != -1) OSName = "Android";

$(document).ready(function () {
	var divRespuestaRenapo 	= document.getElementById('divRespuestaRenapo');
	var addRespuesta = '';
	$(document).keydown(function(ev)
    {			
		if((ev.keyCode >= 112) && (ev.keyCode <= 123))
		{			
			return false;
		}

		return true;
	});
	bloquearModal();
	obtenerfecha();

	$("#bntCodigoautenticacion").hide();
	$("#lblCodigoAu").hide();
	document.getElementById("lblCodigoAu").disabled = true;


	document.getElementById("lblMot").disabled = true;
	document.getElementById("cbxMotReg").disabled = true;

	$("#lblMot").hide();
	$("#cbxMotReg").hide();

	$("#txtMot").hide();
	$("#lblMotReg").hide();
	
	document.getElementById("MotivoTraspaso").disabled = true;
	$("#MotivoTraspaso").hide();

	


	/*Obtener Variables pasadas por URL*/
	if (OSName == "Android") {
		iEmpleado = getParameterByName("empleado");
		iFolio = getParameterByName("folio");
		var MotReg = document.getElementById("cbxMotReg");
		MotReg.style.backgroundColor = "white";
	} else {
		iEmpleado = getQueryVariable("empleado");
		iFolio = getQueryVariable("folio");
	}

	//INE//Se toma el valor que devuelve la funcion para validar el status del envio del mensaje o llamada del codigo autenticación.
	statusSmsRealizado = fnvalidastatusenvioine(iFolio);

	//INE//Se asigna el valor obtenido de la funcion a la variable
	cCurp = fnobtenercurpconstancia(iFolio);
	sCurpTitular = obTenercurpTitular(iFolio);
	if(ValidaConsultaRenapoPrevia(sCurpTitular,1))
	{
		if(divRespuestaRenapo != null)
		{
			divRespuestaRenapo.innerHTML = '';
			addRespuesta += '<input style="cursor:default" autocomplete="off" type = "image" src="imagenes/RenapoExitoso.png" width = "132" height = "28"></input>';
			divRespuestaRenapo.innerHTML = addRespuesta;
			iRenapo = 1;
		}
	}
	//INE//
	generarFolioIne(cCurp, iFolio);

	//INE//
	validaINE = validacionine(iFolio);


	// obtener liga de imprecion de documentos.
	ligaFrame = obtenerLigaImprecionDocs();

	//0-DAFULT | 1-AUTENTICACION INE | 2-AUTENTICACION BANCOPPEL
	iEnvioAutenticacion = validaINE;

	if (validaINE>0) {
		console.log("Entro FOLIO");
		folioINE = fnobtenerfolioine(iFolio);
		cLigaLamadaIVR = fnobtenerLigaIVR();

		console.log(cCurp,folioINE);
		fnobtenerparaivr(cCurp, folioINE);
	}

	//Falta numero de Empleado o no hay datos de este
	if ((iEmpleado != "") && (obtnerinformacionpromotor(iEmpleado))) {
		obtenerConfiguracionWs();
		desbloquearModal();
	}

	llenarcbxMotivoRegistro();

	//Validar que Tipo de Afiliacion es Registro / Traspaso
	tipoAfi = validarFolioSolicitud(iFolio);

	// LABEL
	$("#divFolConst").show();
	var lblConst = document.getElementById('lblFolConst');
	var lblConstImplica = document.getElementById('lblFolConstImpli');
	var lblMotTras = document.getElementById('lblMot');

	sTipoSol = "";


	// TODO: borrar
	/*validaINE = 1;
	tipoAfi = 27;*/

	if (tipoAfi == 0) {
		// TODO: descomentar
		MensajeCerrar("Ocurrio una excepcion al obtener información de la solicitud del Trabajador y el tipo de afiliación.");
	}
	//Mostrar Opciones segun el Tipo de Solicitud
	else if (tipoAfi == 26)//Registro IMSS
	{
		console.log("registro imss -> ",tipoAfi, validaINE);


		/**
         *Mostrar:
            *txt Folio Constancia de Registro
            *slc *Motivo Registro (si slecciona OTRO mostrar txt Motivo)
         **/
		$("#divFolConst").show();
		lblConst.innerHTML = 'Folio de Solicitud: ';
//		lblMotTras.innerHTML = '* Motivo de Registro: ';
		sTipoSol = "Registro";

		$("#divFolConst2").show();
//		lblMotTras.innerHTML = '* Motivo de Registro: ';
		sTipoSol = "Registro";
	}
	else if (tipoAfi == 33)//Registro Independiente
	{
		console.log("registro idpendiente -> ",tipoAfi, validaINE);

        /**
         *Mostrar:
            *txt Folio Constancia de Registro
            *slc *Motivo Registro (si selecciona OTRO mostrar txt Motivo)
            *slc *Tipo Administración
            *rdo *Deposito
         **/
		$("#divFolConst").show();
		$("#divFolConst2").show();
		$("#divTipoAdmin").show();
		$("#divDeposito").show();
		$("#divDeposito1").show();
		$("#divDeposito2").show();

		if (validaINE > 0) {
			
			$("#lblCodigoAu").css("visibility", "visible");

			$("#btnCodigoAu").css("visibility", "visible");
			$("#btnAutenticacion").css("display", "");
		}

		lblConst.innerHTML = 'Folio de Solicitud: ';
	//	lblMotTras.innerHTML = '* Motivo de Registro: ';
		sTipoSol = "Registro";

	//	lblMotTras.innerHTML = '* Motivo de Registro: ';
		sTipoSol = "Registro";
		llenarcbxTipoAdmin();
	}
	else//Traspaso IMSS/ISSSTE/Independiente
	{
		console.log("Traspaso IMSS/ISSSTE/Independiente -> ",tipoAfi, validaINE);
        /**
         *Mostrar:
            *txt Folio Constancia de Traspaso
            *txt Folio Constancia sobre implicaciones de Traspaso
            *slc *Motivo Registro (si slecciona OTRO mostrar txt Motivo)
         **/
		$("#divFolConst").show();
		//$("#divFolConstImpli").show();
		lblConstImplica.innerHTML = 'Folio Constancia sobre implicaciones de Traspaso: ';
		lblConst.innerHTML = 'Folio de Solicitud: ';
		lblMotTras.innerHTML = '* Motivo de Traspaso: ';
		sTipoSol = "Traspaso";
		//INE//
		lblCodigoAu.innerHTML = "Código de Autenticación ";
		
		console.log(" tipo solicitud : "+sTipoSol);
		
			$("#lblMot").show();
			$("#cbxMotReg").show();

			$("#txtMot").show();
			$("#lblMotReg").show();

			document.getElementById("lblMot").disabled = false;
			document.getElementById("cbxMotReg").disabled = false;
			
		document.getElementById("MotivoTraspaso").disabled = false;
		$("#MotivoTraspaso").show();
			
			if(sTipoSol == "Traspaso")
		{
			document.getElementById("cbxMotReg").disabled = true;
		}

	}


	//INE//valida si se cuenta con autenticacion si es asi muestra lo siguiente
	if (validaINE > 0 && validaINE !=3) {
		$("#lblCodigoAu").show();
		document.getElementById("lblCodigoAu").disabled = false;
		$("#lblCodigoAu").css("visibility", "visible");
		$("#txtCodigoAu").css("visibility", "visible");
		$("#btnCodigoAu").css("visibility", "visible");
		$("#btnAutenticacion").css("display", "");

		var codigo = fnobtenersemillaafiliacion(iFolio);
		if(codigo  != '') {
			$("#txtCodigoAu").val(codigo);
			isTercerasFiguras = true;
		}
	}

	$("#divFolConstImpli").show();



	$("#cbxMotReg").change(function () {
		var iMotRef = $("#cbxMotReg option:selected").val();

		if (iMotRef != -1)
			document.getElementById("btnLlmadaCat").disabled = false;
		else {
			document.getElementById("btnLlmadaCat").disabled = true;
			document.getElementById("btnGuardar").disabled = true;
		}

		if (iMotRef == 0)
			document.getElementById("txtMot").disabled = false;
		else {
			var txtMOT = document.getElementById("txtMot");
			txtMOT.disabled = true;
			txtMOT.value = '';
		}
	});

	//No permita Caracteres especiales, solo numeros y letras
	$("#txtFolConstaImpli").keypress(function (event) {
		keyRE = /[0-9A-Za-z]/;
		if ((typeof (event.keyCode) != 'undefined' && event.keyCode > 0 && String.fromCharCode(event.keyCode).search(keyRE) != (-1)) ||
			(typeof (event.charCode) != 'undefined' && event.charCode > 0 && String.fromCharCode(event.charCode).search(keyRE) != (-1)) ||
			(typeof (event.charCode) != 'undefined' && event.charCode != event.keyCode && typeof (event.keyCode) != 'undefined' && event.keyCode.toString().search(/^(8|9|13|45|46|35|36|37|39)$/) != (-1)) ||
			(typeof (event.charCode) != 'undefined' && event.charCode == event.keyCode && typeof (event.keyCode) != 'undefined' && event.keyCode.toString().search(/^(8|9|13)$/) != (-1))) {
			return true;
		}
		else {
			return false;
		}
	});

	/*$("#btnLlmadaCat").click(function () {

		var llamadaCat = document.getElementById("btnLlmadaCat");
		var btnGuardar = document.getElementById("btnGuardar");//Habilitar Boton Guardar
	});*/

	$("#btnLlmadaCat").click(function () {
        bloquearModal();
        var llamadaCat = document.getElementById("btnLlmadaCat");
        var btnGuardar = document.getElementById("btnGuardar"); //Habilitar Boton Guardar
        llamadaCat.disabled = true; //Desabilitar Boton CAT
        if (validarCamposMotivoFolioImplicacion(sTipoSol, tipoAfi)) //Son validos
        {
            console.log("itipoTraspaso:", iTipoTraspaso);
            if (guardarDatosMotivoAfiliacion(iFolio, tipoAfi, iTipoTraspaso)) {
                $(".disaCompo").prop('disabled', true); //Desabilitar todos los componenetes por Nombre de Clase
                if (validaINE > 0 && validaINE !=3) {
                    contadorDetonacionCodigoAu = 0;
                    document.getElementById("btnAutenticacion").disabled = false;
                    if (isTercerasFiguras) {
                        document.getElementById("btnAutenticacion").disabled = true;
                        document.getElementById("btnGuardar").disabled = false;
                    } else if (statusSmsRealizado > 0) {
                        document.getElementById("btnAutenticacion").disabled = false;
                        document.getElementById("txtCodigoAu").disabled = false;
                        document.getElementById("btnCodigoAu").disabled = false;
                    }
                } else {
					
                    btnGuardar.disabled = false;
				
					
                }
                //AGREGA LOS DATOS DE LA SOLICITUD AL WS PARA CONSULTA DE FOLIO
                var resp = agregarDatosConsultaFolio(1);
            }
        } else {
            llamadaCat.disabled = false; //Habilitar Boton CAT
        }
    });
	//INE//boton que valida la fecha vigencia de el foliosemilla o si el folio es incorrecto
	$("#btnCodigoAu").click(function () {
		var validaine = "";
		var btnGuardar = document.getElementById("btnGuardar");//Habilitar Boton Guardar
		var btnAutentica = document.getElementById("btnCodigoAu");
		var txtcodigo = document.getElementById("txtCodigoAu");
		var foliosem = $('#txtCodigoAu').val().toUpperCase();
		var btnAutenticacion = document.getElementById("btnAutenticacion");
		validaine = fnvalidavigenciasemilla(iFolio, foliosem);
		txtcodigo.disabled = false;
		intentos++;

		if (foliosem.length != '') {

			if (foliosem.length == 7) {

				if (validaine == 1) {
					btnGuardar.disabled = false;
					btnAutentica.disabled = true; //boton autentica bloqueado si la respuesta es positva osea cumple
					txtcodigo.disabled = true;
				}
				else if (validaine == -1) {
					ModalMensaje('El código capturado no fue generado por AFORE');
				}
				else if (validaine == -2) {
					ModalMensaje('El código no corresponde al trabajador indicado');
				}
				else if (validaine == -3) {
					ModalMensaje('Promotor, el Código de Autenticación capturado no se encuentra vigente, favor de verificarlo y volver a intentar o iniciar un nuevo trámite');
					intentos = 0;
					btnAutenticacion.disabled = false;
					txtcodigo.disabled = true;
					btnAutentica.disabled = true;
					$('#txtCodigoAu').val('');
				}
				else if (validaine == -4) {
					ModalMensaje('El código de autenticación no es valido');
					if (intentos == 2) {
						ModalMensaje('Promotor, Vuelva a generar el código de autenticación');
						intentos = 0;
						btnAutenticacion.disabled = false;
						txtcodigo.disabled = true;
						btnAutentica.disabled = true;
						$('#txtCodigoAu').val('');
					}
				}
				else if (validaine == -5) {
					ModalMensaje('Promotor, el Código de Autenticación no coincide, favor de verificar');
					if (intentos == 2) {
						ModalMensaje('Promotor, Vuelva a generar el código de autenticación');
						intentos = 0;
						btnAutenticacion.disabled = false;
						txtcodigo.disabled = true;
						btnAutentica.disabled = true;
						$('#txtCodigoAu').val('');
					}
				}
				else {
					btnGuardar.disabled = true;
					btnautentica.disabled = false;
				}
			}
			else {
				ModalMensaje('Promotor, favor de capturar correctamente el Código de Autenticación');
				if (intentos == 2) {
					ModalMensaje('Promotor, Vuelva a generar el código de autenticación');
					intentos = 0;
					btnAutenticacion.disabled = false;
					txtcodigo.disabled = true;
					btnAutentica.disabled = true;
					$('#txtCodigoAu').val('');
				}
			}
		}
		else {
			ModalMensaje('Promotor, favor de capturar el Código de Autenticación');
			if (intentos == 2) {
				ModalMensaje('Promotor, Vuelva a generar el código de autenticación');
				intentos = 0;
				btnAutenticacion.disabled = false;
				txtcodigo.disabled = true;
				btnAutentica.disabled = true;
				$('#txtCodigoAu').val('');
			}
		}

	});


	$("#btnGuardar").click(function () {
		var retorno = '';
		var retorno2 = '';
		var folioSol = $('#txtFolConsta').val().toUpperCase();
		folioAnterior 	= $('#txtFolConsta').val().toUpperCase();
		document.getElementById("txtFolConsta").disabled = true;

		if (folioSol != '') {
			if (folioSol.length == 6) {
					document.getElementById("btnGuardar").disabled = true;//Desabilitar Boton
					bloquearModal();
					if (iFolio > 0)//Validar que tengamos un folio
					{
						if (validarFolioSol(iFolio)) //validamos que el folio no exista en la colsolicitudes
						{
							retorno2 = existenciafechaProcesar(iFolio);
							retorno = existenciafolioprocesar(iFolio, folioSol);

							console.log("retornos de guardado:",retorno2, retorno);

							if (retorno2 == 1) {
								if (retorno == 1) {
									if (guardarSolicitudBD(iFolio, iEmpleado)) {
										//FOLIO 877.1
										bloquearModal();
										setTimeout(function () {
											validarDatosConsultarFolio();
										}, tiempoWS);
									}
									else {
										ModalMensaje("Ocurrio un problema al guardar en la base de datos.");
										document.getElementById("btnGuardar").disabled = false;
										document.getElementById("btnCancelar").disabled = false;

									}

								}
								else if (retorno == 0) {
									ModalMensaje("Ocurrio un problema al guardar el folio recibido en la base de datos.");
									document.getElementById("btnGuardar").disabled = false;
									document.getElementById("btnCancelar").disabled = false;//Habilitar Boton
								}
								else {
									ModalMensaje('Promotor, el folio capturado ya fue utilizado anteriormente, favor de verificarlo');
									document.getElementById("btnGuardar").disabled = false;
									document.getElementById("btnCancelar").disabled = false;//Habilitar Boton
									document.getElementById("txtFolConsta").disabled = false;
									$('#txtFolConsta').val('');
								}
							}
							else if (retorno2 == -1) {
								ModalMensaje('Promotor, la vigencia del folio para este Trabajador no es  válida, favor de realizar una nueva captura de datos');
								document.getElementById("btnGuardar").disabled = false;
								document.getElementById("btnCancelar").disabled = false;//Habilitar Boton
							}
							else if (retorno2 == -2) {
								ModalMensaje('Promotor, no se ha recibido una fecha de vigencia de folio para este  Trabajador');
								document.getElementById("btnGuardar").disabled = false;
								document.getElementById("btnCancelar").disabled = false;//Habilitar Boton
							}
							else { ModalMensaje("Ocurrio un problema al guardar en la base de datos."); }
						}
						else {
							console.log("else de validarFolioSol");
							ModalMensaje("PROMOTOR: Ocurrió un problema al guardar en base de datos, no se podrá continuar con la afiliación. Favor de reportar a Mesa de Ayuda.");
						}
					}
					else {
						document.getElementById("btnGuardar").disabled = false;
						MensajeCerrar("Ocurrio un problema al obtener el folio, Favor de comunicarse a Mesa de Ayuda.");
					}
				}
				else {
					ModalMensaje("Promotor, favor de capturar correctamente el folio que recibio el trabajador.");
					document.getElementById("cbxMotReg").disabled 		= false;
					document.getElementById("txtFolConsta").disabled 	= false;
					$('#txtFolConsta').val('');
				}
			}
			else {
				ModalMensaje("Promotor, favor de capturar correctamente el folio que recibio el trabajador.");
				document.getElementById("cbxMotReg").disabled 	= false;
				$('#txtFolConsta').val('');
				document.getElementById("txtFolConsta").disabled = false;
			}
	});

	$("#btnListadoDocumentos").click(function () {
		levantarIframe();
	});

	$("#btnCancelar").click(function () {
		bloquearModal();
		MensajeCerrarPreg("Seguro que desea Cerrar la pagina.");
	});

	$('*').bind("cut copy paste", function (e) {
		e.preventDefault();
	});

	$('#txtFolConsta').on('click', function () {
		$(this).attr('type', 'password');
	});

});
/*--------------------------------------------General---------------------------------------------*/
//Metodo para extraer los parameteros  de la URL
function getQueryVariable(varGet) {
	//Obtener la url compleata del navegador
	var sUrl = window.location.search.substring(1);
	//Separar los parametros junto con su valor
	var vars = sUrl.split("&");
	for (var i = 0; i < vars.length; i++) {
		//Obtener el parametro y su valor en arreglo de 2
		var par = vars[i].split("=");
		if (par[0] == varGet) // [0] nombre de variable, [1] valor
		{
			return par[1];
		}
	}
	return (false);
}

function obtenerfecha() {
	var arrDate = new Array();
	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 16 },
			success: function (data) {
				arrDate = eval(data);
				dFechaActualHora = arrDate.fechadiahora;
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});
}
//Metodo que obtiene los datos de promotor para ponerlos en la parte superior del html que se obtiene de la una funcion
function obtnerinformacionpromotor(empleado) {
	var bResp = false;
	var arrFolioSol = new Array();

	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 1, empleado: empleado },
			success: function (data) {
				arrFolioSol = eval(data);

				//Validacion para saber si esta registrado el promotor o no
				if (arrFolioSol.iCodigo == 0) {
					$("#txtFechaDia").val(arrFolioSol.dFechaActual);
					$("#txtUsuario").val(arrFolioSol.cNombre);
					$("#txtNomTienda").val(arrFolioSol.cTienda);
					bResp = true;
				}
				else {
					MensajeCerrar(arrFolioSol.cMensaje);
				}
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});

	return bResp;
}

function llenarcbxMotivoRegistro() {
	var arrMotReg = new Array();
	var addOption = '';
	var cbxMotReg = document.getElementById('cbxMotReg');

	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 3 },
			success: function (data) {
				arrMotReg = eval(data);
				var addOption = "<option value='-1'>SELECCIONE ...</option>";
				if (arrMotReg.length != 0) {
					for (var i = 0; i < arrMotReg.length; i++) {
						addOption += "<option value='" + arrMotReg[i].id + "'>" + arrMotReg[i].descripcion + "</option>";
					}
				}
				addOption += "<option value='0'>OTRO</option>";
				cbxMotReg.innerHTML = addOption;
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});
}

function llenarcbxTipoAdmin() {
	var arrTipoAdmin = new Array();
	var addOption = '';
	var cbxTpAdmin = document.getElementById('cbxTipoAdmin');

	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 4 },
			success: function (data) {
				arrTipoAdmin = eval(data);
				var addOption = "<option value='-1'>SELECCIONE ...</option>";
				if (arrTipoAdmin.length != 0) {
					for (var i = 0; i < arrTipoAdmin.length; i++) {
						addOption += "<option value='" + arrTipoAdmin[i].id + "'>" + arrTipoAdmin[i].descripcion + "</option>";
					}
				}
				cbxTpAdmin.innerHTML = addOption;
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});
}

/*--------------------------------------------Mensajes---------------------------------------------*/

//Metodo para Modal
function bloquearModal() {
	sHtml = "</br><img width=\"100%\" class=\"logoafore\" src=\"../imagenes/loadingBar1.gif\">";
	var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
	myDlg.espera(400, 150, 'Espere Un Momento Por Favor...');
	myDlg.mostrar(sHtml);
}

//Metodo para Modal
function desbloquearModal() {
	$("#divDlgMain").hide();//escondemos el div que bloquea pantalla
}

//Mensaje Generales
function ModalMensaje(cMensaje) {
	var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
	myDlg.crear(650, 200, cTituloModal);
	myDlg.mostrar(cMensaje);
}

function ModalMensajeAccion(cMensaje) {
	var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
	myDlg.crearBtnAccion(650, 200, cTituloModal, cerrarModalMensajeAccion);
	myDlg.mostrar(cMensaje);
}


function cerrarModalMensajeAccion() {
	levantarIframe();
}

//Mensaje y Cerrar en Navegador
function MensajeCerrar(sMensaje) {
	var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
	myDlg.modalcierre(500, 250, cTituloModal);
	myDlg.mostrar(sMensaje);
}

//Mensaje y Cerrar en Navegador PREGUNTA
function MensajeCerrarPreg(sMensaje) {
	var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
	myDlg.modalcierremensaje(500, 250, cTituloModal);
	myDlg.mostrar(sMensaje);
}

/*-------------------------------------------- Información ---------------------------------------------*/
function validarFolioSolicitud(foliosol) {
	var bResp = 0;
	iTipoTraspaso = 0;
	var arrFolioSol = new Array();

	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 2, foliosol: foliosol },
			success: function (data) {
				console.log("validar folio solicitud:",data);
				//Validacion para saber si esta registrado el promotor o no
				arrFolioSol = eval(data);
				if (arrFolioSol.folio == iFolio) {
					bResp = arrFolioSol.tiposolicitud;
					iTipoTraspaso = arrFolioSol.tipotraspaso;
					$("#txtFolConsta").val(arrFolioSol.folioconstanciaregtras);
					if (arrFolioSol.folioimplicaciontraspaso != "") {
						$("#txtFolConstaImpli").val(arrFolioSol.folioimplicaciontraspaso);
						$("#txtFolConstaImpli").attr("disabled", true);
					}
				}
				else {
					// TODO: descomentar
					MensajeCerrar("Ocurrio una excepcion al obtener información de la solicitud del Trabajador.");
				}
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});

	return bResp;
}

function validarCamposMotivoFolioImplicacion(tipoTras, tipoAfi) {
	var bResp = true;
	var iMotRef = $("#cbxMotReg option:selected").val();
	var iTipoAd = $("#cbxTipoAdmin option:selected").val();
	var iDepo = $('input:radio[name=rdoDeposito]:checked').val();
	var txtMOT = $("#txtMot").val();
	var MOTLen = $("#txtMot").val().length;
	var txtFol = $("#txtFolConstaImpli").val().length;

	console.log(iMotRef);

	if (iMotRef == -1) {
		if(sTipoSol == "Traspaso")
			{
				ModalMensaje("Debe escribir un motivo de " + tipoTras + ".");
			}
	}
	else if (iMotRef == 0)//Cuando Selecione OTRO en el Combo de Motivo de Registro/Traspaso
	{
		if (MOTLen < 1)//validar que no este vacio el txt Motivo
		{
			if(sTipoSol == "Traspaso")
			{
				ModalMensaje("Debe escribir un motivo de " + tipoTras + ".");
			}
			return false;
		}
		else if (MOTLen < 15)//Validar que tenga minimo 15 caracteres
		{
			if(sTipoSol == "Traspaso")
			{
				ModalMensaje("Debe escribir un motivo de " + tipoTras + " mas largo.");
			}
			return false;
		}
		else//No se repita 3 veces el mismo caracter
		{
			for (var i = 0; i < MOTLen; i++) {
				if (i == 0) {
					sLetra = txtMOT.charAt(i);
					iContador = 1;
				}
				else {
					if (sLetra == txtMOT.charAt(i)) {
						iContador++;
					}
					else {
						iContador = 1;
						sLetra = txtMOT.charAt(i);
					}
				}

				if (iContador > 3) {
					if(sTipoSol == "Traspaso")
					{
						ModalMensaje("El motivo de " + tipoTras + " no es valido, escriba otro.");
					}
					return false;
				}
			}
		}
	}
	else if (iMotRef > 0) {
		bResp = true;
	}

	if ((iTipoTraspaso == 1 || iTipoTraspaso == 3) && txtFol == 0) {
		bResp = true;
	}
	else if ((iTipoTraspaso == 1 || iTipoTraspaso == 3) && txtFol < 6)//Certificado Traspaso Con BanCoppel
	{
		ModalMensaje("El Folio de Constancia sobre Implicaciones de Traspaso no es valido.");
		return false;
	}

	if (tipoAfi == 33 && iTipoAd == -1) {
		ModalMensaje("El tipo de Administracion es requerido.");
		return false;
	}

	if (tipoAfi == 33 && (iDepo != 1 && iDepo != 0)) {
		ModalMensaje("Debe indicar si el cliente dio deposito previo.");
		return false;
	}

	return bResp;
}

function guardarDatosMotivoAfiliacion(foliosol, tipoafi, tipotraspaso) {
	var bResp = true;
	var MotRegi = '';
	var esimss = 0;
	var arrGuardarTem = new Array();
	//Text
	var FolConsta = $("#txtFolConsta").val().toUpperCase();
	var FolConstaImpli = $("#txtFolConstaImpli").val();
	var Motivo = $("#txtMot").val();
	//Combos
	var iMotRef = $("#cbxMotReg option:selected").val();
	var iTipoAdmin = $("#cbxTipoAdmin option:selected").val();
	//Radio
	var iDepo = $('input:radio[name=rdoDeposito]:checked').val();

	if (iMotRef == 0)
		MotRegi = "01";
	else
		MotRegi = "01";

	if (iDepo != 1 && iDepo != 0)
		iDepo = 0;

	if (tipoafi == 27 && tipotraspaso == 1)
		esimss = 1;
	else if (tipoafi == 27 && tipotraspaso == 2)
		esimss = 2;
	else
		esimss = 0;

	FolConsta.toUpperCase();
	FolConstaImpli.toUpperCase();
	Motivo.toUpperCase();

	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: {
				opcion: 5,
				opc: 3,
				foliosol: foliosol,
				tipoafi: tipoafi,
				esimss: esimss,
				tipotraspaso: tipotraspaso,
				motregi: MotRegi,
				folconstaimple: FolConstaImpli,
				iTipoAdmin: iTipoAdmin,
				depo: iDepo
			},
			success: function (data) {
				console.log("guardar datos motivo afiliacion:",data);
				arrGuardarTem = eval(data);
				if (arrGuardarTem.respuesta == 1)
					bResp = true;
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});

	return bResp;
}

function validarFolioSol(foliosol) {
	var bResp = false;
	var arrFoliSol = new Array();

	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 9, foliosol: foliosol },
			success: function (data) {
				arrFoliSol = eval(data);
				if (arrFoliSol.valor == 0) {
					bResp = true;
				}
				else {
					document.getElementById("btnGuardar").disabled = false;//Desabilitar Boton
					desbloquearModal();
				}
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});

	return bResp;
}

function guardarSolicitudBD(foliosol, empleado) {
	var bResp = false;
	var arrGuarSol = new Array();

	console.log("datos a guardar:",foliosol, empleado, dFechaActualHora);
	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 6, foliosol: foliosol, empleado: empleado, fecha: dFechaActualHora },
			success: function (data) {
				arrGuarSol = eval(data);
				console.log("Data de guardar solicitud:", data);
				if (arrGuarSol.respuesta == 1) {
					bResp = true;
				}
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});

	return bResp;
}

function obtenerRutaImprimir(opcionpag, foliosol, empleado, reimpre) {

	var bResp = '';
	var arrObtLiga = new Array();
	console.log()
	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 13, opcionpag: opcionpag, foliosol: foliosol, empleado: empleado, reimpre: reimpre },
			success: function (data) {
				arrObtLiga = eval(data);
				bResp = arrObtLiga.destino;
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});

	return bResp;
}

function soloLetras(e) {
	key = e.keyCode || e.which;
	tecla = String.fromCharCode(key).toLowerCase();
	letras = "2346789abcdefghjklmnñpqrtuvwxyz";
	especiales = [8, 09, 13, 14, 15, 24, 50, 51, 52, 54, 55, 56, 57, 127];

	tecla_especial = false
	for (var i in especiales) {
		if (key == especiales[i]) {
			tecla_especial = true;
			break;
		}
	}

	if (letras.indexOf(tecla) == -1 && !tecla_especial) {
		//ModalMensaje('Promotor, el folio de la solicitud capturado es incorrecto, favor de verificarlo');
		return false;
	}
}

function soloLetras2(e) {
	key = e.keyCode || e.which;
	tecla = String.fromCharCode(key).toLowerCase();
	letras = "0123456789abcdefghijklmnopqrstuvwxyz";
	especiales = [8, 09, 13, 14, 15, 24, 50, 51, 52, 54, 55, 56, 57, 127];

	tecla_especial = false
	for (var i in especiales) {
		if (key == especiales[i]) {
			tecla_especial = true;
			break;
		}
	}

	if (letras.indexOf(tecla) == -1 && !tecla_especial) {
		//ModalMensaje('Promotor, el folio de la solicitud capturado es incorrecto, favor de verificarlo');
		return false;
	}
}

function existenciafolioprocesar(foliosol, foliosolpro) {
	var bResp = '';
	var arrexis = new Array();

	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 7, foliosol: foliosol, folioprocesar: foliosolpro },
			success: function (data) {
				arrexis = eval(data);
				if (arrexis.irespuesta.trim() == -1) {
					ModalMensaje('Promotor, el folio capturado ya fue utilizado anteriormente, favor de verificarlo');
					bResp = -1;
				}
				else if (arrexis.irespuesta.trim() == 1) {
					bResp = 1;
				}
				else { bResp = 0; }
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});
	return bResp;
}
//Valida la fecha vigencia del folio que envia procesar
function existenciafechaProcesar(foliosol) {
	var bResp = '';
	var arrexis = new Array();

	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 8, foliosol: foliosol },
			success: function (data) {
				arrexis = eval(data);
				if (arrexis.irespuesta == -1) {
					ModalMensaje('Promotor, la vigencia del folio para este Trabajador no es  válida, favor de realizar una nueva captura de datos');
					bResp = -1;
				}
				else if (arrexis.irespuesta == -2) {
					ModalMensaje('Promotor, no se ha recibido una fecha de vigencia de folio para este  Trabajador');
					bResp = -2;
				}
				else if (arrexis.irespuesta == 1) {
					bResp = 1;
				}
				else { bResp = 0; }
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});
	return bResp;
}
//Funcion que desbloquea el segundo campo de validacion del folio enviado por procesar
function validacion() {

	var pas1 = $("#txtFolConsta").val().toUpperCase();

	if (pas1.length == 6) {
		if ($("#txtFolConstaImpli").val() == "") {
			document.getElementById("txtFolConstaImpli").disabled = true;
		}
		if(sTipoSol == "Traspaso")
		{
			document.getElementById("cbxMotReg").disabled = false;
		}
		else
		{
			document.getElementById("btnLlmadaCat").disabled = false;
		}
	}
	else {
		if ($("#txtFolConstaImpli").val() != "") {
			document.getElementById("txtFolConstaImpli").disabled = false;
		}

		document.getElementById("cbxMotReg").disabled = true;
	}
}

// MOVIL
/* Obtener parametros get desde el webview android */
function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search);
	return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

//INE//
//Funcion que valida si se cuenta con una autenticacion INE
function validacionine(foliosol) {
	var iResp = 0;
	var arrexis = new Array();
	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 10, foliosol: foliosol },
			success: function (data) {
				console.log("validacion ine:",data);
				arrexis = (data);
				iResp = arrexis.respuesta;
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});
	return iResp;
}
////Para validar si la fecha es correcta o si el folio es reutilizable
function fnvalidavigenciasemilla(foliosol, foliosemilla) {
	console.log("Validar Semilla:",foliosol, foliosemilla);
	var bResp = '';
	var arrexis = new Array();

	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 11, foliosol: foliosol, foliosemilla: foliosemilla },
			success: function (data) {
				console.log("vigencia de semillas:",arrexis);
				arrexis = eval(data);
				bResp = arrexis.irespuesta;
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});
	return bResp;
}
//Se obtiene la curp de la solconstancia
function fnobtenercurpconstancia(foliosol) {
	var bResp = '';
	var arrObtLiga = new Array();

	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 12, foliosol: foliosol },
			success: function (data) {
				console.log("obtener constancia:",data);
				arrObtLiga = eval(data);
				bResp = arrObtLiga.curp;
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});
	return bResp;
}
//Genera el codigó de autenticacion
function generarFolioIne(cCurp, foliosol) {
	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 14, curp: cCurp, foliosol: foliosol },
			success: function (data) {
				datos = data;
				generaCodigo = datos.codigoautenticacion;
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});
}

function fnvalorcombobox(value) {
	opcioncombo = value;
}

function envioIne() {
	$('#divMensaje').dialog('close');
	if (opcioncombo == 1) {
		fnValidaComboIvrSms();
	}
	else {
		fnValidaComboIvrSms();
	}
}

//Genera el codigó de autenticacion
function python(folioINE) {
	var arrexis = new Array();
	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 15, ifolioine: folioINE, opc: iEnvioAutenticacion},
			success: function (data) {
				console.log("consulta python:", data);
				arrexis = eval(data);
				bResp = arrexis.ifolioine;
				console.log("PYTHON folio->" + bResp);
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});
}

function fnvalidastatusenvioine(foliosol) {
	var bResp = '';
	var arrexis = new Array();

	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 17, foliosol: foliosol },
			success: function (data) {
				console.log("valida estatus envio ine:",data);
				arrexis = eval(data);
				bResp = arrexis.irespuesta;
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});
	return bResp;
}

//Modal que se muestra al dar clic en el boton
function fnAutentica() {
	var newHtml = "<div class='alerta'>";
	newHtml += "<p>";
	newHtml += "<div id='comboAu' style='text-align:justify;'><label id = 'lAutentica'><b>Promotor, favor de solicitar al Trabajador el medio por el que desea se le envíe el Código de Autenticación para su trámite</b></label></div>" + "</br></br>";
	newHtml += "<td>Medio de Envío:<select id='cboAutentica' name='cboAutentica' class ='comboBoxAutentica' onChange='fnvalorcombobox(value)'>";
	newHtml += "<option value = '1'>Llamada IVR</option>";
	newHtml += "<option value = '2'>SMS</option>";
	newHtml += "</select></td></input></td></p>";
	$('#divMensaje').html(newHtml);
	$("#divMensaje").dialog({
		resizable: false,
		height: 240,
		width: 350,
		modal: true,
		open: function (event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); $(".ui-widget-overlay", ui.front).css('height', '1000px'); },
		title: "CÓDIGO DE AUTENTICACIÓN",
		buttons: {
			"ENVIAR": function () {
				$('#divMensaje').dialog('close');

				fnValidaComboIvrSms();

			}
		}

	});
}

function fnValidaComboIvrSms() {
	var btnGuardar = document.getElementById("btnGuardar");
	var btnAutentica = document.getElementById("btnCodigoAu");
	var txtcodigo = document.getElementById("txtCodigoAu");
	var btnAutenticacion = document.getElementById("btnAutenticacion");

	banderabotenerparaIVR = fnopcionenvioIVR();
	if (contadorDetonacionCodigoAu < 2) {
		if (generaCodigo != '') {
			btnAutentica.disabled = false;
			txtcodigo.disabled = false;

			if (opcioncombo == 1) //LLAMADA IVR
			{
				if (banderabotenerparaIVR == 1)//BANDERA PARA VLAIDAR SI EL IVR ESTA ENCENDIDO
				{
					fnactualizaropcioncombo(cCurp, opcioncombo);
					fnInvocaLlamadaIvr(telefonoINE, codigoautenticacionIvr);
				}
				else {
					fnactualizaropcioncombo(cCurp, opcioncombo);
					python(folioINE);
				}
			}
			else if (opcioncombo == 2) //SMS
			{
				console.log("python");
				fnactualizaropcioncombo(cCurp, opcioncombo);
				python(folioINE);
			}

			opcioncombo = 1;

			contadorDetonacionCodigoAu++;

			if (contadorDetonacionCodigoAu == 2) {
				btnAutenticacion.disabled = true;
			}
		}
		else {
			ModalMensaje("Error al generar código de autenticación,favor de volverlo a intentar.");
		}
	}
	else {
		ModalMensaje("La detonación del código de autenticación agotó sus intentos, favor de contactar a Mesa de Ayuda.");
		btnAutenticacion.disabled = true;
	}
}

function fnobtenerfolioine(foliosol) {
	var bResp = '';
	var arrexis = new Array();
	console.log("folio sol a buscar:",foliosol);
	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 18, foliosol: foliosol },
			success: function (data) {
				console.log("folioine:",data);
				arrexis = eval(data);
				bResp = arrexis.ifolioine;
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});
	return bResp
}

//Funcion que obtiene el numero de telefon al que se le llamara y el codigo.
// de autenticacion que se dara por meio del IVR
function fnobtenerparaivr(cCurp, folioINE) {

	var bResp = '';
	var arrDatos = new Array();
	console.log(cCurp, folioINE);
	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 19, curp: cCurp, ifolioine: folioINE },
			success: function (data) {
				console.log("obtener para ivr:",data);
				arrDatos = eval(data);
				telefonoINE = arrDatos.telefono;
				codigoautenticacionIvr = arrDatos.codigoautenticacion;
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});

	return bResp;
}

//Funcion para invocar la Llamada IVR
function fnInvocaLlamadaIvr(telefonoINE, codigoautenticacionIvr)
{
	opcionLlamada = iEnvioAutenticacion = 1 && (tipoAfi == 26 || tipoAfi == 33) ? 3 : 0;

	console.log("telefono:",telefonoINE, codigoautenticacionIvr, opcionLlamada);
	$.ajax
		({
			async: false,
			cache: false,
			url: cLigaLamadaIVR,
			type: 'GET',
			dataType: 'json',
			// TODO: se corrigió esta parte para que funcionara con ivr.
			data: { telefono: telefonoINE, codautent: codigoautenticacionIvr, opcion: opcionLlamada },
			success: function (data) {
				if (data.estatus == 1) {
					//Llamada ejecutada correctamente
					console.log("Llamda IVR exitosa");
				}
				else {
					ModalMensaje("Se encontro un problema al realizar la llamada, favor de volverlo a intentar");
					document.getElementById("btnAutenticacion").disabled = false;
					document.getElementById("txtCodigoAu").disabled = true;
					document.getElementById("btnCodigoAu").disabled = true;
				}
			},
			error: function (a, b, c) {
				console.log("error fnInvocaLlamadaIvr");
				alert("error ajax " + a + " " + b + " " + c);
			},
			beforeSend: function () { }
		});

}

//INE //
function fnactualizaropcioncombo(cCurp, opcioncombo) {
	var bResp = '';
	var arrDatos = new Array();

	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 20, curp: cCurp, opcioncombo: opcioncombo },
			success: function (data) {
				arrDatos = eval(data);
				bResp = arrDatos.irespuesta;
			},
			error: function (a, b, c) {
				console.log("error fnactualizaropcioncombo");
				alert("error ajax " + a + " " + b + " " + c);
			},
			beforeSend: function () { }
		});
	return bResp;
}

function obtenerLigaImprecionDocs()
{

	var bResp = '';
	var arrObtLiga = new Array();
	var opcionpag = 11;//Levantar Pagina de Impresion
	var reimpre = 0

	console.log()
	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 13, opcionpag: opcionpag, foliosol: iFolio, empleado: iEmpleado, reimpre: reimpre },
			success: function (data)
			{
				console.log(data);
				arrObtLiga = eval(data);
				bResp = arrObtLiga.destino;
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});

	return bResp;
}

//INE //
function fnobtenerLigaIVR() {
	var bResp = '';
	var arrDatos = new Array();

	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 21, iopcionLigaine: 9 },
			success: function (data) {
				console.log(data);
				arrDatos = eval(data);
				bResp = arrDatos.liga;
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});
	return bResp;
}
//INE //
function fnopcionenvioIVR() {
	var bResp = '';
	var arrDatos = new Array();

	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 22 },
			success: function (data) {
				console.log("opcion envio ivr:",data);
				arrDatos = eval(data);
				bResp = arrDatos.irespuesta;
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});
	return bResp;
}
/********************folio 877.1******************************** */
function obtenerConfiguracionWs() {
	var datos = new Array();

	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 23 },
			success: function (data) {
				datos = (data);
				intentosWS = datos.configuracion.intentos;
				tiempoWS = datos.configuracion.tiempo;
				console.log("obtener configuracion ws:",datos);
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});
}

function agregarDatosConsultaFolio(opci) {

	var bResp = false;
	var datos = new Array();
	var folioregtras = ''

	if (opci == 1) {
		folioregtras = $("#txtFolConsta").val().toUpperCase();
		folioAnterior = $("#txtFolConsta").val().toUpperCase();
	}
	else {
		folioregtras = $("#txtFolio").val().toUpperCase();
		folioAnterior = $("#txtFolio").val().toUpperCase();
		bloquearModal();
	}
	nIntentos++;
	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 24, empleado: iEmpleado, curp: sCurpTitular, tipoafi: tipoAfi, folioRegTras: folioregtras, foliosol: iFolio },
			success: function (data) {
				datos = (data);
				console.log("agregar datos consulta folios:",datos);
				if (datos.estatus == 1) {
					keyxWs = datos.result;
		//			alert("keyxgarza  = "+ keyxWs);
					bResp = true;
					if (opci == 2) {
						setTimeout(function () {
							validarDatosConsultarFolio();
						}, tiempoWS);
					}
					else {
						console.log("desbloquea el modal");
						desbloquearModal();
					}
				}
				else {
					MensajeCerrar('Promotor: Ocurrió un error al ejecutar el WS, favor de comunicarse con mesa de ayuda');
				}
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});
	return bResp;
}

function validarDatosConsultarFolio() {
	var bResp = 0;
	var respuestaWs = 0;
	var accionWs = '';
	var mensajeErrorWs = '';
	var datos = new Array();

	console.log("validar datos de consulta de folios  - keyxWs:",keyxWs);

	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 25, keyX: keyxWs },
			success: function (data) {
				datos = (data);
				console.log(datos);

				if (datos.estatus == 1) {
					respuestaWs = datos.result.respuesta;
					accionWs = datos.result.accion;
					mensajeWs = datos.result.mensaje;
					mensajeErrorWs = datos.result.mensajeerror;

					if (respuestaWs == 1) {
						console.log("respuesta de validarDatosConsultarFolio:", respuestaWs, folioAnterior);

						$('#iconoServicio').css('display', 'inline');
						actualizarconsultaregtra(2, folioAnterior);
						// ModalMensaje("LA SOLICITUD HA SIDO GUARDADA.");
						// ModalMensajeAccion("LA SOLICITUD HA SIDO GUARDADA.")
						levantarIframe();
						document.getElementById("btnGuardar").disabled = true;//Desabilitar Boton
						document.getElementById("btnCancelar").disabled = true;//Desabilitar Boton
						//document.getElementById("btnImprimir").disabled = false;//Habilitar Boton
						bResp = true;
					}
					else {
						if (nIntentos < intentosWS) {
							actualizarconsultaregtra(3, folioAnterior);
							pantallaCapturaFolio(mensajeWs);
						}
						else {
							if (accionWs == 'cerrar') {
								actualizarconsultaregtra(3, folioAnterior);
								MensajeCerrar(mensajeErrorWs);
							}
							else {
								actualizarconsultaregtra(1, folioAnterior);
								//ModalMensaje("LA SOLICITUD HA SIDO GUARDADA.");
								// ModalMensajeAccion("LA SOLICITUD HA SIDO GUARDADA.")
								levantarIframe();
								document.getElementById("btnGuardar").disabled = true;//Desabilitar Boton
								document.getElementById("btnCancelar").disabled = true;//Desabilitar Boton
								//document.getElementById("btnImprimir").disabled = false;//Habilitar Boton
								bResp = true;
							}
						}
					}
				}
				else {
					MensajeCerrar('Promotor: Ocurrió un error al ejecutar el WS, favor de comunicarse con mesa de ayuda');
				}
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});
	return bResp;
}

function pantallaCapturaFolio(mensaje) {
	var sHtml = mensaje + '<br><br>' +
		'<label style = "color:red;">Folio anterior capturado: </label>' + '<label style = "color:red;margin-left: 11%;">' + folioAnterior + '</label><br><br>' +
		'<label>Folio de solicitud: </label>' + '<input id="txtFolio" name="txtFolio" style ="margin-left: 22%;" class="CajaTexto" type="password" onkeypress = "return soloLetras(event);" maxlength = "6" tabindex = 1 onkeyup = "habilitarConfirm()" /><br><br>' +
		'<label>Confirmacion de folio de solicitud:</label>' + '<input id="txtFolioConfirmar" name="txtFolioConfirmar" disabled class="CajaTexto" type="text" onkeypress = "return soloLetras(event);" maxlength = "6" tabindex = 1 onkeyup = "confirmar();" style="margin-left: 1.5%;" /><img id="imgConfirmar"src="imagenes/loading.gif" style="height: 20px;width: 20px;"><br><br>';
	var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
	myDlg.crearFuncion(700, 300, cTituloModal);
	myDlg.mostrar(sHtml);
	$('#imgConfirmar').css('display', 'none');
}

function actualizarconsultaregtra(medioConsulta, folioProcesar) {
	var bResp = false;
	var datos = new Array();
	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 26, curp: sCurpTitular, folioprocesar: folioProcesar, foliosol: iFolio, keyX: medioConsulta },
			success: function (data) {
				datos = (data);
				console.log(datos);
				if (datos.estatus == 1) {
					if (datos.result.respuesta == 1) {
						bResp = true;
					}
					else {
						MensajeCerrar('Promotor: Ocurrió un error al actualizar los datos, favor de comunicarse con mesa de ayuda');
					}
				}
				else {
					MensajeCerrar('Promotor: Ocurrió un error al actualizar los datos, favor de comunicarse con mesa de ayuda');
				}
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});
}

function habilitarConfirm() {
	var long = $("#txtFolio").val();

	if (long.length == 6) {
		$("#txtFolio").attr("disabled", "disabled");
		$("#txtFolioConfirmar").removeAttr("disabled");
		$("#txtFolioConfirmar").focus();
	}
	else {
		$("#txtFolioConfirmar").attr("disabled", "disabled");
		$("#txtFolio").removeAttr("disabled");
	}

}

function confirmar() {
	var fol1 = $("#txtFolio").val();
	var fol2 = $("#txtFolioConfirmar").val();
	$('#imgConfirmar').css('display', 'inline');
	$('#imgConfirmar').css('margin-left', '5PX');

	if (fol1 == fol2) {

		$('#txtiguales').css('display', 'none');
		$("#imgConfirmar").attr("src", "../Imagenes/exito.png");
		$("#txtFolioConfirmar").attr("disabled", "disabled");
		setTimeout(function () {
			agregarDatosConsultaFolio(2);
		}, 2000);
	}
	else {
		$("#imgConfirmar").attr("src", "../Imagenes/error.png");
		if (fol2.length == 6) {
			$("#txtFolioConfirmar").val('');
		}
	}

	if (fol2 == '') {
		$('#imgConfirmar').css('display', 'none');
		$("#txtFolioConfirmar").attr("disabled", "disabled");
		$("#txtFolio").removeAttr("disabled");
		$("#txtFolio").focus();
	}

}

function obTenercurpTitular(iFolio)
{
	var curpTitular = '';
	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 27, foliosol: iFolio },
			success: function (data)
			{
				console.log("obtener curp titular:",data);
				curpTitular = data.curp;

			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});

		return curpTitular;
}


function levantarIframe(){
	// Armar liga y levantar frame
	ligaFrame = ligaFrame.replace("[EMPLEADO]", iEmpleado);
	ligaFrame = ligaFrame.replace("[FOLIO]", iFolio);
	ligaFrame = ligaFrame + "&renapo=" + iRenapo;
	console.log("levantar frame con liga:",ligaFrame);

	if(OSName == "Android"){
		Android.llamaComponenteEnlace(ligaFrame);
		setTimeout(function () { Android.volverMenu('1'); },1000);
	}else{
		// cerramos el modal para divDlgMain
		var divDlgMain = document.getElementById('divDlgMain')
		divDlgMain.innerHTML="";
		divDlgMain.style.display = 'none';
		var myDlg = new dialog_frame(document.getElementById('divDlgFrame'));
		myDlg.FramePagina(99,99,ligaFrame);
		document.getElementById("btnAutenticacion").disabled = false;
		document.getElementById("btnCancelar").disabled = true;
	}

}

function cerrarIframe(){
	desbloquearModal();
	cerrarFrame();
	$("#btnListadoDocumentos").show();
}

function cerrarFrame(){
	var divDlgMain = document.getElementById('divDlgFrame');
	divDlgMain.innerHTML="";
	divDlgMain.style.display = 'none';
}

function fnobtenersemillaafiliacion(foliosol) {
	console.log("fnobtenersemillaafiliacion-> "+ foliosol);
	var bResp;
	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion: 29, foliosol: foliosol },
			success: function (data) {
				console.log(data);
				bResp = data.semilla;
			},
			error: function (a, b, c) { alert("error ajax " + a + " " + b + " " + c); },
			beforeSend: function () { }
		});

	return bResp;
}
function ValidaConsultaRenapoPrevia(cCurp,iEsTitular) 
{
	var bResp = false;
	var arrRenapo = new Array();

	$.ajax
    ({
		async: false,
		cache: false,
		url: 'php/casecapturaafiliacion.php',
		type: 'POST',
		dataType: 'json',
		data:{opcion:24,curp:cCurp,iEsTitular:iEsTitular},
		success: function(data)
		{
			arrRenapo = eval(data);

			//Validacion para saber si fue consultado ante renapo previamente.
			if(arrRenapo.iRenapo == 1)
			{
				bResp = true;
			}
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});

	return bResp;
}