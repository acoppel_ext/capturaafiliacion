//Variables Estaticas
LIGA_PAGINAS_WEB		= 1;
// GRABAR_AUDIOS			= 2;
VALIDAR_HUELLAS_CLIENTE	= 3;
CERRAR_NAVEGADOR = 4;

//Tipos de Solicitud
SOLICITUD_REGISTRO	= 26;// Registro. Cliente que se afilia a una Afore por primera vez.
SOLICITUD_TRASPASO	= 27;// Traspaso. Cliente que cambia de Afore
SOLICITUD_INDEPENDIENTE	= 33;// No Afiliado; cliente sin afiliación al Seguro Social. No tiene NSS.

//Tipos de Traspaso
TRASPASO_TIPO_NORMAL	= 0;// Traspaso normal. Cliente asignado que cambia a Afore
TRASPASO_TIPO_NORMAL_INE= 828//Traspaso normal con autenticación INE
TRASPASO_TIPO_ISSSTE	= 1;// Traspaso a un cliente que tiene ISSSTE
TRASPASO_TIPO_ISSSTE_INE= 829//Traspaso a un cliente que tiene ISSSTE con autenticación INE
TRASPASO_TIPO_ASIGNADO	= 2;// Traspaso a un cliente asignado
TRASPASO_TIPO_IMSS_BCPL = 830;//Traspaso a un cliente que tiene IMSS con autenticación BCPL
TRASPASO_TIPO_ISSSTE_BCPL = 831;//Traspaso a un cliente que tiene ISSSTE/NO AFILIADO con autenticación BCPL
TRASPASO_TIPO_IMSS_DTL = 832; //Traspaso a un cliente que tiene IMSS con autenticacion Dactilar
TRASPASO_TIPO_ISSSTE_DTL = 833; //Traspaso a un cliente que tiene ISSSTE con autenticacion Dactilar

//Declaracion de Variables
var iEmpleado			= 0;
var Curp             	= '';
var iGuardarMsj        	= 0;
var iGuardarMsjError   	= 0;
var cTituloModal		= 'Captura Afiliación';
var iOpcion				= 0;
var folioEnrol 			= 0;
var iNss				= 0;
var iFolioSol			= 0;
var SolicitudTipoID		= 0;
var iSolReg				= 0;
var arrTipoSolicitud	= new Array();
var ipServidorSPA		= '';
var parametrosHuellas	= '';
var iContHuellas 		= 0;
var iFlujoTermino		= 0;
var itieneexcepcion 	= 0;
var cCurpBeneficiario	= '';
var cCurp				= '';
var bEsRenapo			= false;
var iMarcaRenapo		= 0;
//INE
var AUTENTICACION_EDOCTA = 0;
var AUTENTICACION_INE = 1;
var AUTENTICACION_BCPL = 2;
var AUTENTICACION_DTL = 3;
var validarAutenticacion = 0; //0-DAFULT | 1-AUTENTICACION INE | 2-AUTENTICACION BANCOPPEL | 3-AUTENTICACION DACTILAR
var esimss		= 0;

//Instruccion que permite conocer el sistema operativo de donde ingresa el usuario
var OSName="Desconocido";
if (navigator.appVersion.indexOf("Win")!=-1) OSName="Windows";
if (navigator.appVersion.indexOf("Mac")!=-1) OSName="MacOS";
if (navigator.appVersion.indexOf("X11")!=-1) OSName="UNIX";
if (navigator.appVersion.indexOf("Linux")!=-1) OSName="Linux";
if (navigator.appVersion.indexOf("Android")!=-1) OSName="Android";

//Estaticos
var ligaCase			= 'php/casecapturaafiliacion.php';

$(document).ready(function()
{
	$(document).keydown(function(ev)
    {			
		if((ev.keyCode >= 112) && (ev.keyCode <= 123))
		{			
			return false;
		}

		return true;
	});
	bloquearModal();
	var divRespuestaRenapo 	= document.getElementById('divRespuestaRenapo');
	var addRespuesta = '';
	/*Obtener Variables pasadas por URL*/
	if(OSName == "Android"){

		$("body").on("keyup", "#txtCURP", function (e) 
		{
			var texto = $("#txtCURP").val();
			texto = texto.toString();

			var expReg = /^[a-zA-Z0-9]*$/;

			if (expReg.test(texto))
			{
				if($(this).val().length >= 18)
				{
					$(this).val($(this).val().substr(0, 18));
					
					if(e.which != 8 && e.which != 0) 
					{
						e.preventDefault();
					}
					else
					{
						return validarKeypressCurp(e);
					}
				}
				else
				{
					return validarKeypressCurp(e);
				}
			}
			else
			{
				var texto = $("#txtCURP").val().replace(/[\W_]/gi, '');
				$("#txtCURP").val(texto);
			}
		});
		
		iEmpleado = getParameterByName('empleado');
	}else{
		iEmpleado = getQueryVariable("empleado");
	}



	//Falta numero de Empleado o no hay datos de este
	if((iEmpleado != "") && (obtnerinformacionpromotor(iEmpleado)))
	{
		desbloquearModal();
	}

	llenadoTipoRegistro();

	$("#txtNSS").keypress(function (e) {
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57))
			return false;
	});

	// $("#btnVolver").click(function()
	// {
		// MensajeCerrarPreg("Seguro que desea Cerrar la Captura de Afiliación");
	// });

	//Boton para validar la CURP y continuar con el procesos de CAPTURA DE AFILIACION
	$("#btnContinuar").click(function()
    {
		bloquearModal();
		
		if(validacionesCurpAfiliacion())
		{
			if (iMarcaRenapo == 0)
			{	
				var cCurp = $('#txtCURP').val().toUpperCase();
				if(!ValidaConsultaRenapoPrevia(cCurp, 1))
				{
					if(caminoWSRenapo(cCurp, iEmpleado, iFolioSol, 1))
					{
						cCurpBeneficiario = obtenerCurpBeneficiario(cCurp);
						bEsRenapo = true;
						if(cCurp == cCurpBeneficiario)
						{
							divRespuestaRenapo.innerHTML = '';
							addRespuesta += '<input style="cursor:default" autocomplete="off" type = "image" src="imagenes/RenapoExitoso.png" width = "132" height = "28"></input>';
							divRespuestaRenapo.innerHTML = addRespuesta;
							iMarcaRenapo = 1;
						}
						else
						{
							if(!ValidaConsultaRenapoPrevia(cCurpBeneficiario, 0))
							{
								if(caminoWSRenapo(cCurpBeneficiario, iEmpleado, iFolioSol,0))
								{
									if(bEsRenapo)
									{
										divRespuestaRenapo.innerHTML = '';
										addRespuesta += '<input style="cursor:default" autocomplete="off" type = "image" src="imagenes/RenapoExitoso.png" width = "132" height = "28"></input>';
										divRespuestaRenapo.innerHTML = addRespuesta;
										iMarcaRenapo = 1;
									}
								}
							}
						}
					}
				}
				else
				{
					cCurpBeneficiario = obtenerCurpBeneficiario(cCurp);
					if(cCurp == cCurpBeneficiario)
					{
						divRespuestaRenapo.innerHTML = '';
						addRespuesta += '<input style="cursor:default" autocomplete="off" type = "image" src="imagenes/RenapoExitoso.png" width = "132" height = "28"></input>';
						divRespuestaRenapo.innerHTML = addRespuesta;
						iMarcaRenapo = 1;
					}
					else
					{
						if(!ValidaConsultaRenapoPrevia(cCurpBeneficiario, 0))
						{
							if(caminoWSRenapo(cCurpBeneficiario, iEmpleado, iFolioSol, 0))
							{
								if(bEsRenapo)
								{
									divRespuestaRenapo.innerHTML = '';
									addRespuesta += '<input style="cursor:default" autocomplete="off" type = "image" src="imagenes/RenapoExitoso.png" width = "132" height = "28"></input>';
									divRespuestaRenapo.innerHTML = addRespuesta;
									iMarcaRenapo = 1;
								}
							}
						}
					}
				}
			}
		}
	});

	//Boton para validar la NSS y continuar con el procesos de CAPTURA DE AFILIACION
	/*$("#btnContinuarNSS").click(function()
    {
		bloquearModal();
		validacionesNssAfiliacion();
	});*/

//	Opcion que permite ocultar o mostrar la caja de texto
//	dependiendo la opcion seleccionada en el radiobutton
	$(document).ready(function() {
		$("#opcionCurp").click(function() {
		$("#uno").show();//1 en curp
		$("#dos").hide();//2 en cnss
		});
		$("#opcionNss").click(function() {
			$("#uno").hide();//1 en opcionnss
			$("#dos").show();//2 en nss
		});
	});


	$("#cbxTipoSolicitud").change(
	function()
	{
		//Obtener Valor del Arreglo donde esta la informacion de la tabla
		var iSolVal = $("#cbxTipoSolicitud option:selected").val();
		$(".divBusquedad").hide();

		iSolReg	= 0; //Obtener el Tipo de Solicitus para la Afiliacion
		SolicitudTipoID	= 0; //Obtener ID Tipo Traspaso

		if( iSolVal >= 0)//Si seleciono una solicitud
		{
			iSolReg = arrTipoSolicitud[iSolVal].itiposol;
			SolicitudTipoID = arrTipoSolicitud[iSolVal].itipotras;
		}
		else
		{
			iSolReg = 0;
			SolicitudTipoID	= 0;
		}
		//Validar la solicitud
		if( iSolReg == SOLICITUD_REGISTRO )
		{
			$(".divBusquedad").show();
			{
				// Sirve ocultar los radio button y mostrar dependiendo la opcion que se le seleccione
					if($("#opcionCurp").is(':checked')) {
						$("#uno").show();//1 en curp
						$("#dos").hide();//2 en nss
					}
					if($("#opcionNss").is(':checked'))
					{
						$("#dos").show();//2 en nss
						$("#uno").hide();//1 en curp
					}
					$('input[name="opcionCurp"]').attr('checked', false);
			}
		}
		else if( iSolReg == SOLICITUD_TRASPASO )
		{
			if( SolicitudTipoID == TRASPASO_TIPO_NORMAL)
			{
				$(".divBusquedad").show();
				{
					// Sirve ocultar los radio button y mostrar dependiendo la opcion que se le seleccione
						if($("#opcionCurp").is(':checked')) {
							$("#uno").show();//1 en curp
							$("#dos").hide();//2 en nss
						}
						if($("#opcionNss").is(':checked'))
						{
							$("#dos").show();//2 en nss
							$("#uno").hide();//1 en curp
						}
						$('input[name="opcionCurp"]').attr('checked', false);
				}
			}
			else if( SolicitudTipoID == TRASPASO_TIPO_ISSSTE)
			{
				$(".divCURP").show();
				$(".divNSS").hide();
				$('#modo').hide();
			}
			else if( SolicitudTipoID == TRASPASO_TIPO_ASIGNADO)
			{
				$(".divCURP").show();
				$(".divNSS").hide();
				$('#modo').hide();
			}
		}
		else if( iSolReg == SOLICITUD_INDEPENDIENTE )
		{
			$(".divCURP").show();
			$(".divNSS").hide();
			$('#modo').hide();
		}
		else
		{
			$(".divCURP").hide();
			$(".divNSS").hide();
			$(".btnContinuar").hide();
		}
	});

	$("#txtNSS").change(
	function()
	{
		//Obtener Valor del Arreglo donde esta la informacion de la tabla
		var cNss = $("#txtNSS").val();
		//console.log(cNss);
		ConsultarSolicitudConstancia(cNss);

	});

	$("#txtCURP").change(
	function()
	{
		//alert("entró al change de nss");

		//Obtener Valor del Arreglo donde esta la informacion de la tabla
		var cCurp = $("#txtCURP").val().toUpperCase();

		$("#txtNSS").val(consultarNSS(cCurp));

		ConsultarSolicitudConstancia(cCurp);

		//Obtener Valor del Arreglo donde esta la informacion de la tabla
		var iSolVal = $("#cbxTipoSolicitud option:selected").val();

		iSolReg	= 0; //Obtener el Tipo de Solicitus para la Afiliacion
		SolicitudTipoID	= 0; //Obtener ID Tipo Traspaso

		if( iSolVal >= 0)//Si seleciono una solicitud
		{
			iSolReg = arrTipoSolicitud[iSolVal].itiposol;
			SolicitudTipoID = arrTipoSolicitud[iSolVal].itipotras;
		}
		else
		{
			iSolReg = 0;
			SolicitudTipoID	= 0;
		}

	});
});

function ConsultarSolicitudConstancia(sDato)
{
	var bResp = '';
	var arrSolC = new Array();

	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:16,sDato:sDato},
		success: function(data)
		{
			//arrSolC = eval(data);
			//if(validarErrorCaseSolExp(arrSolC.error,arrSolC.mensaje,arrSolC.redireccionar));

				$("#cbxTipoSolicitud option").each(function () {
		        if ($(this).html().toUpperCase() == data[0].tipoconstancia.toString().toUpperCase()) {
		            $(this).attr("selected", "selected");
			            return;
			        }
				});


			//bResp = arrSolC.mensaje;

		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});

	return bResp;
}


function validacionesCurpAfiliacion()
{

	iContHuellas	= 0;
	var sCurp 		= $('#txtCURP').val().toUpperCase();
	//Obtener Valor del Arreglo donde esta la informacion de la tabla
	var iSolVal 	= $("#cbxTipoSolicitud option:selected").val();
	var bValido 	= false;

	if(validacionesCURP())
	{
		if(validacionCurpAfiliacion(sCurp, iSolReg)){
			//INE//para obtener el resultado de la funcion en la variable
			validarAutenticacion = validacionine(iFolioSol);
			bValido = true;
		}
	}
	return bValido;
}

function validacionesNssAfiliacion()
{
	var iNssVal 	= 0;
	iContHuellas	= 0;
	iNssVal			= $('#txtNSS').val();
	//Obtener Valor del Arreglo donde esta la informacion de la tabla
	var iSolVal 	= $("#cbxTipoSolicitud option:selected").val();
	if(iSolVal != -1)
	{
		if(validacionesNSS())
		{
			var sCurp = obtenerCurpSolcontancia(iNssVal,iSolVal);
			if(sCurp != '')
				validacionCurpAfiliacion(sCurp, iSolReg);
		}
	}
	else
	{
		ModalMensaje("Selecciona un Tipo de Solicitud.");
	}
}

/*--------------------------------------------General---------------------------------------------*/
//Metodo para extraer los parameteros  de la URL
function getQueryVariable(varGet)
{
	//Obtener la url compleata del navegador
	var sUrl = window.location.search.substring(1);
	//Separar los parametros junto con su valor
	var vars = sUrl.split("&");
	for (var i=0;i<vars.length;i++)
	{
		//Obtener el parametro y su valor en arreglo de 2
		var par = vars[i].split("=");
		if(par[0] == varGet) // [0] nombre de variable, [1] valor
		{
			return par[1];
		}
	}
	return(false);
}

//Valida la estructura de la curp
function validarEstructuraCurp(curp)
{
	var esValida = 0;
	if(curp.match(/^([A-Z])([A,E,I,O,U,X])([A-Z]{2})([0-9]{2})([0-1])([0-9])([0-3])([0-9])([M,H])([A-Z]{2})([B,C,D,F,G,H,J,K,L,M,N,Ñ,P,Q,R,S,T,V,W,X,Y,Z]{3})([0-9,A-Z])([0-9])$/i))
	{
		esValida = 1;
	}
	return esValida;
}

function validacionesCURP()
{
	bResp = false;
	var sCurp = $('#txtCURP').val().toUpperCase();

	if (sCurp.length == 0)
	{
		ModalMensaje("El campo CURP es obligatorio, Favor de proporcionar uno.");
	}
	else if (sCurp.length == 18)
	{

		if(validarEstructuraCurp(sCurp))
		{
			bResp = true;
		}
		else
		{
			ModalMensaje("CURP no Valida.");
		}
	}
	else
	{
		ModalMensaje("Favor de Agregar la CURP con 18 caracteres.");
	}
	return bResp;
}

function validacionesNSS()
{
	bResp = false;
	var iNssVal = $('#txtNSS').val();

	if (iNssVal.length == 0)
	{
		ModalMensaje("El campo NSS es obligatorio, Favor de proporcionar uno.");
	}
	else if (iNssVal.length == 11)
	{
		bResp = true;
	}
	else
	{
		ModalMensaje("Favor de Agregar el NSS con 11 caracteres.");
	}
	return bResp;
}
/**************************************<APPLET>**************************************/
// function ejecutaAplet(sParametros)
// {
	// switch(iOpcion)
	// {
		// case LIGA_PAGINAS_WEB:
		// $('#divApplet').empty();
			// var Ruta = 'C:\\SYS\\firefoxportable\\navegadorAfore.exe';
				// document.getElementById('divApplet').innerHTML =
					// "<APPLET CODE  = \"appafore.class\" " +
						// "ARCHIVE  = \"../applet/appafore.jar\" " +
						// "ID = \"appafore\" " +
						// "WIDTH    = 1 " +
						// "HEIGHT   = 1 > " +
					// "<param name=\"opcion\" value=\"3\"> " +
					// "<param name=\"comando\" value=\""+ Ruta + "\"> " +
					// "<param name=\"argumentos\" value=\"" + sParametros + "\"> " +
				// "</APPLET>";
			// break;

		// case GRABAR_AUDIOS:
		// $('#divApplet').empty();
			// var Ruta = 'C:\\SYS\\PROGS\\soundviewer.exe';
				// document.getElementById('divApplet').innerHTML =
					// "<APPLET CODE  = \"appafore.class\" " +
						// "ARCHIVE  = \"../applet/appafore.jar\" " +
						// "ID = \"appafore\" " +
						// "WIDTH    = 1 " +
						// "HEIGHT   = 1 > " +
					// "<param name=\"opcion\" value=\"1\"> " +
					// "<param name=\"comando\" value=\""+ Ruta + "\"> " +
					// "<param name=\"argumentos\" value=\"" + sParametros + "\"> " +
				// "</APPLET>";
			// break;

		// case VALIDAR_HUELLAS_CLIENTE:
		// $('#divApplet').empty();
			// var Ruta = 'C:\\SYS\\PROGS\\HEIMGHUELLA.EXE';
				// document.getElementById('divApplet').innerHTML =
					// "<APPLET CODE  = \"appafore.class\" " +
						// "ARCHIVE  = \"../applet/appafore.jar\" " +
						// "ID = \"appafore\" " +
						// "WIDTH    = 1 " +
						// "HEIGHT   = 1 > " +
					// "<param name=\"opcion\" value=\"1\"> " +
					// "<param name=\"comando\" value=\""+ Ruta + "\"> " +
					// "<param name=\"argumentos\" value=\"" + sParametros + "\"> " +
				// "</APPLET>";
			// break;

		// default:
		// break;
	// }
// }

// function respuestaApplet(iRespuesta)
// {
	// switch(iOpcion)
	// {
		// case LIGA_PAGINAS_WEB:
			// if(iFlujoTermino != 1)
			// {
				// /*if($("#txtCURP").val() != "")
					// validacionesCurpAfiliacion();
				// else
					// validacionesNssAfiliacion();*/
				// ModalMensaje("Promotor: Favor de continuar con el Guardado de la Solicitud, da click al botón CONTINUAR.");
			// }
			// else
			// {
				// closeNavegador();
			// }
			// break;

		// case GRABAR_AUDIOS:
			// validarRespAudio(iRespuesta);
			// break;

		// case VALIDAR_HUELLAS_CLIENTE:
			// if(iContHuellas<=3)
			// {
				// if(iFolioSol == iRespuesta)
					// pasarCaptura();
				// else
					// ModalMensajeHuellas("La huella del cliente no coincide con la registrada.", parametrosHuellas);
			// }
			// else
			// {
				// ModalMensaje("La Autentidad del Trabajador NO ha podido ser comprobada...Verifique por Favor!!.");
			// }
			// break;

		// default:
			// break;
	// }
// }

/**************************************</APPLET>**************************************/

/**************************************<WEBSERVICE EXE>**************************************/
/**
 * OPCION WEBSERVICE
 */
function opcionEjecuta(sParametros)
{

	if(OSName != "Android"){

		var Ruta = '';
		switch(iOpcion)
		{
			case LIGA_PAGINAS_WEB:
				Ruta = 'C:\\SYS\\firefoxportable\\navegadorAfore.exe';
				break;
			case VALIDAR_HUELLAS_CLIENTE:
				Ruta = 'C:\\SYS\\PROGS\\HEIMGHUELLA.EXE';
				break;
		}
		ejecutaWebService(Ruta, sParametros);

	}else{

		switch(iOpcion)
		{
			case LIGA_PAGINAS_WEB:
				//sParametros = "http://10.44.172.234/expedienteIdentificacionDevSergio/indexExpedienteIdentificacion.php?numEmp=98856014&folio=6706824&tipoServicio=26&existeEi=0";
				$("#divDlgMain").hide();
				
				sParametros = sParametros.replace('http://', '');
				sParametros = sParametros.replace('https://', '');
				var indice = sParametros.indexOf("/");
				sParametros = sParametros.substring(indice);
				sParametros = 'http://' + location.host + sParametros;
				
				Android.llamaComponenteEnlace(sParametros);
				setTimeout(function () 
				{
					closeNavegador();
				},1000);
				break;
			case VALIDAR_HUELLAS_CLIENTE:
				var sCurp = $('#txtCURP').val().toUpperCase();
				Android.llamaComponenteHuellas(7, iFolioSol, 1, 0, iEmpleado, 0, sCurp);
				break;
			case CERRAR_NAVEGADOR:
				Android.volverMenu('1');
				break;
		}
	}
}

/**
 * EJECUTA WEBSERVICE
 */
function ejecutaWebService(sRuta, sParametros)
{
	console.log(`Ejecuta web service -> ruta:${sRuta} - parametros:${sParametros}`);
	if(OSName != "Android"){
		soapData 	= "",
		httpObject 	= null,
		docXml 		= null,
		iEstado 	= 0,
		sMensaje 	= "";
		sUrlSoap 	= "http://127.0.0.1:20044/";
		var bResp	= false;

		soapData =
		'<?xml version=\"1.0\" encoding=\"UTF-8\"?>' +
			'<SOAP-ENV:Envelope' +
				' xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"'+
				' xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\"'+
				' xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"'+
				' xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"'+
				' xmlns:ns2=\"urn:ServiciosWebx\">'+
				'<SOAP-ENV:Body  SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">'+
					'<ns2:ejecutarAplicacion>'+
						'<inParam>'+
							'<Esperar>1</Esperar>'+
							'<RutaAplicacion>' + sRuta + '</RutaAplicacion>'+
							//'<parametros>' + sParametros + '</parametros>'+
							'<parametros><![CDATA[' + sParametros + ']]></parametros>'+
						'</inParam>'+
					'</ns2:ejecutarAplicacion>'+
				'</SOAP-ENV:Body>'+
			'</SOAP-ENV:Envelope>';

		httpObject = getHTTPObject();

		if(httpObject)
		{
			if(httpObject.overrideMimeType)
			{
				httpObject.overrideMimeType("false");
			}

			httpObject.open('POST', sUrlSoap, false); //-- no asincrono
			httpObject.setRequestHeader("Accept-Language", null);
			httpObject.onreadystatechange = function()
			{
				if(httpObject.readyState == 4 && httpObject.status == 200)
				{
					parser 	= new DOMParser();
					docXml 	= parser.parseFromString(httpObject.responseText, "text/xml");
					iEstado = docXml.getElementsByTagName('Estado')[0].childNodes[0].nodeValue;
					//Llamada a la función que recibe la respuesta del WebService
					setTimeout(function() {
						respuestaWebService(iEstado);
					}, 4000);
					bResp = true;
				}
				else
				{

				}
				return bResp;
			};
			httpObject.send(soapData);
		}
	}
}

function getHTTPObject()
{
	if(OSName != "Android"){
		var xhr = false;
		if (window.ActiveXObject) {
			try {
				xhr = new ActiveXObject("Msxml2.XMLHTTP");
			} catch(e) {
				try {
					xhr = new ActiveXObject("Microsoft.XMLHTTP");
				} catch(e) {
					xhr = false;
				}
			}
		} else if (window.XMLHttpRequest) {
			try {
				xhr = new XMLHttpRequest();
			} catch(e) {
				xhr = false;
			}
		}
		return xhr;
	}
}

/**
 * RESPUESTA WEBSERVICE
 */
function respuestaWebService(iRespuesta)
{
	console.log("respuesta web service:",iRespuesta);
	desbloquearModal();
	switch(iOpcion)
	{
		case LIGA_PAGINAS_WEB:
			if(iFlujoTermino != 1)
			{
				ModalMensaje("Promotor: Favor de continuar con el Guardado de la Solicitud, da click al botón CONTINUAR.");
			}
			else
			{
				setTimeout(function ()
				{
					//closeNavegador();
				},2000);
			}
			break;

		// case GRABAR_AUDIOS:
		// 	validarRespAudio(iRespuesta);
		// 	break;

		case VALIDAR_HUELLAS_CLIENTE:

			if(iContHuellas<=3)
			{
				if(OSName == "Android"){
					if(iRespuesta == 1)
						pasarCaptura();
					else
						ModalMensajeHuellas("La huella del cliente no coincide con la registrada.", parametrosHuellas);//validarHuellaCliente(parametrosHuellas);
				}else{
					//iRespuesta = iFolioSol;
					if(iFolioSol == iRespuesta)
						pasarCaptura();
					else
						ModalMensajeHuellas("La huella del cliente no coincide con la registrada.", parametrosHuellas);//validarHuellaCliente(parametrosHuellas);
				}

			}
			else
			{
				ModalMensaje("La Autentidad del Trabajador NO ha podido ser comprobada...Verifique por Favor!!.");
			}
			break;

		default:
			break;
	}
}

/**************************************</WEBSERVICE EXE>**************************************/

function obtenerCurpSolcontancia(nss,tiposol)
{
	var bResp = '';
	var arrSolC = new Array();

	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:14,nss:nss,tiposol:tiposol},
		success: function(data)
		{
			arrSolC = eval(data);
			if(validarErrorCaseSolExp(arrSolC.error,arrSolC.mensaje,arrSolC.redireccionar))
				bResp = arrSolC.mensaje;

		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});

	return bResp;
}

/*--------------------------------------------Mensajes---------------------------------------------*/
//Cerrar Modal Mensaje
function cerrarConstancia()
{
		divDlgMain.innerHTML="";
		divDlgMain.style.display = 'none';
};

//Metodo para Modal
function bloquearModal()
{
	sHtml= "</br><img width=\"100%\" class=\"logoafore\" src=\"../imagenes/loadingBar1.gif\">";
    var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
	myDlg.espera(400,150,'Espere Un Momento Por Favor...');
	myDlg.mostrar(sHtml);
}

//Metodo para Modal
function desbloquearModal()
{
   //escondemos el div que bloquea pantalla
   $("#divDlgMain").hide();
}

//Mensaje Generales
function ModalMensaje(cMensaje)
{
	var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
	myDlg.crear(500,200, cTituloModal);
	myDlg.mostrar(cMensaje);
}

//Mensaje Para abrir pagina WEB
function ModalMensajeURL(cMensaje,url)
{
	var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
	myDlg.MensajeURL(500,250, cTituloModal,url);
	myDlg.mostrar(cMensaje);
}

//Mensaje Para abrir pagina WEB
function ModalMensajeURLSolVigente(cMensaje,url)
{
	var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
	myDlg.MensajeURLSolVigente(500,250, cTituloModal,url);
	myDlg.mostrar(cMensaje);
}

//Mensaje Para grabar Audio
// function ModalMensajeAudio(cMensaje,parametrosAudio)
// {
// 	var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
// 	myDlg.MensajeAudio(500,250, cTituloModal,parametrosAudio);
// 	myDlg.mostrar(cMensaje);
// }

//Mensaje Para pedir las Huellas
function ModalMensajeHuellas(cMensaje,parametrosHuellas)
{
	var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
	myDlg.MensajeHuellas(500,250, cTituloModal,parametrosHuellas);
	myDlg.mostrar(cMensaje);
}

//Mensaje y Cerrar en Navegador
function MensajeCerrar(sMensaje)
{
	var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
	myDlg.modalcierre(500,250,cTituloModal);
	myDlg.mostrar(sMensaje);
}

//Mensaje y Cerrar en Navegador PREGUNTA
function MensajeCerrarPreg(sMensaje)
{
	var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
	myDlg.modalcierremensaje(500,250,cTituloModal);
	myDlg.mostrar(sMensaje);
}

function closeNavegador()
{
	if(OSName == "Android"){
		sParametro = '';
		iOpcion = CERRAR_NAVEGADOR;
		opcionEjecuta(sParametro);
		$("#divDlgMain").hide();
	}
	else
	{
		//firefox
		if(navigator.appName.indexOf('Netscape') >= 0 )
		{
			//NAVEGADOR FIREFOX
			javascript:window.close();
		}
		else
		{
			if(navigator.appName.indexOf('Microsoft') >= 0)
			{//internet explorer
				var ventana = window.self;
				ventana.opener = window.self;
				ventana.close();
			}
		}
	}
}

/*--------------------------------------------Información---------------------------------------------*/
//Metodo que obtiene los datos de promotor para ponerlos en la parte superior del html que se obtiene de la una funcion
function obtnerinformacionpromotor(empleado)
{
	var bResp = false;
	var arrInformacionProm = new Array();

	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:1,empleado:empleado},
		success: function(data)
		{
			arrInformacionProm = eval(data);

			//Validacion para saber si esta registrado el promotor o no
			if(arrInformacionProm.iCodigo == 0)
			{
				$("#txtFechaDia").val(arrInformacionProm.dFechaActual);
				$("#txtUsuario").val(arrInformacionProm.cNombre);
				$("#txtNomTienda").val(arrInformacionProm.cTienda);
				bResp = true;
			}
			else
			{
				MensajeCerrar(arrInformacionProm.cMensaje);
			}
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});

	return bResp;
}

function llenadoTipoRegistro()
{
	var arrTipoSol = new Array();
	var addOption  = '';
	var cbxTipoSol = document.getElementById('cbxTipoSolicitud');

	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:2},
		success: function(data)
		{
			arrTipoSol = eval(data);
			var addOption = "<option value='-1'>TIPO SOLICITUD ...</option>";
			if(arrTipoSol.length != 0)
			{
				for (var i = 0; i < arrTipoSol.length; i++) {
					
				    var descripTipo = arrTipoSol[i].cdescripcion;
					descripTipo = descripTipo.replace('?006e00200049','ÓN I');

					arrTipoSolicitud.push
					({
						"idSol":		i,
						"itiposol":		arrTipoSol[i].itiposolictud,
						"itipotras":	arrTipoSol[i].itipotraspoaso,
						"descripcion": descripTipo
					});

					//INE//Si el tipotraspaso = 829 y 828 que no los muestre en el combobox
					if(arrTipoSol[i].itipotraspoaso != 829 && arrTipoSol[i].itipotraspoaso != 828)
					{
						addOption += "<option value='" + arrTipoSolicitud[i].idSol + "'>" + arrTipoSolicitud[i].descripcion + "</option>";
					}
					//addOption += "<option value='" + arrTipoSolicitud[i].idSol + "'>" + arrTipoSolicitud[i].descripcion + "</option>";
				}
			}
			cbxTipoSol.innerHTML = addOption;
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});
}

function validacionCurpAfiliacion(curp, tiposol)
{
	var arrValCurp = new Array();
	iNss 		= 0;
	iFolioSol 	= 0;
	var	bRespuesta =false;
	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:3,curp:curp,tiposol:tiposol},
		success: function(data)
		{
			arrValCurp = eval(data);
			if(arrValCurp.estatus == 1){
				bResp = validarErrorCaseSolExp(arrValCurp.error,arrValCurp.mensaje,arrValCurp.redireccionar);
				iNss = arrValCurp.nss;
	
				iFolioSol = arrValCurp.mensaje;
	
				if(bResp)
				{
					if(validarVideo(arrValCurp.mensaje))
					{
						validarEnrolamientoExpediente(iNss,curp);
						bRespuesta = true;
					}
				}
			}else{
				MensajeCerrar("Error al consultar la vigencia de la solicitud, favor de Comunicarse a Mesa de Ayuda!");
			}
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});
	return bRespuesta;
}

function levantarPagina(url)
{
	iOpcion = LIGA_PAGINAS_WEB;
	opcionEjecuta(url);
	cerrarConstancia();
}

function validarEnrolamientoExpediente(nss,curp)
{
	if(validarExpediente(curp,nss))
	{
		if(validarFolioSol(iFolioSol))
		{
			var trabajadorSinManos 	= 0;
			trabajadorSinManos 		= cuentaConManos(iFolioSol);
			itieneexcepcion			= tieneexcepcion(iFolioSol);

			if( (itieneexcepcion == 1)|| trabajadorSinManos == 1 || trabajadorSinManos == 2)
			{
				pasarCaptura();
			}
			else
			{
				ipServidorSPA = obtenerIPServidor();
				parametrosHuellas = ipServidorSPA + " 1 " + iFolioSol;
				ModalMensajeHuellas("Promotor: indicarle al trabajador que debe colocar su indice derecho sobre el sensor para validar la captura de afiliación.", parametrosHuellas);
				validarHuellaCliente(parametrosHuellas);
			}
		}
		else
		{
			MensajeCerrar("Ya existe el folio asociado a una CURP, Favor de Comunicarse a Mesa de Ayuda!.");
		}
	}
}

function tieneexcepcion(foliosol)
{
	var bResp 		= -1;
	
	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:17,foliosol:foliosol},
		success: function(data)
		{
			bResp = eval(data);
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});
	return bResp;
		
}

function obtenerFolioEnrolamiento(foliosol)
{
	var bResp = 0;
	var arrFolioEnrol = new Array();

	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:6,foliosol:foliosol},
		success: function(data)
		{
			arrFolioEnrol = eval(data);

			if(arrFolioEnrol.folioenrolamiento != 0)
			{
				bResp = arrFolioEnrol.folioenrolamiento;
			}
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});

	return bResp;
}

// function obtenerNombreAdudio(foliosol)
// {
// 	var bResp = '';
// 	var arrNombreAudio = new Array();

// 	$.ajax
//     ({
// 		async: false,
// 		cache: false,
// 		url: ligaCase,
// 		type: 'POST',
// 		dataType: 'json',
// 		data:{opcion:7,foliosol:foliosol},
// 		success: function(data)
// 		{
// 			arrNombreAudio = eval(data);
// 			bResp = arrNombreAudio.nombreaudio;
// 		},
//         error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
//         beforeSend: function(){}
// 	});

// 	return bResp;
// }

// function validarRespAudio(iRespuesta)
// {
// 	var sCurp = $('#txtCURP').val().toUpperCase();
// 	if(iRespuesta == 1)
// 	{
// 		if(actualizaAudioGrabado(folioEnrol))
// 		{
// 			validarEnrolamientoExpediente(iNss,sCurp);
// 		}
// 	}
// 	else
// 	{
// 		MensajeCerrar("El audio no se grabó adecuadamente. Vuelva a intentarlo por favor.");
// 	}
// }

// function actualizaAudioGrabado(folioenrol)
// {
// 	var bResp = false;
// 	var arrAudio = new Array();

// 	$.ajax
//     ({
// 		async: false,
// 		cache: false,
// 		url: ligaCase,
// 		type: 'POST',
// 		dataType: 'json',
// 		data:{opcion:8,folioenrol:folioenrol},
// 		success: function(data)
// 		{
// 			arrAudio = eval(data);
// 			if(arrAudio.respuesta == 1)
// 			{
// 				bResp = true;
// 			}
// 		},
//         error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
//         beforeSend: function(){}
// 	});

// 	return bResp;
// }

function validarExpediente(curp,nss)
{

	var bResp = false;
	var arrExpediente = new Array();

	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:5,curp:curp},
		success: function(data)
		{
			arrExpediente = eval(data);
			var iErrorExp = arrExpediente.error;

			if(arrExpediente.mensaje != nss && arrExpediente.error == 0)
			{
				iErrorExp = -1;
			}
			bResp = validarErrorCaseSolExp(iErrorExp,arrExpediente.mensaje,arrExpediente.redireccionar);
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});

	return bResp;
}

function validarErrorCaseSolExp(error,mensaje,redireccionar)
{
	var bResp = false;
	switch (error)
	{
		case -2: //Mostrar Mensaje de Error, Si ya Esta Afiliado con nosotros o se encuentra en otro porceso
			ModalMensaje(mensaje);
			break;
		case -1:
			//no dejamos avanzar el flujo
			ModalMensajeURL("El NSS capturado para esa CURP, es distinto al que capturaste en la Solicitud de la Constancia para Registro o Traspaso.",redireccionar);
			break;
		case 0: //Continuar Flujo Normal
		bResp = true;
			break;
		case 1: //Parametros incorrectos, Solicitud capturada vigente con estatus proceso diferente a 4
			MensajeCerrar(mensaje);
			break;
		case 2://Solicitud vigente pero Rechaza, solicitud no vigente, no tiene solicitud capturada.
			ModalMensaje(mensaje);
		case 3://Solicitud Vigente con constraseña Vigente
			break;
		case 4://Solicitud Vigente con contraseña vencida.
			mensaje += " ¿El trabajador desea realizar el trámite de Registro / Traspaso?";
			redireccionar = redireccionar.replace("{0}", iEmpleado);
			ModalMensajeURLSolVigente(mensaje,redireccionar); //noPromotor
			break;
		case 5://No cuenta con Expediente Independiente valido
			redireccionar = redireccionar.replace("{0}", iEmpleado); //noPromotor
			redireccionar = redireccionar.replace("{1}", iFolioSol); //SolicitudFolio
			//redireccionar = redireccionar.replace("{2}", SolicitudTipoID); //SolicitudTipoID
			redireccionar = redireccionar.replace("{2}", iSolReg); //Tipo Servicio
			ModalMensajeURL(mensaje,redireccionar); //noPromotor, SolicitudFolio, SolicitudTipoID
			break;
		default:
			MensajeCerrar("Ocurrio una excepción al validar los prerequisitos, no se puede continuar con el proceso de afiliación.");
			break;
	}

	return bResp;
}

function validarFolioSol(foliosol)
{
	var bResp 		= false;
	var arrFoliSol 	= new Array();

	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:9,foliosol:foliosol},
		success: function(data)
		{
			arrFoliSol = eval(data);
			if(arrFoliSol.valor == 0)
			{
				bResp = true;
			}
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});

	return bResp;
}

function cuentaConManos(foliosol)
{
	var bResp 		= -1;
	var arrManos 	= new Array();

	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:10,foliosol:foliosol},
		success: function(data)
		{
			arrManos = eval(data);
			bResp = arrManos.respuesta;
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});

	return bResp;
}

function obtenerIPServidor()
{
	var bResp 		= '';
	var arrIP 	= new Array();

	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:11},
		success: function(data)
		{
			arrIP = eval(data);
			bResp = arrIP.IP;
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});

	return bResp;
}

function validarHuellaCliente(parametros)
{
	console.log("validar huella cliente");
	iContHuellas++;
	iOpcion = VALIDAR_HUELLAS_CLIENTE;
	opcionEjecuta(parametros);
	cerrarConstancia();
	//pasarCaptura();
}

// funcion que sirve para indicar que tipo de de seleccion se hizo en el combobox 
//Ejemplo: si seleccionas traspaso normal el esimss es = 1
//Pero si se selecciona el traspaso co autenticacion ine es = 829 
function pasarCaptura()
{
	var iOpcionPag 	= 0;
	var iReImpre	= 0;
	var sUrl 		= '';
	esimss		= 0;


	if(iSolReg == 27 && SolicitudTipoID == 1)
	{
		//INE //Si se selecciona traspaso ISSSTE con autenticacion INE, esmiss = 829
		if(validarAutenticacion == AUTENTICACION_INE){
			esimss = TRASPASO_TIPO_ISSSTE_INE;
		}
		else if (validarAutenticacion == AUTENTICACION_BCPL){
			esimss = TRASPASO_TIPO_ISSSTE_BCPL;
		}
		else if (validarAutenticacion == AUTENTICACION_DTL) {// AUTENTICACION_DTL = validacionine = 3
            esimss = TRASPASO_TIPO_ISSSTE_DTL; // esimss toma valor 833
        }
		//Si se selecciona traspaso ISSSTE sin autenticacion INE
		else{esimss = 1;}
	}	
	else if(iSolReg == 27 && SolicitudTipoID == 2)
	{
		if (validarAutenticacion == AUTENTICACION_BCPL){
			esimss = TRASPASO_TIPO_ISSSTE_BCPL;
		}
		else if (validarAutenticacion == AUTENTICACION_DTL) {// AUTENTICACION_DTL = validacionine = 3
            esimss = TRASPASO_TIPO_ISSSTE_DTL; // esimss toma valor 833
        }
		//Si se selecciona traspaso ISSSTE sin autenticacion INE
		else{esimss = 2;}
	}		
	else
	{
		//INE //Si se selecciona traspaso IMSS con autenticacion ine, esmiss = 828
		if(validarAutenticacion == AUTENTICACION_INE)
		{
			esimss = TRASPASO_TIPO_NORMAL_INE;
		}
		else if (validarAutenticacion == AUTENTICACION_BCPL){
			esimss = TRASPASO_TIPO_IMSS_BCPL;
		}
		else if (validarAutenticacion == AUTENTICACION_DTL){
            esimss = TRASPASO_TIPO_IMSS_DTL; // esimss toma valor 832
        }
		//Si se selecciona traspaso IMSS sin autenticacion ine
		else{esimss = 0;}		
	}
	if(guardarInfoAfiliacion(iFolioSol,iSolReg,esimss,SolicitudTipoID))
	{
		if(iSolReg == 26 || iSolReg == 33 )
			iOpcionPag = 8;//Registro"
		else if(iSolReg == 27)
			if (validarAutenticacion > 0)//INE//
			{
				//En caso de que validacionINE se cumpla, al ser tipo de afiliacion traspaso
				//omitira los decomentos que se muestran al pasar la pantalla principal de la solicitud hacia el motivoafiliacion.
				iOpcionPag = 8;
			} 
			else
			{
				iOpcionPag = 9;//Traspaso
			}
		sUrl = obtenerPaginaRedireccionar(iOpcionPag,iFolioSol,iEmpleado,iReImpre);
		if(sUrl != '')
		{
			iOpcion = LIGA_PAGINAS_WEB;
			iFlujoTermino = 1;
			
			opcionEjecuta(sUrl);
		}
	}
}

function guardarInfoAfiliacion(foliosol,tiposol,esimss,tipotrasp)
{
	var bResp 		= 0;
	var arrGuardar 	= new Array();

	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:12,foliosol:foliosol,tiposol:tiposol,esimss:esimss,tipotrasp:tipotrasp},
		success: function(data)
		{
			arrGuardar = eval(data);
			bResp = arrGuardar.respuesta;
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});

	return bResp;
}

function obtenerPaginaRedireccionar(opcionpag,foliosol,empleado,reimpre)
{
	var bResp 		= '';
	var arrObtLiga 	= new Array();

	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:13,opcionpag:opcionpag,foliosol:foliosol,empleado:empleado,reimpre:reimpre},
		success: function(data)
		{
			arrObtLiga = eval(data);
			bResp = arrObtLiga.destino;
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});

	return bResp;
}

function consultarNSS(curp)
{
	var bResp = '';
	var arrNss = new Array();

	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:15,curp:curp},
		success: function(data)
		{
			arrNss = eval(data);
			bResp = arrNss.nss;
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});

	return bResp;
}

// MOVIL
/* Obtener parametros get desde el webview android */
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

//funcion para desplegar un mensaje
function mostrarMensaje(mensaje,sTitle)
{
  $("#divMensaje").dialog
	({
		title: sTitle,
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+mensaje+"</p>");
		},
		buttons:
			{
				"Aceptar" : function()
				{

					$(this).dialog( "close" );

				}
			}
	});
}

// MOVIL
/* Funcion que recibe la respuesta del componente EnrolMovil */
function getComponenteHuella(estatusProceso, jsonFingers, opcionHuella, jsonTemplate){
	if(opcionHuella==0){
		// HUELLA PROMOTOR
		document.getElementById("fingerString0").value = jsonFingers;
		var template = JSON.parse(jsonTemplate);
		document.getElementById("fingerTemplate0").value = template["FingerTemplate"][0]["Template"];

		var statusFinger0 = validarHuellas(0, 3);

		if(statusFinger0 == 1 || statusFinger0 == 2){
			statusFinger0=1;
		}

		setTimeout(function ()
		{
			respuestaWebService(statusFinger0, iOpcion);
		},4000);

	}

}


function validarHuellas(oHuella, iTipoP)
{
	var bRetorna = '';
	var iNumFuncionario = iEmpleado;
	var iFolioSolicitud = iFolioSol;
	var iTipoPerson = iTipoP;

	if(oHuella == 0){
		var base64Imagen = $('#fingerString0').val();
		var cTemplate = $('#fingerTemplate0').val();
	}

	$.ajax(
	{
		async: false,
		cache: false,
		data:{iNumFuncionario: iNumFuncionario, iFolioSolicitud: iFolioSolicitud, iTipoPerson:iTipoPerson, cTemplate: cTemplate},
		 url: '../apirestafiliacion/codeigniter/api/CtrlconstanciaAfiliacion/validarHuellaCertificado',
		 headers:{
            "Authorization":"Basic YWRtaW46MTIzNA==",
            "x-api-key":"1234",
            "Content-Type":"application/x-www-form-urlencoded"
		 },
		 type: 'POST',
		 dataType: 'json',
		 success: function(data)
		 {
				bRetorna =data;

		},
		error: function(xhr,status,error){},
	});

	return bRetorna;
}
//funcion que sirve para limitar el  uso de caracteres
function soloLetras(e)
{
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " 0123456789abcdefghijklmnñopqrstuvwxyz";
    especiales = [8,48,37,39,49,50,51,52,54,55,56,57]; //DEC del Codigo ASCII

    tecla_especial = false
    for(var i in especiales){
 if(key == especiales[i]){
     tecla_especial = true;
     break;
        }
    }
    if(letras.indexOf(tecla)==-1 && !tecla_especial){
		return false;
	}
}

//funcion que valida la existencia del grabado de video, mostrando mensajes dependiendo del caso de respuesta que se obtenga
function validarVideo(foliosol)
{
	var bResp = false;
	var arrVideo = new Array();
	console.log("valida video con folio:",foliosol);
	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:4,foliosol:foliosol},
		success: function(data)
		{
			arrVideo = eval(data);
			switch (parseInt(arrVideo.error))
			{
				case 0:
					ModalMensaje("El trabajador no cuenta con grabado de video. favor de realizarlo en la captura de datos.");
					break;
				case 1:
					bResp = true;
					break;
				case 2:
					ModalMensaje("El trabajador no se encuentra enrolado, favor de realizar su enrolamiento.");
					break;
				case 3:
					ModalMensaje("No se encontró registro de guardo de video.");
					break;
				default:
					ModalMensaje("Ocurrio una excepción al obtener información de la solicitud del trabajador.");
					break;
			}
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});
	return bResp;
}

function validarKeypressCurp(event)
{
	if(!(arrCaracteresCurp.indexOf(event.which) > -1) )
	{
		if(!(event.which >= 65 && event.which<= 90 || event.which >= 97 && event.which <= 122 || event.which >= 48 && event.which <= 57))
		{
			event.preventDefault();
		}
	}
}

function encode_utf8(s) {
  return unescape(encodeURIComponent(s));
}

function decode_utf8(s) {
  return decodeURIComponent(escape(s));
}

//Funcion para validar si folio cuenta con AutenticacionIne
function validacionine(foliosol)
{
	var iResp 		= 0;
	var arrexis	= new Array();
	
	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:18,foliosol:foliosol},
		success: function(data)
		{
			datos = (data);
			iResp = datos.respuesta;
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});
	return iResp;
}
function caminoWSRenapo(cCurp, iEmpleado, iFolioSol, iEsTitular)
{//metodo que invoca el WS de ranapo
	console.log("CaminoWSRenapo -> Curp: "+ cCurp + ", Empleado: " + iEmpleado + ", Folio: " + iFolioSol + ", Es Titular: " + iEsTitular);
	var iRespuesta = -1;
	var bRespuesta = false;
	$.ajax
		({
			async: false,
			cache: false,
			url: ligaCase,
			type: 'POST',
			dataType: 'json',
			data: { opcion			:23, 
					curp			:cCurp, 
					empleado		:iEmpleado,
					foliosol		:iFolioSol,
					iEsTitular		:iEsTitular},
			success: function (data) {
				iRespuesta = data.codigoerrorrenapo;

				if(iRespuesta == 0 || iRespuesta == 1)
				{
					bRespuesta = true;
				}
			}
		});

	return bRespuesta;
}
function ValidaConsultaRenapoPrevia(cCurp, iEsTitular)
{
	var bResp = false;
	var arrRenapo = new Array();

	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:24,curp:cCurp,iEsTitular:iEsTitular},
		success: function(data)
		{
			arrRenapo = eval(data);

			//Validacion para saber si fue consultado ante renapo previamente.
			if(arrRenapo.iRenapo == 1)
			{
				bResp = true;
			}
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});

	return bResp;
}
function obtenerCurpBeneficiario(cCurp)
{
	var cCurpB = '';
	var arrRenapo = new Array();

	$.ajax
    ({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'json',
		data:{opcion:25,curp:cCurp},
		success: function(data)
		{
			arrRenapo = eval(data);
			//Validacion para saber si fue consultado ante renapo previamente.
			if(arrRenapo.Exito == 1)
			{
				cCurpB = arrRenapo.cCurpSolicitante;
			}
		},
        error: function(a, b, c){alert("error ajax " + a + " " + b + " " + c);},
        beforeSend: function(){}
	});

	return cCurpB;
}