<?php
	include_once('Clases/CRetCapturaafiliacion.php');
	include_once('Clases/CMetodoGeneral.php');
	include_once("JSON.php");
	
	$json 			= new Services_JSON();
	$arrResp 		= array();
	$objGn 			= new CMetodoGeneral();
	$sIpRemoto 		= '';
	$opcion 		= isset($_POST['opcion']) 		? $_POST['opcion'] 		: 0;

	$iEmpleado 		= isset($_POST['empleado']) 	? $_POST['empleado'] 	: 0;
	$sCurp 			= isset($_POST['curp']) 		? $_POST['curp'] 		: 0;
	$foliosol 		= isset($_POST['foliosol']) 	? $_POST['foliosol'] 	: 0;
	$nss 			= isset($_POST['nss']) 			? $_POST['nss'] 		: 0;
	$folioenrol 	= isset($_POST['folioenrol']) 	? $_POST['folioenrol'] 	: '';
	$opcionpag 		= isset($_POST['opcionpag']) 	? $_POST['opcionpag'] 	: '';
	$reimpre 		= isset($_POST['reimpre']) 		? $_POST['reimpre'] 	: '';
	$iEsTitular		= isset($_POST['iEsTitular']) 	? $_POST['iEsTitular'] 	: 0;
	//fnguardardatosstmpcapturaafiliacion
	//pantalla tiposolicitud Opcion 1
	$iOpcGuar 		= 1;
	$iTipoSol 		= isset($_POST['tiposol']) 		? $_POST['tiposol'] 	: 0;
	$esimss 		= isset($_POST['esimss']) 		? $_POST['esimss'] 		: '';
	//pantalla tipo traspaso Opcion 2
	$tipotrasp 		= isset($_POST['tipotrasp']) 	? $_POST['tipotrasp'] 	: '';
	$cFolioEdo 		= isset($_POST['folioedo']) 	? $_POST['folioedo'] 	: '';
	$cAforeEdo 		= isset($_POST['aforeedo']) 	? $_POST['aforeedo'] 	: '';
	$cCuatriEdo 	= isset($_POST['cuatriedo']) 	? $_POST['cuatriedo'] 	: '';
	$aforeced 		= isset($_POST['aforeced']) 	? $_POST['aforeced'] 	: 0;
	//pantalla motivo Opcion 3
	$cMotivo 		= isset($_POST['motivo']) 		? $_POST['motivo'] 		: '';
	$cFolioImpli	= isset($_POST['folioimpli']) 	? $_POST['folioimpli'] 	: '';
	$iTipoAdmon		= isset($_POST['tipoadmon']) 	? $_POST['tipoadmon'] 	: 0;
	$cDepPrev 		= isset($_POST['depprevi']) 	? $_POST['depprevi'] 	: '';
	$sDato 			= isset($_POST['sDato']) 	? $_POST['sDato'] 	: '';

    $sIpRemoto = $objGn->getRealIP();
	
	switch($opcion)
	{
        case 1:
			$arrResp = CCapturaafiliacion::obtnerinformacionpromotor($iEmpleado);
		    break;
		case 2:
			$arrResp = CCapturaafiliacion::obtenertiposolicitud();
		    break;
		case 3:
			$arrResp = CCapturaafiliacion::validacionCurpAfiliacion($sCurp,$iTipoSol,$sIpRemoto);
			break;
			// El case 4 valida la existencia del grabado del video.
		case 4:
			$arrResp = CCapturaafiliacion::validarVideo($foliosol);
		    break;
		case 5:
			$arrResp = CCapturaafiliacion::validarExpediente($sCurp,$sIpRemoto);
		    break;
		case 6:
			$arrResp = CCapturaafiliacion::obtenerFolioEnrolamiento($foliosol);
		    break;
		case 9:
			$arrResp = CCapturaafiliacion::validarFolioSol($foliosol);
		    break;
		case 10:
			$arrResp = CCapturaafiliacion::cuentaConManos($foliosol);
			break;
		case 11:
			$arrResp = CCapturaafiliacion::obtenerIPServidor($sIpRemoto);
			break;
		case 12:
			$arrResp = CCapturaafiliacion::guardarInfoAfiliacion($iOpcGuar,$foliosol,$iTipoSol,$esimss,$tipotrasp,$cFolioEdo,$cAforeEdo,$cCuatriEdo,$cMotivo,$cFolioImpli,$iTipoAdmon,$cDepPrev,$aforeced);
			break;
		case 13:
			$arrResp = CCapturaafiliacion::obtenerPaginaRedireccionar($opcionpag,$foliosol,$iEmpleado,$sIpRemoto,$reimpre);
			break;
		case 14:
			$arrResp = CCapturaafiliacion::obtenerCurpSolcontancia($nss,$iTipoSol,$sIpRemoto);
			break;
		case 15:
			$arrResp = CCapturaafiliacion::consultarNSS($sCurp);
			break;
		case 16:
			$arrResp = CCapturaafiliacion::ConsultarSolicitudConstancia($sDato);
			break;
		case 17:
			$arrResp = CCapturaafiliacion::tieneexcepcion($foliosol);
			break;
		case 18:
			$arrResp = CCapturaafiliacion::validacionine($foliosol);
			break;
		case 23: 
			$arrResp = CCapturaafiliacion::caminoWSRenapo($sCurp, $iEmpleado, $foliosol, $iEsTitular);
			break;
		case 24: 
			$arrResp = CCapturaafiliacion::ValidaConsultaRenapoPrevia($sCurp, $iEsTitular); 
			break;
		case 25: 
			$arrResp = CCapturaafiliacion::obtenerCurpBeneficiario($sCurp); 
			break;
	}
	
	echo $json->encode($arrResp);
?>