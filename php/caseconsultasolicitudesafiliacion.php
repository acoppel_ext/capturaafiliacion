<?php
	include_once('Clases/CRetCapturaafiliacion.php');
    include_once('Clases/CRetConsultaSolicitudesAfiliacion.php');
	include_once('Clases/CMetodoGeneral.php');
	include_once("JSON.php");
	
	$json 			= new Services_JSON();
	$arrResp 		= array();
	$objGn 			= new CMetodoGeneral();
	$sIpRemoto 		= '';
	$opcion 		= isset($_POST['opcion']) 			? $_POST['opcion'] 			: 0;

	$iEmpleado 		= isset($_POST['empleado']) 		? $_POST['empleado'] 		: 0;
    $foliosol 		= isset($_POST['foliosol']) 		? $_POST['foliosol'] 		: 0;
	$curp 			= isset($_POST['curp']) 			? $_POST['curp'] 			: "";
	$nss 			= isset($_POST['nss']) 				? $_POST['nss'] 			: 0;
    $nocliente		= isset($_POST['nocliente']) 		? $_POST['nocliente'] 		: "";
    $tipodom        = isset($_POST['tipodom']) 	    	? $_POST['tipodom'] 		: 0;
	$compdom        = isset($_POST['compdom']) 	    	? $_POST['compdom'] 		: 0;
	$ocupacionprof  = isset($_POST['ocupacionprof']) 	? $_POST['ocupacionprof'] 	: 0;
	$activinegocio 	= isset($_POST['activinegocio']) 	? $_POST['activinegocio'] 	: 0;
	$opcionpag 		= isset($_POST['opcionpag']) 		? $_POST['opcionpag'] 		: '';
	$reimpre 		= isset($_POST['reimpre']) 			? $_POST['reimpre'] 		: '';
	
    $sIpRemoto = $objGn->getRealIP();
	
	switch($opcion)
	{
        //Metodos utilizados en CAPTURAAFILIACION
        case 1:
			$arrResp = CCapturaafiliacion::obtnerinformacionpromotor($iEmpleado);
		    break;
		case 13:
			$arrResp = CCapturaafiliacion::obtenerPaginaRedireccionar($opcionpag,$foliosol,$iEmpleado,$sIpRemoto,$reimpre);
			break;
		case 16:
			$arrResp = CCapturaafiliacion::obtenerfecha();
		    break;

        //Metodos utilizados en CONSULTA SOLICITUDES AFILIACIONES
		case 2:
			$arrResp = CConsultaSolicitudesAfiliacion::obtenerSolicitudAfiliacion($foliosol, $curp, $nss, $nocliente);
		    break;
        case 3:
			$arrResp = CConsultaSolicitudesAfiliacion::obtenerDatosSolicitudAfiliacionGeneral($foliosol);
		    break;
        case 4:
			$arrResp = CConsultaSolicitudesAfiliacion::obtenerDomicilio($foliosol,$tipodom);
		    break;
		case 5:
			$arrResp = CConsultaSolicitudesAfiliacion::obtenerComprobanteDomicilio($compdom);
		    break;
		case 6:
			$arrResp = CConsultaSolicitudesAfiliacion::obtenerDatosSolicitudAfiliacionTelefonos($foliosol);
		    break;
		case 7:
			$arrResp = CConsultaSolicitudesAfiliacion::obtenerOcupacionProf($ocupacionprof);
		    break;
		case 8:
			$arrResp = CConsultaSolicitudesAfiliacion::obtenerActividadGiro($activinegocio);
		    break;
		case 9:
			$arrResp = CConsultaSolicitudesAfiliacion::obtenerDatosSolicitudAfiliacionBeneficiarios($foliosol);
		    break;
		case 10:
			$arrResp = CConsultaSolicitudesAfiliacion::obtenerDiagnosticoSolicitud($foliosol);
			break;			
	}
	
	echo $json->encode($arrResp);
?>