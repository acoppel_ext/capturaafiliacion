<?php
include_once('Clases/CRetCapturaafiliacion.php');
include_once('Clases/CRetMotivoafiliacion.php');
include_once('Clases/CMetodoGeneral.php');
include_once("JSON.php");

$json 			= new Services_JSON();
$arrResp 		= array();
$objGn 			= new CMetodoGeneral();
$sIpRemoto 		= '';
$opcion 		= isset($_POST['opcion']) 		    ? $_POST['opcion'] 		    : 0;

$iEmpleado 		= isset($_POST['empleado']) 	    ? $_POST['empleado'] 	    : 0;
$foliosol 		= isset($_POST['foliosol']) 	    ? $_POST['foliosol'] 	    : 0;
$opc 		    = isset($_POST['opc']) 	            ? $_POST['opc'] 	        : 0;
$tipoafi 		= isset($_POST['tipoafi']) 	        ? $_POST['tipoafi'] 	    : 0;
$esimss 		= isset($_POST['esimss']) 	        ? $_POST['esimss'] 	        : 0;
$tipotraspaso 	= isset($_POST['tipotraspaso'])     ? $_POST['tipotraspaso']    : 0;
$motregi 	    = isset($_POST['motregi'])          ? $_POST['motregi']         : '';
$folconstaimple = isset($_POST['folconstaimple'])   ? $_POST['folconstaimple']  : '';
$iTipoAdmin 	= isset($_POST['iTipoAdmin'])       ? $_POST['iTipoAdmin']      : 0;
$depo 	        = isset($_POST['depo'])   	       	? $_POST['depo']   		    : '';
$fecha 			= isset($_POST['fecha']) 	   	 	? $_POST['fecha'] 	    	: '1900-01-01 00:00:00';
$opcionpag 		= isset($_POST['opcionpag']) 		? $_POST['opcionpag'] 		: '';
$reimpre 		= isset($_POST['reimpre']) 			? $_POST['reimpre'] 		: '';
$foliosolpro	= isset($_POST['folioprocesar']) 	? $_POST['folioprocesar'] 	: '';

//Folio 877
$folioRegTras	= isset($_POST['folioRegTras']) 	? $_POST['folioRegTras'] 	: '';
$keyX			= isset($_POST['keyX']) 			? $_POST['keyX'] 			: '';

//INE//
$foliosemilla	= isset($_POST['foliosemilla']) 	? $_POST['foliosemilla'] 	: '';
$opcionINE		= isset($_POST['opcionINE']) 		? $_POST['opcionINE'] 		: 0;
$sCurp			= isset($_POST['curp']) 			? $_POST['curp'] 			: '';
$opcioncombo 	= isset($_POST['opcioncombo']) 		? $_POST['opcioncombo'] 	: 0;
$folioine		= isset($_POST['ifolioine']) 		? $_POST['ifolioine'] 		: 0;
$iopcionLigaine = isset($_POST['iopcionLigaine']) 		? $_POST['iopcionLigaine'] 		: 0;
$url			= isset($_POST['url']) 	? $_POST['url'] 		: '';
$sIpRemoto = $objGn->getRealIP();

switch ($opcion) {
		//Metodos utilizados en CAPTURAAFILIACION
	case 1:
		$arrResp = CCapturaafiliacion::obtnerinformacionpromotor($iEmpleado);
		break;
	case 9:
		$arrResp = CCapturaafiliacion::validarFolioSol($foliosol);
		break;
	case 13:
		$arrResp = CCapturaafiliacion::obtenerPaginaRedireccionar($opcionpag, $foliosol, $iEmpleado, $sIpRemoto, $reimpre);
		break;
	case 16:
		$arrResp = CCapturaafiliacion::obtenerfecha();
		break;

		//Metodos utilizados en MOTIVOAFILIACION
	case 2:
		$arrResp = CMotivoafiliacion::validarFolioSolicitud($foliosol);
		break;
	case 3:
		$arrResp = CMotivoafiliacion::llenarcbxMotivoRegistro();
		break;
	case 4:
		$arrResp = CMotivoafiliacion::llenarcbxTipoAdmin();
		break;
	case 5:
		$arrResp = CMotivoafiliacion::guardarDatosMotivoAfiliacion($opc, $foliosol, $tipoafi, $esimss, $tipotraspaso, $motregi, $folconstaimple, $iTipoAdmin, $depo);
		break;
	case 6:
		$arrResp = CMotivoafiliacion::guardarSolicitudBD($foliosol, $iEmpleado, $sIpRemoto, $fecha);
		break;
	case 7:
		$arrResp = CMotivoafiliacion::fnexistenciafolioprocesar($foliosol, $foliosolpro);
		break;
	case 8:
		$arrResp = CMotivoafiliacion::existenciafechaProcesar($foliosol);
		break;
		//INE//Para validar si se tiene autenticaci�n INE
	case 10:
		$arrResp = CMotivoafiliacion::validacionine($foliosol);
		break;
		//INE//Para validar si la fecha es correcta o si el folio es reutilizable
	case 11:
		$arrResp = CMotivoafiliacion::fnvalidavigenciasemilla($foliosol, $foliosemilla);
		break;
		//INE//Para obtener la curp
	case 12:
		$arrResp = CMotivoafiliacion::fnobtenercurpconstancia($foliosol);
		break;
		//INE//Para generar codigo de autenticacion
	case 14:
		$arrResp = CMotivoafiliacion::generarFolioIne($sCurp, $foliosol);
		break;
		//INE// Invocacion a phyton para realizar el envio de mensaje
	case 15:
		$arrResp = CMotivoafiliacion::python($folioine, $opc);
		break;
		//INE//valida si se encuentra el registro en la tabla y en que estatus dejo el envio de Sms al tabajador
	case 17:
		$arrResp = CMotivoafiliacion::fnvalidastatusenvioine($foliosol);
		break;
	case 18:
		$arrResp = CMotivoafiliacion::fnobtenerfolioine($foliosol);
		break;
	case 19:
		$arrResp = CMotivoafiliacion::fnobtenerparaivr($sCurp,$folioine);
		break;
	case 20:
		$arrResp = CMotivoafiliacion::fnactualizaropcioncombo($sCurp, $opcioncombo);
		break;
	case 21:
		$arrResp = CMotivoafiliacion::fnobtenerLigaIVR($iopcionLigaine);
		break;
	case 22:
		$arrResp = CMotivoafiliacion::fnenvioIVR();
		break;
	case 23:
		$arrResp = CMotivoafiliacion::obtenerConfiguracionWs();
		break;
	case 24:
		$arrResp = CMotivoafiliacion::agregarDatosConsultaFolio($iEmpleado, $sCurp, $tipoafi, $folioRegTras, $foliosol);
		break;
	case 25:
		$arrResp = CMotivoafiliacion::validarDatosConsultarFolio($keyX);
		break;
	case 26:
		$arrResp = CMotivoafiliacion::actualizarconsultaregtra($foliosol, $sCurp, $keyX, $foliosolpro);
		break;
	case 27:
		$arrResp = CMotivoafiliacion::obTenercurpTitular($foliosol);
		break;
	case 28: 
		$arrResp = CMotivoafiliacion::llamarivr($url); 
	break;	
	case 29: 
		$arrResp = CMotivoafiliacion::fnobtenersemillaafiliacion($foliosol); 
	break;			
}

echo $json->encode($arrResp);
?>