<?php
include_once ('global.php');
include_once ('CMetodoGeneral.php');
include_once("JSON.php");
include_once('Capirestcapturaafiliacion.php');

$json = new Services_JSON();

class CConsultaSolicitudesAfiliacion
{
	//OPCION 2
	public static function obtenerSolicitudAfiliacion($foliosol, $curp, $nss, $nocliente)
	{
		$arrDatos 	= array();
		$objGn 		= new CMetodoGeneral();
        $objAPI 	= new Capirestcapturaafiliacion();

		$arrAPI = array('foliosol' => $foliosol,
						'curp' => $curp,
						'nss' => $nss,
						'nocliente' => $nocliente
					);
		try
		{
			$objGn->grabarLogx("Inicio de ejecucion de API Rest obtenerSolicitudAfiliacion");
			$resultAPI = $objAPI->consumirApi('obtenerSolicitudAfiliacion',$arrAPI);
			$objGn->grabarLogx("Respuesta Api: ".$resultAPI);
			$objGn->grabarLogx("Fin de ejecucion de API Rest obtenerSolicitudAfiliacion");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI,true);

				if ($resultAPI['estatus'] == 1)
				{
					foreach($resultAPI['registros'] as $reg)
					{
						$arrDatos[]=array_map('utf8_encode', $reg);
					}
				}
				else
				{
					$objGn->grabarLogx( 'Error en la consulta [obtenerSolicitudAfiliacion] : '.$resultAPI['descripcion']);
				}
			}else{
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

			}
		}catch (Exception $e){
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);

		}

		return $arrDatos;
	}

	//OPCION 3
	public static function obtenerDatosSolicitudAfiliacionGeneral($foliosol)
	{
		$cSql 		= "";
		$arrDatos 	= array();
		$objGn 		= new CMetodoGeneral();
		$objAPI 	= new Capirestcapturaafiliacion();
		$arrAPI 	= array('foliosol' => $foliosol);

		try
		{
			$objGn->grabarLogx("Inicio de ejecucion de API Rest obtenerDatosSolicitudAfiliacionGeneral");

			$resultAPI = $objAPI->consumirApi('obtenerDatosSolicitudAfiliacionGeneral',$arrAPI);

			$objGn->grabarLogx("Respuesta Api: ".$resultAPI);

			$objGn->grabarLogx("Fin de ejecucion de API Rest obtenerDatosSolicitudAfiliacionGeneral");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI,true);

				if($resultAPI['estatus'] == 1)
				{
					foreach($resultAPI['registros'] as $reg)
					{
						$arrDatos[]=array_map('utf8_encode', $reg);
					}
				}
				else
				{
					$objGn->grabarLogx( 'Error en la consulta [obtenerDatosSolicitudAfiliacionGeneral] : '.$resultAPI['descripcion']);
				}
			}
			else
			{
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
			}
		}catch (Exception $e){
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}

		return $arrDatos;
	}

	//OPCION 4
	public static function obtenerDomicilio($foliosol,$tipodom)
	{
		$arrDatos 	= array();
		$objGn 		= new CMetodoGeneral();
		$objAPI 	= new Capirestcapturaafiliacion();
		$arrAPI 	= array('foliosol' => $foliosol,
							'tipodom' => $tipodom
						);

		try
		{
			$objGn->grabarLogx("Inicio de ejecucion de API Rest obtenerDomicilio");

			$resultAPI = $objAPI->consumirApi('obtenerDomicilio',$arrAPI);

			$objGn->grabarLogx("Respuesta Api: ".$resultAPI);

			$objGn->grabarLogx("Fin de ejecucion de API Rest obtenerDomicilio");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI,true);

				if($resultAPI['estatus'] == 1)
				{
					foreach($resultAPI['registros'] as $reg)
					{
						$arrDatos[]=array_map('trim', $reg);
					}

				}else{
					$objGn->grabarLogx( 'Error en la consulta [obtenerDomicilio] : '.$resultAPI['descripcion']);
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
				}
			}else{
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
			}
		}catch (Exception $e){
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}

	//OPCION 5
	public static function obtenerComprobanteDomicilio($compdom)
	{
		$cSql 		= "";
		$arrDatos 	= array('comprobante'=>'');
		$objGn 		= new CMetodoGeneral();
		$objAPI 	= new Capirestcapturaafiliacion();
		$arrAPI 	= array('compdom' => $compdom);

		try
		{
			$objGn->grabarLogx("Inicio de ejecucion de API Rest obtenerComprobanteDomicilio");

			$resultAPI = $objAPI->consumirApi('obtenerComprobanteDomicilio',$arrAPI);

			$objGn->grabarLogx("Respuesta Api: ".$resultAPI);

			$objGn->grabarLogx("Fin de ejecucion de API Rest obtenerComprobanteDomicilio");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI,true);

				if($resultAPI['estatus'] == 1)
				{
					foreach($resultAPI['registros'] as $reg)
					{
						$arrDatos["comprobante"]=$reg["comprobante"];
					}

				}else{
					$objGn->grabarLogx( 'Error en la consulta [obtenerComprobanteDomicilio] : '.$resultAPI['descripcion']);
				}
			}else{
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
			}
		}catch (Exception $e){
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}

		return $arrDatos;
	}

	//OPCION 6
	public static function obtenerDatosSolicitudAfiliacionTelefonos($foliosol)
	{
		$cSql 		= "";
		$arrDatos 	= array();
		$objGn 		= new CMetodoGeneral();
		$objAPI 	= new Capirestcapturaafiliacion();
		$arrAPI 	= array('foliosol' => $foliosol);

		try
		{
			$objGn->grabarLogx("Inicio de ejecucion de API Rest obtenerDatosSolicitudAfiliacionTelefonos");

			$resultAPI = $objAPI->consumirApi('obtenerDatosSolicitudAfiliacionTelefonos',$arrAPI);

			$objGn->grabarLogx("Respuesta Api: ".$resultAPI);

			$objGn->grabarLogx("Fin de ejecucion de API Rest obtenerDatosSolicitudAfiliacionTelefonos");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI,true);

				if($resultAPI['estatus'] == 1)
				{
					foreach($resultAPI['registros'] as $reg)
					{
						$arrDatos[]=array_map('trim', $reg);
					}

				}else{
					$objGn->grabarLogx( 'Error en la consulta [obtenerDatosSolicitudAfiliacionTelefonos] : '.$resultAPI['descripcion']);
				}
			}else{
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
			}
		}catch (Exception $e){
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine();
			$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}

		return $arrDatos;
	}

	//OPCION 7
	public static function obtenerOcupacionProf($ocupacionprof)
	{
		$cSql 		= "";
		$arrDatos 	= array("ocupacion"=>'');
		$objGn 		= new CMetodoGeneral();
		$objAPI 	= new Capirestcapturaafiliacion();
		$arrAPI 	= array('ocupacionprof' => $ocupacionprof);

		try
		{
			$objGn->grabarLogx("Inicio de ejecucion de API Rest obtenerOcupacionProf");

			$resultAPI = $objAPI->consumirApi('obtenerOcupacionProf',$arrAPI);

			$objGn->grabarLogx("Respuesta Api: ".$resultAPI);

			$objGn->grabarLogx("Fin de ejecucion de API Rest obtenerOcupacionProf");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI, true);

				if($resultAPI['estatus'] == 1)
				{
					foreach($resultAPI['registros'] as $reg)
					{
						$arrDatos["ocupacion"] = $reg["ocupacion"];
					}

				}else{
					$objGn->grabarLogx( 'Error en la consulta [obtenerOcupacionProf] : '.$resultAPI['descripcion']);
				}
			}else{
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
			}
		}catch (Exception $e){
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}

	//OPCION 8
	public static function obtenerActividadGiro($activinegocio)
	{
		$cSql 		= "";
		$arrDatos 	= array("actividad"=>'');
		$objGn 		= new CMetodoGeneral();
		$objAPI 	= new Capirestcapturaafiliacion();
		$arrAPI 	= array('activinegocio' => $activinegocio);

		try
		{
			$objGn->grabarLogx("Inicio de ejecucion de API Rest obtenerActividadGiro");

			$resultAPI = $objAPI->consumirApi('obtenerActividadGiro',$arrAPI);

			$objGn->grabarLogx("Respuesta Api: ".$resultAPI);

			$objGn->grabarLogx("Fin de ejecucion de API Rest obtenerActividadGiro");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI, true);
				if($resultAPI['estatus'] == 1)
				{
					foreach($resultAPI['registros'] as $reg)
					{
						$arrDatos["actividad"] = $reg["actividad"];
					}

				}else{
					$objGn->grabarLogx( 'Error en la consulta [obtenerOcupacionProf] : '.$resultAPI['descripcion']);
				}
			}else{
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
			}
		}catch (Exception $e){
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}

		return $arrDatos;
	}

	//OPCION 9
	public static function obtenerDatosSolicitudAfiliacionBeneficiarios($foliosol)
	{
		$arrDatos 	= array();
		$objGn 		= new CMetodoGeneral();
		$objAPI 	= new Capirestcapturaafiliacion();
		$arrAPI 	= array('foliosol' => $foliosol);

		try
		{
			$objGn->grabarLogx("Inicio de ejecucion de API Rest obtenerDatosSolicitudAfiliacionBeneficiarios");

			$resultAPI = $objAPI->consumirApi('obtenerDatosSolicitudAfiliacionBeneficiarios',$arrAPI);

			$objGn->grabarLogx("Respuesta Api: ".$resultAPI);

			$objGn->grabarLogx("Fin de ejecucion de API Rest obtenerDatosSolicitudAfiliacionBeneficiarios");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI, true);

				if($resultAPI['estatus'] == 1)
				{
					foreach($resultAPI['registros'] as $reg)
					{
						$arrDatos[]=array_map('trim', $reg);
					}

				}else{
					$objGn->grabarLogx( 'Error en la consulta [obtenerDatosSolicitudAfiliacionBeneficiarios] : '.$resultAPI['descripcion']);
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
				}
			}else{
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
			}
		}catch (Exception $e){
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}

		return $arrDatos;
	}

	//OPCION 10
	public static function obtenerDiagnosticoSolicitud($foliosol)
	{
		$iStatusProceso = 0;
		$iUsarBDInformix = 0;
		$sDiagnostico = '';
		$arrDatos 	= array("diagnosticosolicitud"=>'');
		$objGn 		= new CMetodoGeneral();
		$objAPI 	= new Capirestcapturaafiliacion();
		$arrAPI 	= array('foliosol' => $foliosol);

		try
		{
			$objGn->grabarLogx("Inicio de ejecucion de API Rest obtenerDiagnosticoSolicitud");

			$resultAPI = $objAPI->consumirApi('obtenerDiagnosticoSolicitud',$arrAPI);

			$objGn->grabarLogx("Respuesta Api: ".$resultAPI);

			$objGn->grabarLogx("Fin de ejecucion de API Rest obtenerDiagnosticoSolicitud");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI, true);

				if($resultAPI['estatus']== 1)
				{
					foreach($resultAPI['registros'] as $reg)
					{
						//$arrDatos[]=array_map('utf8_encode', $reg);
						$arrDatos["diagnosticosolicitud"] = $reg["diagnosticosolicitud"];
						$sDiagnostico = $arrDatos["diagnosticosolicitud"];

						$objGn->grabarLogx("[obtenerDiagnosticoSolicitud]sDiagnostico -->".$sDiagnostico);

						$iStatusProceso = $sDiagnostico;
						//$iStatusProceso = 851;
						if(is_numeric($iStatusProceso))
						{
							$arrDatos["diagnosticosolicitud"] = '';
							$iUsarBDInformix = self::usarConexionInformix();
							if( $iUsarBDInformix  == 1 )
							{
								//validar si es numerico.
								switch ($iStatusProceso)
								{
									//consulta por causa de PROCESAR
									case 851:
									//case 852:
									case 853:
									case 854:
									case 903:
									case 904:
									case 905:
									case 906:
										$arrDatos["diagnosticosolicitud"] = self::obtenerDiagnosticoSolicitudSafre_af($foliosol);
										break;
									default:
										$arrDatos["diagnosticosolicitud"] = '';
										break;
								}
							}
						}
					}

				}else{
					$objGn->grabarLogx( 'Error en la consulta [obtenerDiagnosticoSolicitud] : '.$resultAPI['descripcion']);
				}
			}else{
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
			}
		}catch (Exception $e){
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}

		return $arrDatos;
	}

	public static function usarConexionInformix()
	{
		$cSql 		= "";
		$iRespuesta	= 1;
		//$arrDatos 	= array();
		$arrDatos 	= array("respuesta"=>0);
		$objGn 		= new CMetodoGeneral();
		$objAPI 	= new Capirestcapturaafiliacion();
		$arrAPI 	= array();

		try
		{
			$objGn->grabarLogx("Inicio de ejecucion de API Rest usarConexionInformix");

			$resultAPI = $objAPI->consumirApi('usarConexionInformix',$arrAPI);

			$objGn->grabarLogx("Respuesta Api: ".$resultAPI);

			$objGn->grabarLogx("Fin de ejecucion de API Rest usarConexionInformix");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI, true);

				if ($resultAPI['estatus'] == 1)
				{
					foreach($resultAPI['registros'] as $reg)
					{
						$arrDatos["respuesta"] = (int)$reg["respuesta"];
						$iRespuesta = $arrDatos["respuesta"];
						$objGn->grabarLogx("[usarConexionInformix] iRespuestas-->".$iRespuesta);
					}

				}else{
					$objGn->grabarLogx( 'Error en la consulta [usarConexionInformix] : '.$resultAPI['descripcion']);
				}
			}else{
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
			}
		}catch (Exception $e){
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}

		return $iRespuesta;
	}

	public static function obtenerDiagnosticoSolicitudSafre_af($foliosol)
	{
		$cnxOdbc = null;
		//---------------------------------------------------INFORMIX--------------------------------------------------------------
		$cnxOdbc =  new PDO( "informix:host=".IPSERVIDORINF."; service=1521; database=".BASEDEDATOSINF."; server=safre_tcp; protocol=onsoctcp; EnableScrollableCursors=1", USUARIOINF, PASSWORDINF);
		//--------------------------------------------------Consultar Datos---------------------------------------------------------
		$cSql = null;
		$arrDatos 	= array();
		$sDiagnosticoInformix = '';
		$objGn 		= new CMetodoGeneral();

		if($cnxOdbc)
		{
			$cSql = "EXECUTE FUNCTION fn_Obtener_Diagnostico_Rechazo_Sol(".$foliosol.");";
			$objGn->grabarLogxConsulSolAfi("[obtenerDiagnosticoSolicitud] -->".$cSql);
			$resultSet = $cnxOdbc->query($cSql);
			if($resultSet)
			{
				foreach ($resultSet as $reg)
				{
					$arrDatos=array_map('trim', $reg);
					$sDiagnosticoInformix = $arrDatos[0];
					$objGn->grabarLogxConsulSolAfi("[obtenerDiagnosticoSolicitud] sDiagnosticoInformix-->".$sDiagnosticoInformix);
				}
			}
			else
			{
				//Enviamos el error al log
				$arrErr = $cnxBd->errorInfo();
				$objGn->grabarLogxConsulSolAfi( 'Error en la consulta [obtenerDiagnosticoSolicitud]: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
		}
		else
		{
			 //Enviamos el error al log
			 $arrErr = $cnxBd->errorInfo();
			 $objGn->grabarLogxConsulSolAfi( 'Error en la conexion: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
		}
		return $sDiagnosticoInformix;
	}

}
?>