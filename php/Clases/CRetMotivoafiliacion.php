<?php
include_once('global.php');
include_once('Clases/CMetodoGeneral.php');
include_once("JSON.php");

$json = new Services_JSON();

class CMotivoafiliacion
{
	//OPCION 2
	public static function validarFolioSolicitud($foliosol)
	{

		$arrDatos 	= array(
			"folio"                     => 0,
			"tiposolicitud"             => 0,
			"curp"                      => '',
			"esimss"                    => 0,
			"tipotraspaso"              => 0,
			"folioedocta"               => '',
			"aforeedocta"               => '',
			"cuatrimestreedocta"        => '',
			"motivo"                    => '',
			"folioimplicaciontraspaso"  => '',
			"tipodeadmon"               => 0,
			"depositoprevio"            => '',
			"folioconstanciaregtras"    => ''
		);
		$objGn 		= new CMetodoGeneral();
		$objAPI 	= new Capirestcapturaafiliacion();

		$arrAPI = array('foliosol' => $foliosol);

		try {
			$objGn->grabarLogx("Inicio de ejecucion de API Rest");

			$resultAPI = $objAPI->consumirApi('validarFolioSolicitud', $arrAPI);

			$objGn->grabarLogx("Respuesta Api: " . $resultAPI);

			$objGn->grabarLogx("Fin de ejecucion de API Rest");

			if ($resultAPI) {
				$resultAPI = json_decode($resultAPI, true);

				if ($resultAPI['estatus'] == 1) {
					foreach ($resultAPI['registros'] as $reg) {
						$arrDatos["folio"]                      = $reg["folio"];
						$arrDatos["tiposolicitud"]              = $reg["tiposolicitud"];
						$arrDatos["curp"]                       = TRIM($reg["curp"]);
						$arrDatos["esimss"]                     = $reg["esimss"];
						$arrDatos["tipotraspaso"]               = $reg["tipotraspaso"];
						$arrDatos["folioedocta"]                = TRIM($reg["folioedocta"]);
						$arrDatos["aforeedocta"]                = TRIM($reg["aforeedocta"]);
						$arrDatos["cuatrimestreedocta"]         = TRIM($reg["cuatrimestreedocta"]);
						$arrDatos["motivo"]                     = TRIM($reg["motivo"]);
						$arrDatos["folioimplicaciontraspaso"]   = TRIM($reg["folioimplicaciontraspaso"]);
						$arrDatos["tipodeadmon"]                = $reg["tipodeadmon"];
						$arrDatos["depositoprevio"]             = TRIM($reg["depositoprevio"]);
						$arrDatos["folioprocesar"]     = TRIM($reg["folioprocesar"]);
					}
				} else {
					$objGn->grabarLogxMot('Error en la consulta [colcatmotivotraspaso] : ' . $resultAPI['descripcion']);
				}
			} else {
				// Si existe un error en la consulta mostrar el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
			}
		} catch (Exception $e) {
			$objGn->grabarLogx("Fin de ejecucin de API Rest");
			$mensaje = 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine();
			$objGn->grabarLogxMot('[' . __FILE__ . ']' . $mensaje);
		}

		return $arrDatos;
	}

	//OPCION 3
	public static function llenarcbxMotivoRegistro()
	{

		$arrDatos 	= array();
		$objGn 		= new CMetodoGeneral();
		$objAPI 	= new Capirestcapturaafiliacion();
		$arrAPI 	= array();

		try {
			$objGn->grabarLogx("Inicio de ejecucin de API Rest");

			$resultAPI = $objAPI->consumirApi('llenarcbxMotivoRegistro', $arrAPI);

			$objGn->grabarLogx("Fin de ejecucin de API Rest");

			if ($resultAPI) {
				$resultAPI = json_decode($resultAPI, true);

				if ($resultAPI['estatus'] == 1) {
					foreach ($resultAPI['registros'] as $reg) {
						$arrDatos[] = array_map('trim', $reg);
					}
				} else {
					$objGn->grabarLogxMot('Error en la consulta [colcatmotivotraspaso] : ' . $resultAPI['descripcion']);
				}
			} else {
				// Si existe un error en la consulta mostrar el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
			}
		} catch (Exception $e) {
			$objGn->grabarLogx("Fin de ejecucin de API Rest");
			$mensaje = 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine();
			$objGn->grabarLogxMot('[' . __FILE__ . ']' . $mensaje);
		}

		return $arrDatos;
	}

	//OPCION 4
	public static function llenarcbxTipoAdmin()
	{

		$arrDatos 	= array();
		$objGn 		= new CMetodoGeneral();
		$objAPI 	= new Capirestcapturaafiliacion();
		$arrAPI 	= array();

		try {
			$objGn->grabarLogx("Inicio de ejecucin de API Rest");

			$resultAPI = $objAPI->consumirApi('llenarcbxTipoAdmin', $arrAPI);

			$objGn->grabarLogx("Fin de ejecucin de API Rest");

			if ($resultAPI) {
				$resultAPI = json_decode($resultAPI, true);

				if ($resultAPI['estatus'] == 1) {
					foreach ($resultAPI['registros'] as $reg) {
						$arrDatos[] = array_map('trim', $reg);
					}
				} else {
					$objGn->grabarLogxMot('Error en la consulta [colcatmotivotraspaso] : ' . $resultAPI['descripcion']);
				}
			} else {
				// Si existe un error en la consulta mostrar el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
			}
		} catch (Exception $e) {
			$mensaje = 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine();
			$objGn->grabarLogxMot('[' . __FILE__ . ']' . $mensaje);
		}

		return $arrDatos;
	}

	//OPCION 5
	public static function guardarDatosMotivoAfiliacion($opc, $foliosol, $tipoafi, $esimss, $tipotraspaso, $motregi, $folconstaimple, $iTipoAdmin, $depo)
	{

		$arrDatos 	= array("respuesta" => 0);
		$objGn 		= new CMetodoGeneral();
		$objAPI 	= new Capirestcapturaafiliacion();
		$arrAPI 	= array(
			'opc'			=> $opc,
			'foliosol' 		=> $foliosol,
			'tipoafi' 		=> $tipoafi,
			'esimss' 		=> $esimss,
			'tipotraspaso' 	=> $tipotraspaso,
			'motregi' 		=> $motregi,
			'folconstaimple' => $folconstaimple,
			'iTipoAdmin' 	=> $iTipoAdmin,
			'depo' 			=> $depo
		);

		try {
			$objGn->grabarLogx("Inicio de ejecucin de API Rest guardarDatosMotivoAfiliacion");

			$resultAPI = $objAPI->consumirApi('guardarDatosMotivoAfiliacion', $arrAPI);

			$objGn->grabarLogx("Respuesta Api: " . $resultAPI);

			if ($resultAPI) {
				$resultAPI = json_decode($resultAPI, true);

				if ($resultAPI['estatus'] == 1) {
					foreach ($resultAPI['registros'] as $reg) {
						$arrDatos["respuesta"] = (int)$reg["respuesta"];
					}

					$objGn->grabarLogxMot("[respuesta] -->" . $arrDatos["respuesta"]);
				} else {
					$objGn->grabarLogxMot('Error en la consulta [colcatmotivotraspaso] : ' . $resultAPI['descripcion']);
				}
			} else {
				// Si existe un error en la consulta mostrar el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
			}
		} catch (Exception $e) {
			$mensaje = 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine();
			$objGn->grabarLogxMot('[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}

	//OPCION 6
	public static function guardarSolicitudBD($foliosol, $iEmpleado, $sIpRemoto, $fecha)
	{


		$arrDatos 	= array("respuesta" => 0);
		$objGn 		= new CMetodoGeneral();
		$objAPI 	= new Capirestcapturaafiliacion();
		$arrAPI		= array(
			'foliosol'	=> $foliosol,
			'iEmpleado'	=> $iEmpleado,
			'sIpRemoto'	=> $sIpRemoto,
			'fecha'		=> $fecha
		);
		try {
			$objGn->grabarLogx("Inicio de ejecucin de API Rest guardarSolicitudBD");
			
			$resultAPI = $objAPI->consumirApi('guardarSolicitudBD', $arrAPI);

			$objGn->grabarLogx("Fin de ejecucin de API Rest guardarSolicitudBD");

			if ($resultAPI) {
				$resultAPI = json_decode($resultAPI, true);

				if ($resultAPI['estatus'] == 1) {
					foreach ($resultAPI['registros'] as $reg) {
						$arrDatos["respuesta"] = (int)$reg["respuesta"];
					}

					$objGn->grabarLogxMot("[respuesta] -->" . $arrDatos["respuesta"]);
				} else {
					$objGn->grabarLogxMot('Error en la consulta [guardarSolicitudBD] : ' . $resultAPI['descripcion']);
				}
			} else {
				// Si existe un error en la consulta mostrar el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
			}
		} catch (Exception $e) {
			$mensaje = 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine();
			$objGn->grabarLogxMot('[' . __FILE__ . ']' . $mensaje);
		}

		return $arrDatos;
	}

	//OPCION 7
	public static function fnexistenciafolioprocesar($foliosol, $foliosolpro)
	{
		$cSql 		= "";
		$arrDatos 	= array("irespuesta" => 0);
		$objGn 		= new CMetodoGeneral();
		$objAPI 	= new Capirestcapturaafiliacion();

		$foliosolpro = utf8_decode($foliosolpro);

		$arrAPI		= array(
			'foliosol'	=> $foliosol,
			'foliosolpro'	=> $foliosolpro
		);

		try {
			$objGn->grabarLogx("Inicio de ejecucin de API Rest fnexistenciafolioprocesar");

			$resultAPI = $objAPI->consumirApi('fnexistenciafolioprocesar', $arrAPI);

			$objGn->grabarLogx("Fin de ejecucin de API Rest fnexistenciafolioprocesar");

			if ($resultAPI) {
				$resultAPI = json_decode($resultAPI, true);

				if ($resultAPI['estatus'] == 1) {
					foreach ($resultAPI['registros'] as $reg) {
						$arrDatos["irespuesta"] = $reg["irespuesta"];
					}
					$objGn->grabarLogxMot("[irespuesta] -->" . $arrDatos["irespuesta"]);
				} else {
					$objGn->grabarLogxMot('Error en la consulta [fnexistenciafolioprocesar] : ' . $resultAPI['descripcion']);
				}
			} else {
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
			}
		} catch (Exception $e) {
			$mensaje = 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			$objGn->grabarLogxMot('[' . __FILE__ . ']' . $mensaje);
		}

		return $arrDatos;
	}
	//OPCION 8
	public static function existenciafechaProcesar($foliosol)
	{
		$cSql 		= "";
		$arrDatos 	= array("irespuesta" => 0);
		$objGn 		= new CMetodoGeneral();
		$objAPI 	= new Capirestcapturaafiliacion();
		$arrAPI		= array('foliosol'	=> $foliosol);

		try {
			$objGn->grabarLogx("Inicio de ejecucin de API Rest existenciafechaProcesar");

			$resultAPI = $objAPI->consumirApi('existenciafechaProcesar', $arrAPI);

			$objGn->grabarLogx("Fin de ejecucin de API Rest existenciafechaProcesar");

			if ($resultAPI) {
				$resultAPI = json_decode($resultAPI, true);
				if ($resultAPI['estatus'] == 1) {
					foreach ($resultAPI['registros'] as $reg) {
						$arrDatos["irespuesta"] = $reg["irespuesta"];
					}
					$objGn->grabarLogxMot("[irespuesta] -->" . $arrDatos["irespuesta"]);
				} else {
					$objGn->grabarLogxMot('Error en la consulta [existenciafechaProcesar] : ' . $resultAPI['descripcion']);
				}
			} else {
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
			}
		} catch (Exception $e) {
			$mensaje = 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			$objGn->grabarLogxMot('[' . __FILE__ . ']' . $mensaje);
		}

		return $arrDatos;
	}

	//INE//
	//OPCION 10
	public static function validacionine($foliosol)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos 	= array("respuesta" => 0);

		$arrApi = array(
			'foliosol' => $foliosol,
		);

		try {
			$objGn->grabarLogx("Inicio API Rest validacionine");

			$resultAPI = $objAPI->consumirApi('validacionine', $arrApi);

			$objGn->grabarLogx("Respuesta de Api Rest: " . $resultAPI);

			$objGn->grabarLogx("Fin API Rest validacionine");

			if ($resultAPI) {
				$resultAPI = json_decode($resultAPI, true);

				if ($resultAPI['estatus'] == 1) {

					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = "EXITO";

					foreach ($resultAPI["registros"] as $reg) {
						$arrDatos["respuesta"] = $reg["respuesta"];
					}
				} else {
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx('[' . __FILE__ . ']' . $resultAPI["descripcion"]);
				}
			} else {
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("CRetMotivoafiliacion.php\tvalidacionine" . "\tError al ejecutar la consulta \t" . " | " . pg_errormessage());
			}
		} catch (Exception $e) {
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
			$mensaje = 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			$objGn->grabarLogx('[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}

	//OPCION 11
	public static function fnvalidavigenciasemilla($foliosol, $foliosemilla)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos 	= array("irespuesta" => 0);

		$arrApi = array(
			'foliosol' => $foliosol,
			'foliosemilla' => $foliosemilla
		);

		try {
			$objGn->grabarLogx("Inicio API Rest fnvalidavigenciasemilla");
			
			$resultAPI = $objAPI->consumirApi('fnvalidavigenciasemilla', $arrApi);

			$objGn->grabarLogx("Respuesta de Api Rest: " . $resultAPI);

			$objGn->grabarLogx("Fin API Rest fnvalidavigenciasemilla");

			if ($resultAPI) {
				$resultAPI = json_decode($resultAPI, true);

				if ($resultAPI['estatus'] == 1) {

					//indicador que asigna estatus 1, osea correctamente y su descripcion
					// $arrDatos['estatus'] = 1;
					// $arrDatos['descripcion'] = "EXITO";

					foreach ($resultAPI["registros"] as $reg) {
						$arrDatos["irespuesta"] = $reg["irespuesta"];
						if ($arrDatos["irespuesta"] == 1) {
							$arrDatos['estatus'] = 1;
							$arrDatos['descripcion'] = "EXITO";
						}
					}
				} else {
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx('[' . __FILE__ . ']' . $resultAPI["descripcion"]);
				}
			} else {
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("CRetMotivoafiliacion.php\tfnvalidavigenciasemilla" . "\tError al ejecutar la consulta \t" . " | " . pg_errormessage());
			}
		} catch (Exception $e) {
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
			$mensaje = 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			$objGn->grabarLogx('[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}

	//OPCION 12 para obtener la curp de la tabla solconstancia mediante el folio interno
	public static function fnobtenercurpconstancia($foliosol)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos 	= array("curp" => '');

		$arrApi = array(
			'foliosol' => $foliosol
		);

		try {
			$objGn->grabarLogx("Inicio API Rest fnobtenercurpconstancia");

			$resultAPI = $objAPI->consumirApi('fnobtenercurpconstancia', $arrApi);

			$objGn->grabarLogx("Respuesta de Api Rest: " . $resultAPI);

			$objGn->grabarLogx("Fin API Rest fnobtenercurpconstancia");

			if ($resultAPI) {
				$resultAPI = json_decode($resultAPI, true);

				if ($resultAPI['estatus'] == 1) {

					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = "EXITO";

					foreach ($resultAPI["registros"] as $reg) {
						$arrDatos["curp"] = $reg["curp"];
					}
				} else {
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx('[' . __FILE__ . ']' . $resultAPI["descripcion"]);
				}
			} else {
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("CRetMotivoafiliacion.php\tfnobtenercurpconstancia" . "\tError al ejecutar la consulta \t" . " | " . pg_errormessage());
			}
		} catch (Exception $e) {
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
			$mensaje = 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			$objGn->grabarLogx('[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}

	// Opcion 14: Funcion para generar el codigoAutenticacion
	public static function generarFolioIne($sCurp, $foliosol)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos 	= array();

		$arrApi = array(
			'sCurp' => $sCurp,
			'foliosol' => $foliosol
		);

		try {
			$objGn->grabarLogx("Inicio API Rest generarFolioIne");

			$resultAPI = $objAPI->consumirApi('generarFolioIne', $arrApi);

			$objGn->grabarLogx("Respuesta de Api Rest: " . $resultAPI);

			$objGn->grabarLogx("Fin API Rest generarFolioIne");

			if ($resultAPI) {
				$resultAPI = json_decode($resultAPI, true);

				if ($resultAPI['estatus'] == 1) {

					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = "EXITO";

					foreach ($resultAPI["registros"] as $reg) {
						$arrDatos["codigoautenticacion"] = $reg["codigoautenticacion"];
					}
					$arrDatos["respuesta"] = 1;
				} else {
					$arrDatos["respuesta"] = 0;
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx('[' . __FILE__ . ']' . $resultAPI["descripcion"]);
				}
			} else {
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("CRetMotivoafiliacion.php\tgenerarFolioIne" . "\tError al ejecutar la consulta \t" . " | " . pg_errormessage());
			}
		} catch (Exception $e) {
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
			$mensaje = 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			$objGn->grabarLogx('[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}

	//Invocacion a phyton para realizar el envio de mensaje
	public static function python($folioine, $opc)
	{
		//	CMetodoGeneral::setLogName(CGeneral::$cNombreLog); exec( "python /sysx/progs/web/afore_base4_gil/php/clases/enviosmsine.py ".$foliosol, $output);
		$objGn = new CMetodoGeneral();
		$output = array();
		$objGn->grabarLogx("*************Inicia proceso python***************");
		exec("/bin/python /sysx/progs/afore/enviosmsine/enviosmsine.py " . $folioine . " " . $opc, $output);
		$objGn->grabarLogx("*************Termina proceso python***************");
		return $output;
	}
	//INE//OPCION 17
	public static function fnvalidastatusenvioine($foliosol)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos 	= array("irespuesta" => 0);

		$arrApi = array(
			'foliosol' => $foliosol
		);

		try {
			$objGn->grabarLogx("Inicio API Rest fnvalidastatusenvioine");

			$resultAPI = $objAPI->consumirApi('fnvalidastatusenvioine', $arrApi);

			$objGn->grabarLogx("Respuesta de Api Rest: " . $resultAPI);

			$objGn->grabarLogx("Fin API Rest fnvalidastatusenvioine");

			if ($resultAPI) {
				$resultAPI = json_decode($resultAPI, true);

				if ($resultAPI['estatus'] == 1) {

					//indicador que asigna estatus 1, osea correctamente y su descripcion
					// $arrDatos['estatus'] = 1;
					// $arrDatos['descripcion'] = "EXITO";

					foreach ($resultAPI["registros"] as $reg) {
						$arrDatos["irespuesta"] = (int)$reg["irespuesta"];
						if ($arrDatos["irespuesta"] == 1) {
							$arrDatos['estatus'] = 1;
							$arrDatos['descripcion'] = "EXITO";
						}
					}
				} else {
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx('[' . __FILE__ . ']' . $resultAPI["descripcion"]);
				}
			} else {
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("CRetMotivoafiliacion.php\tfnvalidastatusenvioine" . "\tError al ejecutar la consulta \t" . " | " . pg_errormessage());
			}
		} catch (Exception $e) {
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
			$mensaje = 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			$objGn->grabarLogx('[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}
	//INW//OPCION 15
	public static function fnejecutapy()
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos 	= array();
		$arrApi = array();

		try {
			$objGn->grabarLogx("Inicio API Rest fnejecutapy");

			$resultAPI = $objAPI->consumirApi('fnejecutapy', $arrApi);

			$objGn->grabarLogx("Respuesta de Api Rest: " . $resultAPI);

			$objGn->grabarLogx("Fin API Rest fnejecutapy");

			if ($resultAPI) {
				$resultAPI = json_decode($resultAPI, true);

				if ($resultAPI['estatus'] == 1) {

					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = "EXITO";

					foreach ($resultAPI["registros"] as $reg) {
						$arrDatos = $reg["fnejecutapy"];
					}
				} else {
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx('[' . __FILE__ . ']' . $resultAPI["descripcion"]);
				}
			} else {
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("CRetMotivoafiliacion.php\tfnejecutapy" . "\tError al ejecutar la consulta \t" . " | " . pg_errormessage());
			}
		} catch (Exception $e) {
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
			$mensaje = 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			$objGn->grabarLogx('[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}
	//INE//OPCION 18
	public static function fnobtenerfolioine($foliosol)
	{
		$cSql 		= "";
		$arrDatos 	= array("ifolioine" => '');
		$objGn 		= new CMetodoGeneral();
		try {
			$cnxBd =  new PDO("pgsql:host=" . IPSERVIDOR . ";port=5432;dbname=" . BASEDEDATOS, USUARIO, PASSWORD);
			if ($cnxBd) {
				$cSql = "SELECT TRIM(fnobtenerfolioine) AS ifolioine FROM fnobtenerfolioine($foliosol);";
				$objGn->grabarLogxMot("[fnobtenerfolioine] -->" . $cSql);
				$resulSet = $cnxBd->query($cSql);
				if ($resulSet) {
					foreach ($resulSet as $reg) {
						$arrDatos["ifolioine"] = $reg["ifolioine"];
					}
					$objGn->grabarLogxMot("[ifolioine: ] -->" . $arrDatos["ifolioine"]);
				} else {
					$arrErr = $cnxBd->errorInfo();
					$objGn->grabarLogxMot('Error en la consulta [fnobtenerfolioine]: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
				}
			} else {
				$arrErr = $cnxBd->errorInfo();
				$objGn->grabarLogxMot('Error en la conexion: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
		} catch (Exception $e) {
			$mensaje = 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode() . 'SQL->' . $cSql;
			$objGn->grabarLogxMot('[' . __FILE__ . ']' . $mensaje);
		}
		$cnxBd = null;
		return $arrDatos;
	}
	//INE//OPCION 19
	public static function fnobtenerparaivr($sCurp, $folioine)
	{
		$cSql 		= "";
		$objGn 		= new CMetodoGeneral();
		try {
			$cnxBd =  new PDO("pgsql:host=" . IPSERVIDOR . ";port=5432;dbname=" . BASEDEDATOS, USUARIO, PASSWORD);
			if ($cnxBd) {
				$cSql = "SELECT telefono,codigoautenticacion FROM fnobneterdatosivr('$sCurp',$folioine);";
				$objGn->grabarLogxMot("[fnobneterdatosivr] -->" . $cSql);
				$resulSet = $cnxBd->query($cSql);
				if ($resulSet) {
					foreach ($resulSet as $reg) {
						$arrDatos['telefono'] = $reg["telefono"];
						$arrDatos['codigoautenticacion'] = $reg["codigoautenticacion"];
					}
					$arrDatos['respuesta'] = 1;
					$objGn->grabarLogxMot("[fnobneterdatosivr] -->" . $arrDatos['respuesta']);
				} else {
					$arrErr = $cnxBd->errorInfo();
					$objGn->grabarLogxMot('Error en la consulta [fnobneterdatosivr]: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
				}
			} else {
				$arrErr = $cnxBd->errorInfo();
				$objGn->grabarLogxMot('Error en la conexion: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
		} catch (Exception $e) {
			$mensaje = 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode() . 'SQL->' . $cSql;
			$objGn->grabarLogxMot('[' . __FILE__ . ']' . $mensaje);
		}
		$cnxBd = null;
		return $arrDatos;
	}
	//INE//OPCION 20
	public static function fnactualizaropcioncombo($sCurp, $opcioncombo)
	{
		$cSql 		= "";
		$objGn 		= new CMetodoGeneral();
		$arrDatos 	= array("irespuesta" => 0);
		try {
			$cnxBd =  new PDO("pgsql:host=" . IPSERVIDOR . ";port=5432;dbname=" . BASEDEDATOS, USUARIO, PASSWORD);
			if ($cnxBd) {
				$cSql = "SELECT fnactualizaropcionenvio  AS irespuesta FROM fnactualizaropcionenvio('$sCurp',$opcioncombo);";
				$objGn->grabarLogxMot("[fnactualizaropcionenvio] -->" . $cSql);
				$resulSet = $cnxBd->query($cSql);
				if ($resulSet) {
					foreach ($resulSet as $reg) {
						$arrDatos['irespuesta'] = $reg["irespuesta"];
					}
					$arrDatos['irespuesta'] = 1;
					$objGn->grabarLogxMot("[fnactualizaropcionenvio] -->" . $arrDatos['irespuesta']);
				} else {
					$arrErr = $cnxBd->errorInfo();
					$objGn->grabarLogxMot('Error en la consulta [fnactualizaropcionenvio]: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
				}
			} else {
				$arrErr = $cnxBd->errorInfo();
				$objGn->grabarLogxMot('Error en la conexion: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
		} catch (Exception $e) {
			$mensaje = 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode() . 'SQL->' . $cSql;
			$objGn->grabarLogxMot('[' . __FILE__ . ']' . $mensaje);
		}
		$cnxBd = null;
		return $arrDatos;
	}
	//OPCION 21
	public static function fnobtenerLigaIVR($iopcionLigaine)
	{
		$cSql 		= "";
		$objGn 		= new CMetodoGeneral();
		$arrDatos 	= array();
		try {
			$cnxBd =  new PDO("pgsql:host=" . IPSERVIDOR . ";port=5432;dbname=" . BASEDEDATOS, USUARIO, PASSWORD);
			if ($cnxBd) {
				$cSql = "SELECT TRIM(liga) AS liga FROM fnobtenerligasservicioine($iopcionLigaine);";
				//var_dump($cSql);
				$objGn->grabarLogxMot("[fnobtenerligasservicioine] -->" . $cSql);

				$resulSet = $cnxBd->query($cSql);
				if ($resulSet) {
					foreach ($resulSet as $reg) {
						$arrDatos["liga"] = $reg["liga"];
					}
					$objGn->grabarLogx("[liga: ] -->" . $arrDatos["liga"]);
				} else {
					$arrErr = $cnxBd->errorInfo();
					$objGn->grabarLogxMot('Error en la consulta [fnobtenerligasservicioine]: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
				}
			} else {
				$arrErr = $cnxBd->errorInfo();
				$objGn->grabarLogxMot('Error en la conexion: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
		} catch (Exception $e) {
			$mensaje = 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode() . 'SQL->' . $cSql;
			$objGn->grabarLogxMot('[' . __FILE__ . ']' . $mensaje);
		}
		$cnxBd = null;
		return $arrDatos;
	}
	public static function fnenvioIVR()
	{
		$cSql 		= "";
		$objGn 		= new CMetodoGeneral();
		$arrDatos 	= array();
		try {
			$cnxBd =  new PDO("pgsql:host=" . IPSERVIDOR . ";port=5432;dbname=" . BASEDEDATOS, USUARIO, PASSWORD);
			if ($cnxBd) {
				$cSql = "SELECT fnenviarporivr AS irespuesta FROM fnenviarporivr();";
				$objGn->grabarLogxMot("[fnenviarporivr] -->" . $cSql);

				$resulSet = $cnxBd->query($cSql);
				if ($resulSet) {
					foreach ($resulSet as $reg) {
						$arrDatos["irespuesta"] = $reg["irespuesta"];
					}
					$objGn->grabarLogx("[respuesta para ivr: ] -->" . $arrDatos["irespuesta"]);
				} else {
					$arrErr = $cnxBd->errorInfo();
					$objGn->grabarLogxMot('Error en la consulta [fnenviarporivr]: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
				}
			} else {
				$arrErr = $cnxBd->errorInfo();
				$objGn->grabarLogxMot('Error en la conexion: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
		} catch (Exception $e) {
			$mensaje = 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode() . 'SQL->' . $cSql;
			$objGn->grabarLogxMot('[' . __FILE__ . ']' . $mensaje);
		}
		$cnxBd = null;
		return $arrDatos;
	}

	/**********FOLIO 877.1************** */
	//OPCION 23
	public static function obtenerConfiguracionWs()
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos 	= array("configuracion" => array("intentos" => 0, "tiempo" => 0));

		$arrApi = array();

		try {
			$objGn->grabarLogx("Inicio API Rest configuracionconsultafolioafi");

			$resultAPI = $objAPI->consumirApi('configuracionconsultafolioafi', $arrApi);

			$objGn->grabarLogx("Respuesta de Api Rest: " . $resultAPI);

			$objGn->grabarLogx("Fin API Rest configuracionconsultafolioafi");

			if ($resultAPI) {
				$resultAPI = json_decode($resultAPI, true);

				if ($resultAPI['estatus'] == 1) {

					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['estatus'] 		= 1;
					$arrDatos['descripcion'] 	= "EXITO";

					foreach ($resultAPI["registros"] as $reg) {
						$arrDatos["configuracion"]["intentos"] 	= $reg["iintentos"];
						$arrDatos["configuracion"]["tiempo"] 	= $reg["itiempo"];
					}
				} else {
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx('[' . __FILE__ . ']' . $resultAPI["descripcion"]);
				}
			} else {
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("CRetCapturaafiliacion.php\tvalidacionine" . "\tError al ejecutar la consulta \t" . " | " . pg_errormessage());
			}
		} catch (Exception $e) {
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
			$mensaje = 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			$objGn->grabarLogx('[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}
	//OPCION 24
	public static function agregarDatosConsultaFolio($iEmpleado, $sCurp, $tipoafi, $folioRegTras, $foliosol)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos 	= array("result" => array());

		$arrApi = array(
			'empleado' 				=> $iEmpleado,
			'curp' 					=> $sCurp,
			'claveaforegenerofolio' => 568,
			'tipofolio' 			=> $tipoafi,
			'folioregtrasp' 		=> $folioRegTras,
			'foliosol' 				=> $foliosol
		);

		try {
			$objGn->grabarLogx("Inicio API Rest agregarDatosConsultaFolio");

			$resultAPI = $objAPI->consumirApi('agregardatosconsultafolioafiliacion', $arrApi);

			$objGn->grabarLogx("Respuesta de Api Rest: " . $resultAPI);

			$objGn->grabarLogx("Fin API Rest agregardatosconsultafolioafiliacion");

			if ($resultAPI) {
				$resultAPI = json_decode($resultAPI, true);

				if ($resultAPI['estatus'] == 1) {

					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = "EXITO";

					foreach ($resultAPI["registros"] as $reg) {
						$arrDatos["result"] = $reg;
					}
				} else {
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta agregardatosconsultafolioafiliacion";
					$objGn->grabarLogx('[' . __FILE__ . ']' . $resultAPI["descripcion"]);
				}
			} else {
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("CRetMotivoafiliacion.php\agregardatosconsultafolioafiliacion" . "\tError al ejecutar la consulta \t" . " | " . pg_errormessage());
			}
		} catch (Exception $e) {
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
			$mensaje = 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			$objGn->grabarLogx('[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}
	//OPCION 25
	public static function validarDatosConsultarFolio($keyX)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos 	= array("result" => array());

		$arrApi = array('keyx' => $keyX);

		try {
			$objGn->grabarLogx("Inicio API Rest validarDatosConsultarFolio");

			$resultAPI = $objAPI->consumirApi('validardatosconsultafolioafiliacion', $arrApi);

			$objGn->grabarLogx("Respuesta de Api Rest: " . $resultAPI);

			$objGn->grabarLogx("Fin API Rest validarDatosConsultarFolio");

			if ($resultAPI) {
				$resultAPI = json_decode($resultAPI, true);

				if ($resultAPI['estatus'] == 1) {

					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = "EXITO";

					foreach ($resultAPI["registros"] as $reg) {
						$arrDatos["result"] = $reg;
					}
				} else {
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta validarDatosConsultarFolio";
					$objGn->grabarLogx('[' . __FILE__ . ']' . $resultAPI["descripcion"]);
				}
			} else {
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("CRetMotivoafiliacion.php\validarDatosConsultarFolio" . "\tError al ejecutar la consulta \t" . " | " . pg_errormessage());
			}
		} catch (Exception $e) {
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
			$mensaje = 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			$objGn->grabarLogx('[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}
	//OPCION 26
	public static function actualizarconsultaregtra($foliosol, $sCurp, $keyX, $foliosolpro)
	{ //crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos 	= array("result" => array());

		$arrApi = array(
			'foliosol' => $foliosol,
			'curp' => $sCurp,
			'accion' => $keyX,
			'folioprocesar' => $foliosolpro
		);

		try {
			$objGn->grabarLogx("Inicio API Rest actualizarconsultaregistra");

			$resultAPI = $objAPI->consumirApi('actualizarconsultaregistra', $arrApi);

			$objGn->grabarLogx("Respuesta de Api Rest: " . $resultAPI);

			$objGn->grabarLogx("Fin API Rest actualizarconsultaregistra");

			if ($resultAPI) {
				$resultAPI = json_decode($resultAPI, true);

				if ($resultAPI['estatus'] == 1) {

					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = "EXITO";

					foreach ($resultAPI["registros"] as $reg) {
						$arrDatos["result"] = $reg;
					}
				} else {
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta actualizarconsultaregtra";
					$objGn->grabarLogx('[' . __FILE__ . ']' . $resultAPI["descripcion"]);
				}
			} else {
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("CRetMotivoafiliacion.php\actualizarconsultaregistra" . "\tError al ejecutar la consulta \t" . " | " . pg_errormessage());
			}
		} catch (Exception $e) {
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
			$mensaje = 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			$objGn->grabarLogx('[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}
	//OPCION 27
	public static function obTenercurpTitular($foliosol)
	{ //crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos 	= array();

		$arrApi = array(
			'foliosol' => $foliosol
		);

		try{
			$objGn->grabarLogx("Inicio API Rest obTenercurpTitular");

			$resultAPI = $objAPI->consumirApi('obTenercurpTitular', $arrApi);

			$objGn->grabarLogx("Respuesta de Api Rest: " . $resultAPI);

			$objGn->grabarLogx("Fin API Rest obTenercurpTitular");
			if($resultAPI)
			{
			//indicador que asigna estatus 1, osea correctamente y su descripcion
			$arrDatos['estatus'] = 1;
			$arrDatos['descripcion'] = "EXITO";
			$resultAPI = json_decode($resultAPI,true);

			//$arrDatos['fecha'] = $resulSet['sfechaalta'];
				foreach($resultAPI['registros'] as $reg)
				{
					$arrDatos['curp'] = $reg['respuesta'];
				}
			}
			else
			{
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("constanciaafiliacion.php\tvalidaNumeroTelefono"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}catch (Exception $e) {
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
			$mensaje = 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			$objGn->grabarLogx('[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}
	public static function llamarivr($url)
	{
		$objGn = new CMetodoGeneral();
		$output = array();
		$objGn->grabarLogx("*************Inicia proceso llamarivr***************");
		
		$res = file_get_contents($url);			
		$objGn->grabarLogx("URL IVR" . $url);	
		$objGn->grabarLogx("Respuesta" . $res);	
		
		$objGn->grabarLogx("*************Termina proceso llamarivr***************");
		return $res;
	}

	public static function fnobtenersemillaafiliacion($foliosol)
	{
		$cSql 		= "";
		$arrDatos 	= array("semilla" => '');
		$objGn 		= new CMetodoGeneral();
		try {
			$cnxBd =  new PDO("pgsql:host=" . IPSERVIDOR . ";port=5432;dbname=" . BASEDEDATOS, USUARIO, PASSWORD);
			if ($cnxBd) {
				$cSql = "SELECT fnobtenersemillaafiliacion AS semilla FROM fnobtenersemillaafiliacion($foliosol);";
				$objGn->grabarLogxMot("[fnobtenersemillaafiliacion] -->" . $cSql);
				$resulSet = $cnxBd->query($cSql);

				if ($resulSet) {
					foreach ($resulSet as $reg) {
						$arrDatos["semilla"] = $reg["semilla"];
					}

					$objGn->grabarLogxMot("[semilla: ] -->" . $arrDatos["semilla"]);

				} else {
					$arrErr = $cnxBd->errorInfo();
					$objGn->grabarLogxMot('Error en la consulta [fnobtenersemillaafiliacion]: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
				}
			} else {
				$arrErr = $cnxBd->errorInfo();
				$objGn->grabarLogxMot('Error en la conexion: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
		} catch (Exception $e) {
			$mensaje = 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode() . 'SQL->' . $cSql;
			$objGn->grabarLogxMot('[' . __FILE__ . ']' . $mensaje);
		}

		$cnxBd = null;
		return $arrDatos;
	}
}
?>