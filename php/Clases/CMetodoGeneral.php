<?php
include_once ('global.php');
define('RUTA_LOGX1',	'/sysx/progs/afore/log/CapturaAfiliacion');
define('RUTA_LOGX2',	'/sysx/progs/afore/log/MotivoAfiliacion');
define('RUTA_LOGX3',	'/sysx/progs/afore/log/TipoTraspaso');
define('RUTA_LOGX4',	'/sysx/progs/afore/log/ConsultaSolicitudesAfiliaciones');
include_once('Capirestconsultacurprenapo.php');
include_once('CServicioBusTramites.php');

class CMetodoGeneral
{
	function __construct()
	{
		date_default_timezone_set('America/Mazatlan');
	}
	function __destruct()
	{
	}
	
	var $cnxDb;
	
	public function grabarLogx($cadLogx)
	{
		$cIpCliente = $this->getRealIP();
		$rutaLog =  RUTA_LOGX1 .  '-' . date("Y-m-d") . ".log"; 
		$cad = date("Y-m|H:i:s|") . getmypid() . "|" . $cIpCliente . "| " . $cadLogx . "\n";
		$file = fopen($rutaLog, "a");
		if( $file )
		{
			fwrite($file, $cad);
		}
		fclose($file);
	}

	public function grabarLogxMot($cadLogx)
	{
		$cIpCliente = $this->getRealIP();
		$rutaLog =  RUTA_LOGX2 .  '-' . date("Y-m-d") . ".log"; 
		$cad = date("Y-m|H:i:s|") . getmypid() . "|" . $cIpCliente . "| " . $cadLogx . "\n";
		$file = fopen($rutaLog, "a");
		if( $file )
		{
			fwrite($file, $cad);
		}
		fclose($file);
	}

	public function grabarLogxTipoTras($cadLogx)
	{
		$cIpCliente = $this->getRealIP();
		$rutaLog =  RUTA_LOGX3 .  '-' . date("Y-m-d") . ".log"; 
		$cad = date("Y-m|H:i:s|") . getmypid() . "|" . $cIpCliente . "| " . $cadLogx . "\n";
		$file = fopen($rutaLog, "a");
		if( $file )
		{
			fwrite($file, $cad);
		}
		fclose($file);
	}

	public function grabarLogxConsulSolAfi($cadLogx)
	{
		$cIpCliente = $this->getRealIP();
		$rutaLog =  RUTA_LOGX4 .  '-' . date("Y-m-d") . ".log"; 
		$cad = date("Y-m|H:i:s|") . getmypid() . "|" . $cIpCliente . "| " . $cadLogx . "\n";
		$file = fopen($rutaLog, "a");
		if( $file )
		{
			fwrite($file, $cad);
		}
		fclose($file);
	}
	public function leerXmlServ($archivo, $idElem)
	{
		$res = "";
		if( file_exists($archivo) )
		{
			$datosXml = simplexml_load_file($archivo, "SimpleXMLElement",LIBXML_NOCDATA);
			if($datosXml)
			{
				echo "leyo xml 0000 <br>";
				print_r($datosXml);

				foreach($datosXml->Servidor as $elem)
				{
					echo "<br> " . $idElem .  ' Vs ' .  $elem->key . "<br>";
					print_r($elem);

					if( $elem->id == $idElem )
						$res = $elem;
				}
			}
			else
			{
				$this->grabarLogx("formato XML invalido");
				$res = false;
			}
		}
		else
		{
			$res = "El archivo " . $archivo . " no existe";
			$this->grabarLogx($res);
			$res = false;
		}
		return $res;
	}

	public function obtenerConexionBdPostgres($DireccionIp, $BaseDato, $UsrBaseDato, $PasswdBaseDato)
	{
		global $cnxDb;
		if( pg_connection_status($cnxDb) != PGSQL_CONNECTION_OK )
		{
			$cadConexion = "host=" . $DireccionIp . " dbname=" . $BaseDato . " user=" . $UsrBaseDato . " password=" . $PasswdBaseDato;
			$cnxDb = pg_pconnect($cadConexion);
			if( $cnxDb )
				$this->grabarLogx('[obtenerConexionBdPostgres] Conexion establecida');
		}

		return $cnxDb;
	}

	public function obtenerXML($array)
	{
		$xml = "";
		$xml = "<map>";
		foreach($array as $key => $value)
		{
			$mykey = $key;
			$myvalue= $value;
			$xml .= "<entry><key>".trim($mykey)."</key>"."<value>".trim($myvalue)."</value></entry>";
		}
		$xml .= "</map>";
		return $xml;
	}
	public function consumirServicioEjecutarAplicacion($idServicio,$idServidor,$parametros)
	{

		$servicioBusTramite = new CServicioBusTramites();
		$datos= array();
		$datos = $servicioBusTramite->servicioEjecutarAplicacion($idServicio, $idServidor, $parametros);

		return ($datos);
	}

	public function consumirServicioObtenerRespuesta($idServicio,$folioServicioAfore)
	{
		$servicioBusTramite = new CServicioBusTramites();
		$datos= array();
		$datos = $servicioBusTramite->servicioObtenerRespuesta( $idServicio,$folioServicioAfore);

		return ($datos);

	}
    public function getRealIP() 
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
            return $_SERVER['HTTP_CLIENT_IP'];
           
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
       
        return $_SERVER['REMOTE_ADDR'];
    }
}
?> 