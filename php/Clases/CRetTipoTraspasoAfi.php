<?php
include_once ('global.php');
include_once('Clases/CMetodoGeneral.php');
include_once("JSON.php");
include_once('Capirestcapturaafiliacion.php');

$json = new Services_JSON();

class CTipoTraspasoAfiliacion
{
	//OPCION 2
	public static function obtenerCURP($foliosol)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos 	= array("curp"=>'',"OK"=>1);

		$arrCurp = array(
				'foliosol' => $foliosol
		);

		try
		{
			$objGn->grabarLogx("Inicio API Rest obtenerCURP");

			$resultAPI = $objAPI->consumirApi('obtenerCURP',$arrCurp);

			$objGn->grabarLogx("Resultado API: ".$resultAPI);

			$objGn->grabarLogx("Fin API Rest obtenerCURP");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI,true);

				if ($resultAPI['estatus'] == 1) {

					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = "EXITO";
					//$arrDatos['registros'] = $resultAPI['registros'];

					foreach($resultAPI["registros"] as $reg)
					{
						$arrDatos["OK"] = 0;
            $arrDatos["curp"] = $reg["curp"];
					}
				}
				else
				{
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx( '['.__FILE__.']'.$resultAPI["descripcion"]);
				}
			}
			else
			{
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("casecapturaafiliacion.php\tobtenerCURP"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}

	//OPCION 3
	public static function comboTipoTraspaso()
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos = array();

		$arrCurp = array();

		try
		{
			$objGn->grabarLogx("Inicio API Rest comboTipoTraspaso");

			$resultAPI = $objAPI->consumirApi('comboTipoTraspaso',$arrCurp);

			$objGn->grabarLogx("Resultado API: ".$resultAPI);

			$objGn->grabarLogx("Fin API Rest comboTipoTraspaso");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI,true);

				if ($resultAPI['estatus'] == 1) {

					foreach($resultAPI["registros"] as $reg)
					{
            $arrDatos[] = $reg;
					}
				}
				else
				{
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx( '['.__FILE__.']'.$resultAPI["descripcion"]);
				}
			}
			else
			{
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("casecapturaafiliacion.php\tcomboTipoTraspaso"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;

	}

    //OPCION 4
	public static function comboAforeCedente()
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos = array();

		$arrCurp = array();

		try
		{
			$objGn->grabarLogx("Inicio API Rest comboAforeCedente");

			$resultAPI = $objAPI->consumirApi('comboAforeCedente',$arrCurp);

			$objGn->grabarLogx("Resultado API: ".$resultAPI);

			$objGn->grabarLogx("Fin API Rest comboAforeCedente");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI,true);

				if ($resultAPI['estatus'] == 1) {

					foreach($resultAPI["registros"] as $reg)
					{
            $arrDatos[] = $reg;
					}
				}
				else
				{
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx( '['.__FILE__.']'.$resultAPI["descripcion"]);
				}
			}
			else
			{
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("casecapturaafiliacion.php\tcomboAforeCedente"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;

	}

    //OPCION 5
	public static function comboPeriodoEstado()
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos = array();

		$arrCurp = array();

		try
		{
			$objGn->grabarLogx("Inicio API Rest comboPeriodoEstado");

			$resultAPI = $objAPI->consumirApi('comboPeriodoEstado',$arrCurp);

			$objGn->grabarLogx("Resultado API: ".$resultAPI);

			$objGn->grabarLogx("Fin API Rest comboPeriodoEstado");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI,true);

				if ($resultAPI['estatus'] == 1) {

					foreach($resultAPI["registros"] as $reg)
					{
            $arrDatos[] = $reg;
					}
				}
				else
				{
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx( '['.__FILE__.']'.$resultAPI["descripcion"]);
				}
			}
			else
			{
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("casecapturaafiliacion.php\tcomboPeriodoEstado"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;

	}

    //OPCION 6
	public static function ObtenerNumeroTarjeta($sIpRemoto)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos 	= array("respuesta"=>'');

		$arrCurp = array(
				'sIpRemoto' => $sIpRemoto
		);

		try
		{
			$objGn->grabarLogx("Inicio API Rest ObtenerNumeroTarjeta");

			$resultAPI = $objAPI->consumirApi('ObtenerNumeroTarjeta',$arrCurp);

			$objGn->grabarLogx("Resultado API: ".$resultAPI);

			$objGn->grabarLogx("Fin API Rest ObtenerNumeroTarjeta");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI,true);

				if ($resultAPI['estatus'] == 1) {

					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = "EXITO";

					foreach($resultAPI["registros"] as $reg)
					{
						$arrDatos["respuesta"] = trim($reg["respuesta"]);
					}
				}
				else
				{
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx( '['.__FILE__.']'.$resultAPI["descripcion"]);
				}
			}
			else
			{
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("casecapturaafiliacion.php\tObtenerNumeroTarjeta"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}

    //OPCION 7
	public static function guardarNumeroTarjeta($foliosol,$numtarjeta)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos 	= array("respuesta"=>'');

		$arrCurp = array(
				'foliosol' => $foliosol,
				'numtarjeta' => $numtarjeta
		);

		try
		{
			$objGn->grabarLogx("Inicio API Rest guardarNumeroTarjeta");

			$resultAPI = $objAPI->consumirApi('guardarNumeroTarjeta',$arrCurp);

			$objGn->grabarLogx("Resultado API: ".$resultAPI);

			$objGn->grabarLogx("Fin API Rest guardarNumeroTarjeta");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI,true);

				if ($resultAPI['estatus'] == 1) {

					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = "EXITO";
					//$arrDatos['registros'] = $resultAPI['registros'];

					foreach($resultAPI["registros"] as $reg)
					{
						$arrDatos["respuesta"] = $reg["respuesta"];
					}
				}
				else
				{
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx( '['.__FILE__.']'.$resultAPI["descripcion"]);
				}
			}
			else
			{
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("casecapturaafiliacion.php\tguardarNumeroTarjeta"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;

	}
}
?>