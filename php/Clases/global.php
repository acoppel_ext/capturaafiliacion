<?php

$xml                        = simplexml_load_file("../../conf/webconfig.xml");

$ipServidor                 = $xml->Spa;
$sUsuario                   = $xml->Usuario;
$sBasedeDatos               = $xml->Basededatos;
$sPassword                  = $xml->Password;

$ipAdmonAfore               = $xml->IpAdmonAfore;
$usuarioAdmonAfore          = $xml->UsuarioAdmonAfore;
$baseDeDatosAdmonAfore      = $xml->BasedeDatosAdmonAfore;
$passwordAdmonAfore         = $xml->PasswordAdmonAfore;

$ipServidorBusTramites      = $xml->IpBusTramites;
$sUsuarioBusTramites        = $xml->UsuarioBusTramites;
$sBasedeDatosBusTramites    = $xml->BasededatosBusTramites;
$sPasswordBusTramites       = $xml->PasswordBusTramites;
$sUsuarioPublicaFtp         = $xml->UsuarioPublicacionFtp;
$PswdPublicaFtp             = $xml->PasswordPublicacionFtp;
$ipPublicacionImagenes      = $xml->IpPublicacionImagenes;

$ipServidorInfx             = $xml->Ipinformix;
$sUsuarioInfx               = $xml->UsuarioInf;
$sBasedeDatosInfx           = $xml->BasededatosInf;
$sPasswordInfx              = $xml->PasswordInf;

$IpServiciosAfore           = $xml->IpServiciosAfore;
$UsuarioServiciosAforeBD    = $xml->UsuarioServiciosAforeBD;
$BasedeDatosServiciosAfore  = $xml->BasedeDatosServiciosAfore;
$PasswordServiciosAfore     = $xml->PasswordServiciosAfore;

//Llena las variables define apartir de la lectura del xml
define('OK__',	                    1);
define('DEFAULT__', 	            0);
define('ERR__',	                    -1);

define('ARCHIVO_CONF_WS',	        '../wsmoduloafore/wsconf.dat');
define('path_wsdl', 		        '../wsmoduloafore/wsModuloAfore.wsdl');

define("IPSERVIDOR",                $ipServidor);
define("BASEDEDATOS",               $sBasedeDatos);
define("USUARIO",                   $sUsuario);
define("PASSWORD",                  $sPassword);

define("IPADMONAFORE",              $ipAdmonAfore);
define("BASEDEDATOSADMONAFORE",     $baseDeDatosAdmonAfore);
define("USUARIOADMONAFORE",         $usuarioAdmonAfore);
define("PASSWORDADMONAFORE",        $passwordAdmonAfore);

define("IPSERVIDORBUSTRAMITES",     $ipServidorBusTramites);
define("BASEDEDATOSBUSTRAMITES",    $sBasedeDatosBusTramites);
define("USUARIOBUSTRAMITES",        $sUsuarioBusTramites);
define("PASSWORDBUSTRAMITES",       $sPasswordBusTramites);
define("USUARIO_PUBLICA_FTP",       $sUsuarioPublicaFtp);
define("PASS_PUBLICA_FTP",          $PswdPublicaFtp);
define("IP_PUBLICACION_IMAGENES",   $ipPublicacionImagenes);

define("IPSERVIDORINF",             $ipServidorInfx);
define("BASEDEDATOSINF",            $sBasedeDatosInfx);
define("USUARIOINF",                $sUsuarioInfx);
define("PASSWORDINF",               $sPasswordInfx);

define("IPSERVICIOSAFORE",          $IpServiciosAfore);
define("BASEDEDATOSSERVICIOSAFORE", $BasedeDatosServiciosAfore);
define("USUARIOSERVICIOSAFOREBD",   $UsuarioServiciosAforeBD);
define("PASSWORDSERVICIOSAFORE",    $PasswordServiciosAfore);
?>