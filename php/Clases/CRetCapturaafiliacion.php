<?php
include_once ('global.php');
include_once ('CMetodoGeneral.php');
include_once("JSON.php");
include_once("Clases/Capirestcapturaafiliacion.php");

$json = new Services_JSON();

class CCapturaafiliacion
{
	/******************** Informacion General ********************/
	//OPCION 1
	public static function obtnerinformacionpromotor($iEmpleado)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos = array();
		//Obtenemos la fecha actual y se la mandamos como parametro
		setlocale(LC_TIME,"es_ES");
		$cFechaDia = utf8_encode(strftime("%A, %d de %B de %Y"));
		$cFechaActual = strftime("%d/%B/%Y");
		$cFechaSol = strftime("%F");
		$arrDatos["dFechaActual"] = $cFechaDia;
		$arrDatos["iEmpleado"] = $iEmpleado;

		$arrApi = array(
				'iEmpleado' => $iEmpleado
		);

		try
		{
			$objGn->grabarLogx("Inicio API Rest obtnerinformacionpromotor");

			$resultAPI = $objAPI->consumirApi('obtnerinformacionpromotor',$arrApi);

			$objGn->grabarLogx("Respuesta de Api Rest: ".$resultAPI);

			$objGn->grabarLogx("Fin API Rest obtnerinformacionpromotor");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI,true);

				if ($resultAPI['estatus'] == 1) {

					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = "EXITO";

					foreach($resultAPI["registros"] as $reg)
					{
						$arrDatos["iCodigo"] 		= $reg["icodigo"];
						$arrDatos["cMensaje"] 		= TRIM($reg["cmensaje"]);
						$arrDatos["cNombre"] 		= TRIM($reg["cnombre"]);
						$arrDatos["cTienda"] 		= TRIM($reg["ctienda"]);
						$arrDatos["espromotor"] 	= $reg["espromotor"];
						$arrDatos["curppromotor"] 	= TRIM($reg["curppromotor"]);
						$arrDatos["claveconsar"] 	= $reg["claveconsar"];
					}
				}
				else
				{
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx( '['.__FILE__.']'.$resultAPI["descripcion"]);
				}
			}
			else
			{
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("casecapturaafiliacion.php\tobtnerinformacionpromotor"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}

		return $arrDatos;
	}

	//OPCION 2
	public static function obtenertiposolicitud()
	{
		$cSql 		= "";
		$arrDatos 	= array();
		$objGn 		= new CMetodoGeneral();

		try
		{
			$cnxBd =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
			if($cnxBd)
			{
				$cSql = "SELECT itiposolictud, itipotraspoaso, TRIM(cdescripcion) AS cdescripcion FROM fnobtenertiposolicitudafiliacion(2);";
				$objGn->grabarLogx("[obtenertiposolicitud] -->".$cSql);
				
				$resulSet = $cnxBd->query($cSql);
				if($resulSet)
				{
					foreach($resulSet as $reg)
					{			
						$arrDatos[]=array_map('trim', $reg);
					}

				}else{
					$arrErr = $cnxBd->errorInfo();
					 $objGn->grabarLogx( 'Error en la consulta [obtenertiposolicitud]: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
				}
			}else{
				 $arrErr = $cnxBd->errorInfo();
				  $objGn->grabarLogx( 'Error en la conexion: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
		}catch (Exception $e){
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode().'SQL->'.$cSql;
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		$cnxBd = null;
		return $arrDatos;
	}

	//OPCION 3
	public static function validacionCurpAfiliacion($sCurp,$iTipoSol,$sIpRemoto)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos 	= array("error"=>0,"nss"=>0,"mensaje"=>'',"redireccionar"=>'');

		$arrApi = array(
				'sCurp' => $sCurp,
				'iTipoSol' => $iTipoSol,
				'sIpRemoto' => $sIpRemoto
		);

		try
		{
			$objGn->grabarLogx("Inicio API Rest validacionCurpAfiliacion");

			$resultAPI = $objAPI->consumirApi('validacionCurpAfiliacion',$arrApi);

			$objGn->grabarLogx("Respuesta de Api Rest: ".$resultAPI);

			$objGn->grabarLogx("Fin API Rest validacionCurpAfiliacion");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI,true);

				if ($resultAPI['estatus'] == 1) {

					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = "EXITO";

					foreach($resultAPI["registros"] as $reg)
					{
						$arrDatos["error"] = (int)$reg["ierror"];
						$arrDatos["nss"] 	= utf8_decode(TRIM($reg["inss"]));
						$arrDatos["mensaje"] = TRIM($reg["cmensaje"]);
						$arrDatos["redireccionar"] = TRIM($reg["credireccionar"]);
					}
				}
				else
				{
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx( '['.__FILE__.']'.$resultAPI["descripcion"]);
				}
			}
			else
			{
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("casecapturaafiliacion.php\tvalidacionCurpAfiliacion"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}

	//OPCION 4
	//Validacion de la existencia del video.
	public static function validarVideo($foliosol)
	{
		$arrDatos 	= array("error"=>0);
		$objGn 		= new CMetodoGeneral();
		$objAPI 	= new Capirestcapturaafiliacion();
		$arrApi 	= array('foliosol' => $foliosol);

		try
		{
			$objGn->grabarLogx("Inicio API Rest validarVideo");

			$resultAPI = $objAPI->consumirApi('validarVideo',$arrApi);

			$objGn->grabarLogx("Respuesta de Api Rest: ".$resultAPI);

			$objGn->grabarLogx("Fin API Rest validarVideo");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI, true);

				if ($resultAPI['estatus'] == 1)
				{
					foreach($resultAPI['registros'] as $reg)
					{
						$arrDatos["error"] = (int)$reg["fnexistevideo"];
					}
				}
				else
				{
					$objGn->grabarLogx("[Error: ] -->".$resultAPI["descripcion"]);
				}
			}
			else
			{
				$objGn->grabarLogx( 'Error en la consulta [fnexistevideo]');
			}
		}
		catch (Exception $e)
		{
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}

		return $arrDatos;
	}
	
	//OPCION 5
	public static function validarExpediente($sCurp,$sIpRemoto)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos 	= array("error"=>0,"mensaje"=>'',"redireccionar"=>'');

		$arrApi = array(
				'sCurp' => $sCurp,
				'sIpRemoto' => $sIpRemoto,
		);

		try
		{
			$objGn->grabarLogx("Inicio API Rest validarExpediente");

			$resultAPI = $objAPI->consumirApi('validarExpediente',$arrApi);

			$objGn->grabarLogx("Respuesta de Api Rest: ".$resultAPI);

			$objGn->grabarLogx("Fin API Rest validarExpediente");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI,true);

				if ($resultAPI['estatus'] == 1) {

					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = "EXITO";

					foreach($resultAPI["registros"] as $reg)
					{
						$arrDatos["error"] 	= (int)$reg["error"];
						$arrDatos["mensaje"] 	= utf8_encode(TRIM($reg["mensaje"]));
						$arrDatos["redireccionar"] 	= TRIM($reg["redireccionar"]);
					}
				}
				else
				{
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx( '['.__FILE__.']'.$resultAPI["descripcion"]);
				}
			}
			else
			{
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("casecapturaafiliacion.php\tvalidarExpediente"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}

	//OPCION 6
	public static function obtenerFolioEnrolamiento($foliosol)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos 	= array("folioenrolamiento"=>0);

		$arrApi = array(
				'foliosol' => $foliosol
		);

		try
		{
			$objGn->grabarLogx("Inicio API Rest obtenerFolioEnrolamiento");

			$resultAPI = $objAPI->consumirApi('obtenerFolioEnrolamiento',$arrApi);

			$objGn->grabarLogx("Respuesta de Api Rest: ".$resultAPI);

			$objGn->grabarLogx("Fin API Rest obtenerFolioEnrolamiento");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI,true);

				if ($resultAPI['estatus'] == 1) {

					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = "EXITO";

					foreach($resultAPI["registros"] as $reg)
					{
						$arrDatos["folioenrolamiento"] = $reg["folioenrolamiento"];
					}
				}
				else
				{
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx( '['.__FILE__.']'.$resultAPI["descripcion"]);
				}
			}
			else
			{
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("casecapturaafiliacion.php\tobtenerFolioEnrolamiento"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}

	//OPCION 7
	//OPCION 8

	//OPCION 9
	public static function validarFolioSol($foliosol)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos 	= array("valor"=>0);

		$arrCurp = array(
				'foliosol' => $foliosol
		);

		try
		{
			$objGn->grabarLogx("Inicio API Rest validarFolioSol");

			$resultAPI = $objAPI->consumirApi('validarFolioSol',$arrCurp);

			$objGn->grabarLogx("Respuesta Api: ".$resultAPI);

			$objGn->grabarLogx("Fin API Rest validarFolioSol");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI,true);

				if ($resultAPI['estatus'] == 1) {
					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = "EXITO";

					foreach($resultAPI["registros"] as $reg)
					{
						$arrDatos["valor"] = (int)$reg["valor"];
					}
				}
				else
				{
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx( '['.__FILE__.']'.$resultAPI["descripcion"]);
				}
			}
			else
			{
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("casecapturaafiliacion.php\tvalidarFolioSol"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}

		return $arrDatos;
	}

	//OPCION 10
	public static function cuentaConManos($foliosol)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos 	= array("valor"=>0);

		$arrApi = array(
				'foliosol' => $foliosol
		);

		try
		{
			$objGn->grabarLogx("Inicio API Rest cuentaConManos");

			$resultAPI = $objAPI->consumirApi('cuentaConManos',$arrApi);

			$objGn->grabarLogx("Fin API Rest cuentaConManos");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI,true);

				if ($resultAPI['estatus'] == 1) {

					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = "EXITO";

					foreach($resultAPI["registros"] as $reg)
					{
						$arrDatos["respuesta"] = $reg["respuesta"];
					}
				}
				else
				{
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx( '['.__FILE__.']'.$resultAPI["descripcion"]);
				}
			}
			else
			{
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("casecapturaafiliacion.php\tcuentaConManos"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}

	//OPCION 11
	public static function obtenerIPServidor($sIpRemoto)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos 	= array("IP"=>'');

		$arrApi = array(
				'sIpRemoto' => $sIpRemoto
		);

		try
		{
			$objGn->grabarLogx("Inicio API Rest obtenerIPServidor");

			$resultAPI = $objAPI->consumirApi('obtenerIPServidor',$arrApi);

			$objGn->grabarLogx("Fin API Rest obtenerIPServidor");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI,true);

				if ($resultAPI['estatus'] == 1) {

					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = "EXITO";

					foreach($resultAPI["registros"] as $reg)
					{
						$arrDatos["IP"] = $reg["ipofflinelnx"];
					}
				}
				else
				{
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx( '['.__FILE__.']'.$resultAPI["descripcion"]);
				}
			}
			else
			{
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("casecapturaafiliacion.php\tobtenerIPServidor"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}

	//OPCION 12
	public static function guardarInfoAfiliacion($iOpcGuar,$foliosol,$iTipoSol,$esimss,$tipotrasp,$cFolioEdo,$cAforeEdo,$cCuatriEdo,$cMotivo,$cFolioImpli,$iTipoAdmon,$cDepPrev,$aforeced)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos 	= array("respuesta"=>0);

		$arrApi = array(
				'iOpcGuar' => $iOpcGuar,
				'foliosol' => $foliosol,
				'iTipoSol' => $iTipoSol,
				'esimss' => $esimss,
				'tipotrasp' => $tipotrasp,
				'cFolioEdo' => $cFolioEdo,
				'cAforeEdo' => $cAforeEdo,
				'cCuatriEdo' => $cCuatriEdo,
				'cMotivo' => $cMotivo,
				'cFolioImpli' => $cFolioImpli,
				'iTipoAdmon' => $iTipoAdmon,
				'cDepPrev' => $cDepPrev,
				'aforeced' => $aforeced
		);

		try
		{
			$objGn->grabarLogx("Inicio API Rest guardarInfoAfiliacion");
			$objGn->grabarLogx("**********************************************");
			$objGn->grabarLogx("iOpcGuar ->" .$iOpcGuar ."foliosol ->". $foliosol, "iTipoSol ->". $iTipoSol. "esimss -> " .$esimss."tipotrasp ->". $tipotrasp."cFolioEdo ->" .$cFolioEdo."cAforeEdo ->" .$cAforeEdo."cCuatriEdo ->" .$cCuatriEdo."cMotivo ->" .$cMotivo."cFolioImpli ->". $cFolioImpli."iTipoAdmon ->" .$iTipoAdmon."cDepPrev ->" .$cDepPrev."aforeced ->" .$aforeced);
			$objGn->grabarLogx("**********************************************");

			$resultAPI = $objAPI->consumirApi('guardarInfoAfiliacion',$arrApi);

			$objGn->grabarLogx("Fin API Rest guardarInfoAfiliacion");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI,true);

				if ($resultAPI['estatus'] == 1) {

					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = "EXITO";

					foreach($resultAPI["registros"] as $reg)
					{
						$arrDatos["respuesta"] = $reg["respuesta"];
					}
				}
				else
				{
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx( '['.__FILE__.']'.$resultAPI["descripcion"]);
				}
			}
			else
			{
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("casecapturaafiliacion.php\tguardarInfoAfiliacion"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}

	//OPCION 13
	public static function obtenerPaginaRedireccionar($opcionpag,$foliosol,$iEmpleado,$sIpRemoto,$reimpre)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos 	= array("destino"=>'');

		$arrApi = array(
				'opcionpag' => $opcionpag,
				'foliosol' => $foliosol,
				'iEmpleado' => $iEmpleado,
				'sIpRemoto' => $sIpRemoto,
				'reimpre' => $reimpre
		);

		try
		{
			$objGn->grabarLogx("Inicio API Rest obtenerPaginaRedireccionar");

			$resultAPI = $objAPI->consumirApi('obtenerPaginaRedireccionar',$arrApi);

			$objGn->grabarLogx("Fin API Rest obtenerPaginaRedireccionar");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI,true);

				if ($resultAPI['estatus'] == 1) {

					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = "EXITO";

					foreach($resultAPI["registros"] as $reg)
					{
						$arrDatos["destino"] = $reg["destino"];
					}
				}
				else
				{
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx( '['.__FILE__.']'.$resultAPI["descripcion"]);
				}
			}
			else
			{
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("casecapturaafiliacion.php\tobtenerPaginaRedireccionar"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}

	//OPCION 14
	public static function obtenerCurpSolcontancia($nss,$iTipoSol,$sIpRemoto)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos 	= array("error"=>0,"mensaje"=>'',"redireccionar"=>"");

		$arrApi = array(
				'nss' => $nss,
				'iTipoSol' => $iTipoSol,
				'sIpRemoto' => $sIpRemoto
		);

		try
		{
			$objGn->grabarLogx("Inicio API Rest obtenerCurpSolcontancia");

			$resultAPI = $objAPI->consumirApi('obtenerCurpSolcontancia',$arrApi);

			$objGn->grabarLogx("Fin API Rest obtenerCurpSolcontancia");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI,true);

				if ($resultAPI['estatus'] == 1) {

					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = "EXITO";

					foreach($resultAPI["registros"] as $reg)
					{
						$arrDatos["error"] 	= (int)$reg["error"];
						$arrDatos["mensaje"] 	= utf8_decode(TRIM($reg["mensaje"]));
						$arrDatos["redireccionar"] 	= TRIM($reg["redireccionar"]);
					}
				}
				else
				{
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx( '['.__FILE__.']'.$resultAPI["descripcion"]);
				}
			}
			else
			{
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("casecapturaafiliacion.php\tobtenerCurpSolcontancia"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}
	
	//OPCION 15
	public static function consultarNSS($curp)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos 	= array("error"=>0,"mensaje"=>'',"redireccionar"=>"");

		$arrApi = array(
				'curp' => $curp,
		);

		try
		{
			$objGn->grabarLogx("Inicio API Rest consultarNSS");

			$resultAPI = $objAPI->consumirApi('consultarNSS',$arrApi);

			$objGn->grabarLogx("Respuesta de Api Rest: ".$resultAPI);

			$objGn->grabarLogx("Fin API Rest consultarNSS");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI,true);

				if ($resultAPI['estatus'] == 1) {

					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = "EXITO";

					foreach($resultAPI["registros"] as $reg)
					{
						$arrDatos["nss"] 	= TRIM($reg["nss"]);
					}
				}
				else
				{
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx( '['.__FILE__.']'.$resultAPI["descripcion"]);
				}
			}
			else
			{
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("casecapturaafiliacion.php\tconsultarNSS"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}

	//OPCION 16
	public static function obtenerfecha()
	{
		
		$arrDatos 	= array("fechadia"=>'',"fechadiahora"=>'',"fecharen"=>'');
		$objGn 		= new CMetodoGeneral();

		$fechadiahora  	= "1900-01-01 00:00:00";
		$fecharen		= "1900-01-01";
		$year   		= "0000";
		$month  		= "00";
		$day    		= "00";
		$hora			= "00:00:00";
		
		$fechaactual 	= date('Y-m-d');
		$hora 			= date('H:i:s');
		$year 			= date('Y');
		$month 			= date('m');
		$day  			= date('d');

		$objGn->grabarLogxConsulSolAfi("[obtenerfecha]Fecha dia--> " . $fechaactual);

		$fechadiahora = $fechaactual . " " . $hora;
		$objGn->grabarLogxConsulSolAfi("[obtenerfecha]Fecha dia/hora--> " . $fechadiahora);

		// Determinar la fecha inicial
		if ($day == 15) // Si el dia actual es igual a 15
			$fecharen = $year . "-" . $month . "-" . $day;// La vigencia comienza hoy
		else if ($day > 15) // Si el dia actual es mayor a 15
			$fecharen = $year . "-" . $month . "-" . 15;// La fecha inicial de la vigencia es el dia 14 del mes actual
		else if ($month == 1) // Si el mes actual es Enero
			$fecharen = ($year-1) . "-" . 12 . "-" . 15;// La fecha de vigencia es el dia 14 de diciembre del año pasado
		else 
			$fecharen = $year . "-" . ($month-1) . "-" . 15;// La fecha de vigencia es el dia 14 del mes pasado
			
		$objGn->grabarLogxConsulSolAfi("[obtenerfecha]Fecha ren--> " . $fecharen);

		$arrDatos["fechadia"] 		= $fechaactual;
		$arrDatos["fechadiahora"] 	= $fechadiahora;
		$arrDatos["fecharen"] 		= $fecharen;
	
		return $arrDatos;
	}
	//OPCION 16
	public static function ConsultarSolicitudConstancia($sDato)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos 	= array();

		$arrApi = array(
				'sDato' => $sDato,
		);

		try
		{
			$objGn->grabarLogx("Inicio API Rest ConsultarSolicitudConstancia");

			$resultAPI = $objAPI->consumirApi('ConsultarSolicitudConstancia',$arrApi);

			$objGn->grabarLogx("Respuesta de Api Rest: ".$resultAPI);

			$objGn->grabarLogx("Fin API Rest ConsultarSolicitudConstancia");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI,true);

				if ($resultAPI['estatus'] == 1) {

					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = "EXITO";

					foreach($resultAPI["registros"] as $reg)
					{
						$arrDatos[]=array_map('trim', $reg);
					}
				}
				else
				{
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx( '['.__FILE__.']'.$resultAPI["descripcion"]);
				}
			}
			else
			{
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("casecapturaafiliacion.php\tConsultarSolicitudConstancia"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}

	//OPCION 17
	public static function tieneexcepcion($iFolioSol)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos 	= array();

		$datos = '';

		$arrApi = array(
				'iFolioSol' => $iFolioSol,
		);

		try
		{
			$objGn->grabarLogx("Inicio API Rest tieneexcepcion");

			$resultAPI = $objAPI->consumirApi('tieneexcepcion',$arrApi);

			$objGn->grabarLogx("Respuesta de Api Rest: ".$resultAPI);

			$objGn->grabarLogx("Fin API Rest tieneexcepcion");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI,true);

				if ($resultAPI['estatus'] == 1) {

					foreach($resultAPI["registros"] as $reg)
					{
						$datos = $reg["respuesta"];
					}
					$objGn->grabarLogx("La excepcion es:".$datos);
				}
				else
				{
					$objGn->grabarLogx( '['.__FILE__.']'.$resultAPI["descripcion"]);
				}
			}
			else
			{
				$objGn->grabarLogx("Error al obtener la excepcion:".$datos);
				throw new Exception("casecapturaafiliacion.php\tieneexcepcion"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $datos;
	}

	//OPCION 18
	public static function validacionine($foliosol)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos 	= array("respuesta"=>0);

		$arrApi = array(
				'foliosol' => $foliosol,
		);

		try
		{
			$objGn->grabarLogx("Inicio API Rest validacionine");

			$resultAPI = $objAPI->consumirApi('validacionine',$arrApi);

			$objGn->grabarLogx("Respuesta de Api Rest: ".$resultAPI);

			$objGn->grabarLogx("Fin API Rest validacionine");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI,true);

				if ($resultAPI['estatus'] == 1) {

					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['estatus'] = 1;
					$arrDatos['descripcion'] = "EXITO";

					foreach($resultAPI["registros"] as $reg)
					{
						$arrDatos["respuesta"] = $reg["respuesta"];
					}
				}
				else
				{
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx( '['.__FILE__.']'.$resultAPI["descripcion"]);
				}
			}
			else
			{
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("CRetCapturaafiliacion.php\tvalidacionine"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}
	public static function caminoWSRenapo($sCurp, $iEmpleado, $iFolioAfiliacion, $iEsTitular)
	{
		$objGn = new CMetodoGeneral();
		$iFolioAfore = 0;
		$arrServicioRenapo = CCapturaafiliacion::ejecutaServicioRenapoCurp($sCurp,$iEmpleado);
		if($arrServicioRenapo['Respuesta'] == 1)
		{
			$iFolioAfore = $arrServicioRenapo['iFolioAfore'];
			$arrGuardaSolRenapo = CCapturaafiliacion::guardarSolicitudRenapo($iFolioAfore, $iFolioAfiliacion, $iEmpleado, $sCurp, $iEsTitular);
			if($arrGuardaSolRenapo['estatus'] == 1)
			{
				sleep(5);
				$arrResp = CCapturaafiliacion::consultaRespuestaRenapo($iFolioAfore,$iFolioAfiliacion);
				$mensajelog = 'se ejecuto respuesta de renapo';
				$objGn->grabarLogx( '[VerificarCurpRenapoApi]' . $mensajelog);
			}
			else
			{
				$mensajelog = 'Error la guardar la solicitud del servicio de renapo.';
				$objGn->grabarLogx( '[VerificarCurpRenapoApi]' . $mensajelog);
				$arrResp['Exito'] = -1;
				$arrResp['Descripcion'] = $mensajelog;
				$arrResp['codigoerrorrenapo'] = -1;
			}
		}
		else
		{
			$mensajelog = 'Error la ejecutar el servicio de renapo.';
			$objGn->grabarLogx( '[VerificarCurpRenapoApi]' . $mensajelog);
			$arrResp['Exito'] = -1;
			$arrResp['Descripcion'] = $mensajelog;
			$arrResp['codigoerrorrenapo'] = -1;
		}
		return $arrResp;
	}
	public static function ejecutaServicioRenapoCurp($sCurp, $iEmpleado)
	{		
		//se crea arreglo a retornar
		$arrServicioRenapo = array(
		'iFolioAfore' => 0,
		'Respuesta' =>0,
		'Descripcion' =>''
		);
		$arrResp = array();
		$idServidor  = 5;
		$idServicioC = 9906; //curp
		//CREA UN OBJETO DE LA CLASE
		$objGn = new CMetodoGeneral();
		//se declaran varibles en vacio y 0 por si falla la incacion tenga algo por default
		$arrResp->descripcionRespuesta= '';
		$arrResp->folioServicioAfore = 0;
		$arrResp->respondioServicio = 0;
		//se obtine la fecha del servidor
		date_default_timezone_set('America/Mazatlan');
		setlocale(LC_TIME, 'spanish');
		//Se crea array a mandar 
		$array = array(
			    'clavecurp' => $sCurp
				 );
		//Crear XML a enviar de parametros	
		$xml = $objGn->obtenerXML($array);
		//Ejecutar el Servicio 
		$mensajelog = 'idServidorC: '.$idServicioC.' idServidor: '.$idServidor;
		$objGn->grabarLogx( '[VerificarCurpRenapoApi]' . $mensajelog);
		$arrResp = $objGn->consumirServicioEjecutarAplicacion($idServicioC, $idServidor, $xml);
		//pasa la respuesta obtenida del metodo anterior
		$mensaje = $arrResp->descripcionRespuesta;
		$folioServicioAfore = $arrResp->folioServicioAfore;
		$respuestaservicio = $arrResp->respondioServicio;
		
		$arrServicioRenapo['Descripcion'] =  $mensaje;
		$arrServicioRenapo['Respuesta'] =  $respuestaservicio;
		
		$mensajelog = "Mensaje: ".$mensaje.' FolioAfore: '.$folioServicioAfore." RespServicioInv: ".$respuestaservicio." RespServicioEI: ".$respuestaEI;
		$objGn->grabarLogx( '[VerificarCurpRenapoApi]' . $mensajelog);
		if ($respuestaservicio == 1)
		{
			$iFolioAfore = $folioServicioAfore;
			$arrServicioRenapo['iFolioAfore'] = $iFolioAfore;
		}
		else
		{
			$mensajelog = "El servicio no responde. Favor de intentar la Validación Renapo nuevamente o continuar con el trámite de forma manual";
			$objGn->grabarLogx( '[VerificarCurpRenapoApi]' . $mensajelog);
			$arrResp['Exito'] = -1;
			$arrResp['Descripcion'] = $mensajelog;
			$arrResp['Error'] = 2;
		}
		return $arrServicioRenapo;
		
	}
	public static function guardarSolicitudRenapo($iFolioAfore, $iFolioSolicitud, $iEmpleado, $cCurp, $iEsTitular)
	{
		$bRegresa = false;
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconsultacurprenapo();
		//SE DECLARA UN ARREGLO
		$arrDatos = array("estatus"=>0,
					  "MensajeDescrip"=>"",
					  "respuesta"=>""
					   );
		$iProceso	 = 1;
		if($iEsTitular == 1)
		{
			$idsubproceso = 8;
		}
		else
		{
			$idsubproceso = 462;
		}
		$cFechaNacimiento = '1900-01-01';
		//se obtiene ip del modulo
		$iPmodulo = $objGn->getRealIP();
		$arrApi = array(
			"iFolioAfore"  			=> $iFolioAfore,
			"iFolioSolicitud"  		=> $iFolioSolicitud,
			"iProceso"  			=> $iProceso,
			"iEmpleado"  			=> $iEmpleado,
			"idsubproceso"  		=> $idsubproceso,
			"cCurp"  				=> $cCurp,
			"cApellidopaterno" 		=> '',
			"cApellidomaterno"  	=> '',
			"cNombres"  			=> '',
			"cSexo"  				=> '',
			"cFechaNacimiento"  	=> $cFechaNacimiento,
			"iSlcEntidadNacimiento" => 'ND',
			"iPmodulo" => $iPmodulo);
			$mensajelog  = 'Folio Afore: ' . $iFolioAfore . ' Folio Afiliacion: ' . $iFolioSolicitud . ' Proceso: ' . $iProceso . ' Empleado: ' . $iEmpleado . ' SubProceso: ' . $idsubproceso . ' CURP: ' . $cCurp;
			$objGn->grabarLogx( '[VerificarCurpRenapoApi]' . $mensajelog);
		try
		{
			$resultAPI = $objAPI->consumirApi('guardarSolicitudRenapo',$arrApi);
			$resultAPI = json_decode($resultAPI,true);
			//VERIFICA QUE SE HAYA EJECUTADO CORRECTAMENTE
			if($resultAPI)				
			{
				//INDICADOR QUE ASGNA ESTATUS 1, OSEA QUE ES CORRECTO Y SU DESCRIPCION
				$arrDatos['estatus'] = 1;
				$arrDatos['MensajeDescrip'] = "EXITO";
				$bRegresa = true;
				foreach($resultAPI['registros'] as $reg)
				{			
					$arrDatos['respuesta']= $reg["valor"];
				}
			}
			else
			{
				// SI EXISTE UN ERROR EN LA CONSULTA GUARDARA EL SIGUIENTE MENSAJE
				$mensajelog  = "Se presento un problema al ejecutar la consulta";
				$objGn->grabarLogx( '[VerificarCurpRenapoApi]' . $mensajelog);
			}
		}
		catch (Exception $e)
		{
			//CACHA LA EXCEPCION POR LA QUE FALLO LA EJECUCION DEL QUERY Y LO MANDA COMO PARAMETRO PARA ESCRIBIR LA DESCRIPCION DEL ERROR.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[VerificarCurpRenapoApi]' . $mensaje);
		}
		return $arrDatos;	
	}
	public static function consultaRespuestaRenapo($iFolioAfore)
	{
		//CREA UN OBJETO DE LA CLASE
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestconsultacurprenapo();

		//SE DECLARA UN ARREGLO
		$arrDatos = array("estatus"=>0,
					  "MensajeDescrip"=>"",
					  "tipoerror"=>-1,
					  "codigoerrorrenapo"=>-1, 
					  "descerror"=>""
					   );
		$arrFolio = array(
			'iFolioAfore' => $iFolioAfore
		);
		try
		{
			$objGn->grabarLogx("[VerificarCurpRenapoApi] Inicio API Rest");
			$resultAPI = $objAPI->consumirApi('consultaRespuestaRenapo',$arrFolio);
			if($resultAPI)
			{	
				$resultAPI = json_decode($resultAPI,true);				
				//VERIFICA QUE SE HAYA EJECUTADO CORRECTAMENTE
				if($resultAPI['estatus'] == 1)
				{
					//INDICADOR QUE ASGNA ESTATUS 1, OSEA QUE ES CORRECTO Y SU DESCRIPCION				
					foreach($resultAPI["registros"] as $reg)
					{	
						$arrDatos['estatus'] 				= 1;
						$arrDatos['MensajeDescrip']		 	= "EXITO";
						$arrDatos['tipoerror']				= $reg["idu_tipoerror"];
						$arrDatos['codigoerrorrenapo']		= $reg["idu_codigoerrorrenapo"];
						$arrDatos['descerror']				= utf8_encode(trim($reg["mensaje"]));
						$iEstatus = $arrDatos['estatus'];
						$iCodigoError = $arrDatos['codigoerrorrenapo'];
						$cDescripcionError = $arrDatos['descerror'];
					}
					
					$mensaje = "consulto exitosamente codigoerrorrenapo = __________" + $arrDatos['codigoerrorrenapo'] + "";
					$objGn->grabarLogx( '[VerificarCurpRenapoApi] ' . $mensaje);
				}
				else
				{
					// SI EXISTE UN ERROR EN LA CONSULTA MOSTRARA EL SIGUIENTE MENSAJE
					$arrDatos['estatus'] = -1;
					$arrDatos['MensajeDescrip'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx('[VerificarCurpRenapoApi] '.$resultAPI["descripcion"]);
				}
			}
			else
			{
				$mensaje = "No pudo abrir la conexion a PostGreSQL (aforeglobal)";
				$objGn->grabarLogx( '[VerificarCurpRenapoApi]' . $mensaje);
				$arrDatos['estatus'] = -1;
				$arrDatos['MensajeDescrip'] = "No abrio Conexion a PostGreSQL";
			}
		}
		catch (Exception $e)
		{
			//CACHA LA EXCEPCION POR LA QUE FALLO LA EJECUCION DEL QUERY Y LO MANDA COMO PARAMETRO PARA ESCRIBIR LA DESCRIPCION DEL ERROR.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[VerificarCurpRenapoApi]' . $mensaje);
		}
		CCapturaafiliacion::VerificaRespuestaRenapo($iEstatus,$iCodigoError,$cDescripcionError);
		return $arrDatos;	
	}
	public static function VerificaRespuestaRenapo( $iEstatus, $iCodigoError, $cDescripcionError )
	{
		$objGn = new CMetodoGeneral();
		$bRegresa = false;
		if ($iEstatus == 1)
		{
			if($iCodigoError == 0)
			{/* Mensaje de exito */
				$mensaje= "La Consulta Fue Exitosa";
				$objGn->grabarLogx( '[VerificarCurpRenapoApi]' . $mensaje);
				$bRegresa = true;
			}
			else if($iCodigoError > 0)
			{/* ocurrio un error al generar la curp dentro del WS 	*/
				if($iCodigoError == 1)
				{
					$mensaje= $cDescripcionError;
					$objGn->grabarLogx( '[VerificarCurpRenapoApi]' . $mensaje);
				}	
				if($iCodigoError == 4)
				{
					$mensaje= $cDescripcionError;
					$objGn->grabarLogx( '[VerificarCurpRenapoApi]' . $mensaje);
				}
				else//iCodigoError = 1,2 o 3; 
				{
					$mensaje= $cDescripcionError;
					$objGn->grabarLogx( '[VerificarCurpRenapoApi]' . $mensaje);
				}
			}
			else if($iCodigoError = -1)
			{
				$mensaje = "El servicio no responde. se reintentara nuevamente durante el siguiente plazo de validaciones renapo.";
				$objGn->grabarLogx( '[VerificarCurpRenapoApi]' . $mensaje);

			}
			else//iCodigoError = -2 o 5.
			{/* error al ejecutar la consulta de respuesta */
				$mensaje = "Error al consultar la respuesta";/* muestra este mensaje cuando la funcion regresa vacio */
				$objGn->grabarLogx( '[VerificarCurpRenapoApi]' . $mensaje);
			}
		}
		else
		{
			$mensaje = "El servicio no responde. Se reintentara nuevamente durante el siguiente plazo de validaciones renapo.";
			$objGn->grabarLogx( '[VerificarCurpRenapoApi]' . $mensaje);
		}
		return bRegresa;
	}
	//OPCION 20
	public static function ValidaConsultaRenapoPrevia($sCurp, $iEsTitular)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos 	= array(
			"Exito"=>0,
			"Mensaje"=>''
		);
		$arrApi = array(
			'sCurp' => $sCurp,
			'iEsTitular' => $iEsTitular
		);
		try
		{
			$objGn->grabarLogx("Inicio API Rest ValidaConsultaRenapoPrevia");

			$resultAPI = $objAPI->consumirApi('ValidaConsultaRenapoPrevia',$arrApi);

			$objGn->grabarLogx("Respuesta de Api Rest: ".$resultAPI);

			$objGn->grabarLogx("Fin API Rest ValidaConsultaRenapoPrevia");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI,true);

				if ($resultAPI['estatus'] == 1) {

					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['Exito'] = 1;
					$arrDatos['Mensaje'] = "EXITO";

					foreach($resultAPI["registros"] as $reg)
					{
						$arrDatos["iRenapo"] = $reg["renapo"];
					}
				}
				else
				{
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx( '['.__FILE__.']'.$resultAPI["descripcion"]);
				}
			}
			else
			{
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("casecapturaafiliacion.php\tvalidacionCurpAfiliacion"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}
	//OPCION 21
	public static function obtenerCurpBeneficiario($sCurp)
	{
		//crea objeto de la clase
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestcapturaafiliacion();
		$arrDatos 	= array(
			"Exito"=>0,
			"Mensaje"=>''
		);
		$arrApi = array(
			'sCurp' => $sCurp
		);
		try
		{
			$objGn->grabarLogx("Inicio API Rest obtenerCurpBeneficiario");

			$resultAPI = $objAPI->consumirApi('obtenerCurpBeneficiario',$arrApi);

			$objGn->grabarLogx("Respuesta de Api Rest: ".$resultAPI);

			$objGn->grabarLogx("Fin API Rest obtenerCurpBeneficiario");

			if($resultAPI)
			{
				$resultAPI = json_decode($resultAPI,true);

				if ($resultAPI['estatus'] == 1) {

					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['Exito'] = 1;
					$arrDatos['Mensaje'] = "EXITO";

					foreach($resultAPI["registros"] as $reg)
					{
						$arrDatos["cCurpSolicitante"] = $reg["curpsolicitante"];
					}
				}
				else
				{
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					$objGn->grabarLogx( '['.__FILE__.']'.$resultAPI["descripcion"]);
				}
			}
			else
			{
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("casecapturaafiliacion.php\tobtenerCurpBenficiario"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}

}
?>