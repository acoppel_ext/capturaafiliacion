<?php
	include_once('Clases/CRetCapturaafiliacion.php');
    include_once('Clases/CRetTipoTraspasoAfi.php');
	include_once('Clases/CMetodoGeneral.php');

	include_once("JSON.php");
	
	$json 			= new Services_JSON();
	$arrResp 		= array();
	$objGn 			= new CMetodoGeneral();
	$sIpRemoto 		= '';
	$opcion 		= isset($_POST['opcion']) 		? $_POST['opcion'] 		: 0;

	$empleado 		= isset($_POST['empleado']) 	? $_POST['empleado'] 	: 0;
    $foliosol       = isset($_POST['foliosol']) 	? $_POST['foliosol'] 	: 0;
    $numtarjeta     = isset($_POST['numtarjeta']) 	? $_POST['numtarjeta'] 	: '';

	//fnguardardatosstmpcapturaafiliacion
	//pantalla tiposolicitud Opcion 1
	$iTipoSol 		= isset($_POST['tiposol']) 		? $_POST['tiposol'] 	: 0;
	$esimss 		= isset($_POST['esimss']) 		? $_POST['esimss'] 		: 0;
	//pantalla tipo traspaso Opcion 2
	$iOpcGuar 		= 2;
	$tipotrasp 		= isset($_POST['tipotrasp']) 	? $_POST['tipotrasp'] 	: '';
	$cFolioEdo 		= isset($_POST['folioedo']) 	? $_POST['folioedo'] 	: '';
	$cAforeEdo 		= isset($_POST['aforeedo']) 	? $_POST['aforeedo'] 	: '';
	$cCuatriEdo 	= isset($_POST['cuatriedo']) 	? $_POST['cuatriedo'] 	: '';
	$aforeced 		= isset($_POST['aforeced']) 	? $_POST['aforeced'] 	: 0;
	//pantalla motivo Opcion 3
	$cMotivo 		= isset($_POST['motivo']) 		? $_POST['motivo'] 		: '';
	$cFolioImpli	= isset($_POST['folioimpli']) 	? $_POST['folioimpli'] 	: '';
	$iTipoAdmon		= isset($_POST['tipoadmon']) 	? $_POST['tipoadmon'] 	: 0;
	$cDepPrev 		= isset($_POST['depprevi']) 	? $_POST['depprevi'] 	: '';

	$sIpRemoto = $objGn->getRealIP();
	
	switch($opcion)
	{
        //Metodos utilizados en CAPTURAAFILIACION
		case 1:
			$arrResp = CCapturaafiliacion::obtnerinformacionpromotor($empleado);
		    break;
		case 12:
			$arrResp = CCapturaafiliacion::guardarInfoAfiliacion($iOpcGuar,$foliosol,$iTipoSol,$esimss,$tipotrasp,$cAforeEdo,$cFolioEdo,$cCuatriEdo,$cMotivo,$cFolioImpli,$iTipoAdmon,$cDepPrev,$aforeced);
			break;
		//Metodos utilizados en TIPOTRASPASOAFILIACION
        case 2:
            $arrResp = CTipoTraspasoAfiliacion::obtenerCURP($foliosol);
            break;
        case 3:
			$arrResp = CTipoTraspasoAfiliacion::comboTipoTraspaso();
		    break;
        case 4:
			$arrResp = CTipoTraspasoAfiliacion::comboAforeCedente();
		    break;
        case 5:
			$arrResp = CTipoTraspasoAfiliacion::comboPeriodoEstado();
		    break;
        case 6:
            $arrResp = CTipoTraspasoAfiliacion::ObtenerNumeroTarjeta($sIpRemoto);
            break;
        case 7:
            $arrResp = CTipoTraspasoAfiliacion::guardarNumeroTarjeta($foliosol,$numtarjeta);
            break;
	}
	
	echo $json->encode($arrResp);
?>