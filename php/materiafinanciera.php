<?php

include_once('Clases/global.php');
include_once('Clases/CMetodoGeneral.php');
include_once('Clases/Capirestcapturaafiliacion.php');
//include_once('clases/CLog.php');
include_once('JSON.php');
define('RUTA_LOGXMATERIAFINANCIERA', '/sysx/progs/afore/log/materiafinanciera');

$json = new Services_JSON();
$arrResp = array();

$iOpcion =  isset($_POST['opcion']) ? $_POST['opcion']: 0;
$iOpcionPagina =  isset($_POST['opcionpag']) ? $_POST['opcionpag']: 0;
$iFolio =  isset($_POST['foliomatfinanciera']) ? $_POST['foliomatfinanciera']: 0;
$iEmpleado =  isset($_POST['empleado']) ? $_POST['empleado']: 0;

switch($iOpcion)
{
	case 1:
		$arrResp = obtenercodigoHTML();
	break;
	case 2:
		$arrResp = obtenerligapagina($iOpcionPagina, $iFolio, $iEmpleado);
	break;
	
	default:
		
	break;
	
}
echo $json->encode($arrResp);

function obtenercodigoHTML() {

	//crea objeto de la clase
	$objGn = new CMetodoGeneral();
	$objAPI = new Capirestcapturaafiliacion();
	$datos = array("sCodigoHtml"=>'', "iTiempo"=>0, "iCodRetorno"=>0,"sDescripcion"=>'');
	
	$arrCurp = array();
	
	try
	{
		$objGn->grabarLogx("Inicio API Rest");

		$resultAPI = $objAPI->consumirApi('obtenercodigoHTML',$arrCurp);
		
		if($resultAPI)
		{
			$resultAPI = json_decode($resultAPI,true);

			if ($resultAPI['estatus'] == 1) {

				foreach($resultAPI["registros"] as $reg)
				{
					$datos["sCodigoHtml"] = $reg["html"];
					$datos["iTiempo"] = (int)$reg["tiempo"];
				}
				$datos['iCodRetorno'] = OK__;
				$datos['sDescripcion'] = "EXITO...";
			}
			else
			{
				$datos['iCodRetorno'] = ERR__;
				$datos['sDescripcion'] = "Se presento un problema al ejecutar la consulta";
				$objGn->grabarLogx( '['.__FILE__.']'.$resultAPI["descripcion"]);
			}
		}
		else
		{
			// Si existe un error en la consulta mostrar� el siguiente mensaje
			$datos['iCodRetorno'] = ERR__;
			$datos['sDescripcion'] = "Se presento un problema al ejecutar la consulta";

			throw new Exception("materiafinanciera.php\tobtenercodigoHTML"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
		}
	}
	catch (Exception $e)
	{
		//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
	}
	return $datos;
}

function obtenerligapagina($iOpcionPagina, $iFolio, $iEmpleado) {

	//crea objeto de la clase
	$objGn = new CMetodoGeneral();
	$objAPI = new Capirestcapturaafiliacion();
	$datos = array("iTipoAplicativo"=>0, "sDestino"=>'', "sDescripcionBoton"=>'', "iCodRetorno"=>0,"sDescripcion"=>'');
	$sHtml = "";
	$sIp = $objGn->getRealIP();
	
	$arrCurp = array(
		'iOpcionPagina' => $iOpcionPagina,
		'iFolio' => $iFolio,
		'iEmpleado' => $iEmpleado,
		'sIp' => $sIp	
	);
	
	try
	{
		$objGn->grabarLogx("Inicio API Rest");

		$resultAPI = $objAPI->consumirApi('obtenerligapagina',$arrCurp);

		if($resultAPI)
		{
			$resultAPI = json_decode($resultAPI,true);

			if ($resultAPI['estatus'] == 1) {

				foreach($resultAPI["registros"] as $reg)
				{
					$datos["iTipoAplicativo"] 		= $reg["tipoaplicativo"];
					$datos["sDescripcionBoton"] 	= trim($reg["descripcionboton"]);
					$datos["sDestino"] 				= trim($reg["destino"]);
				}
				$datos['iCodRetorno'] = OK__;
				$datos['sDescripcion'] = "EXITO...";
			}
			else
			{
				$datos['iCodRetorno'] = ERR__;
				$datos['sDescripcion'] = "Se presento un problema al ejecutar la consulta";
				$objGn->grabarLogx( '['.__FILE__.']'.$resultAPI["descripcion"]);
			}
		}
		else
		{
			// Si existe un error en la consulta mostrar� el siguiente mensaje
			$datos['iCodRetorno'] = ERR__;
			$datos['sDescripcion'] = "Se presento un problema al ejecutar la consulta";

			throw new Exception("materiafinanciera.php\obtenerligapagina"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
		}
	}
	catch (Exception $e)
	{
		//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
	}
	return $datos;
}

function grabarLogx($cadLogx)
{
	$objGn = new CMetodoGeneral();
	$cIpCliente = $objGn->getRealIP();
	$rutaLog =  RUTA_LOGXMATERIAFINANCIERA .  '-' . date("d") . '-' . date("m") . '-' . date("Y") . ".txt"; 
	$cad = date("Y-m|H:i:s|") . getmypid() . "|" . $cIpCliente . "| " . $cadLogx . "\n";
	$file = fopen($rutaLog, "a");
	if( $file )
	{
		fwrite($file, $cad);
	}
	fclose($file);
}

?>